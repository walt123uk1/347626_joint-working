@extends('layouts.appNoHeader')

@section('content')
<div class="container-fluid">
    <div class="row">
            <div class="hidden-xs col-sm-6 col-md-8 col-lg-offset-1 col-lg-6">
                <img src="{{ asset('img/circle.png') }}" class="img-responsive">
            </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <h2>Welcome to...</h2>
                    <img class="img-responsive" src="{{asset('img/color-logo.png')}}">
            
            <div class="text-box">
                <p>Joint Working is a collaborative approach to improving patient care, through NHS stakeholders and pharmaceutical companies working in partnership. This site is for people starting a Joint Working project. Before starting a project, or to learn more about Joint Working, contact your line manager. Please visit http://www.tevauk.com/jointworking to view current projects.</p>
            </div>

            @if (session('status'))
            <div style="margin-bottom: 10px;" class="col-xs-12 alert-custom clearfix">
                <div class="col-xs-10">
                    <p>{{ session('status') }}</p>
                </div>
                <div class="col-xs-2">
                        <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                </div>
            </div>
        @endif


            <form id="login-form"  class="form-horizontal" method="POST" action="{{ route('password.email') }}">

                {{--  <button type="submit" class="btn btn-primary no-radius login-btn">
                        Login <i class="-sign-in" aria-hidden="true"></i>
                </button>  --}}
                <div class="no-radius login-btn">
                    Reset Pasword <i class="glyphicon glyphicon-refresh" aria-hidden="true"></i>
            </div>

                <div class="login-form">

                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary standard-btn">
                                Submit
                            </button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div class="footer">
            <div class="container-fluid">
                <div style="text-align: right;" class="col-xs-offset-10 col-xs-2">
                    <p>Internal use only</p>
                    <!-- <p>Date of preparation: December 2017</p>
                    <p>UK/NHSS/17/0002a</p> -->
                    <p>Date of preparation: May 2019</p>
                    <p>UK/NHSS/19/0003</p>
                </div>
            </div>
        </div>
    </div>
    </div>

</div>
@endsection

