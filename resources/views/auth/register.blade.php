@extends('layouts.appNoHeader')

@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-8">
            <img class="img-responsive logo-jw" src="{{asset('img/color-logo.png')}}">
        </div>
        <div class="col-md-8 col-md-offset-2">

            <div class="no-radius login-btn">
                Registration <i class="fa fa-edit" aria-hidden="true"></i>
                <p class="light-para">Welcome to the Teva Joint Working Workflow Portal. <br>Before proceeding, please create your account.</p>
            </div>

                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">

                        <div class="register-form">

                        {{ csrf_field() }}

                        <div class="form-group no-margin{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-xs-12 col-md-offset-2 col-md-8">Firstname</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group no-margin{{ $errors->has('surname') ? ' has-error' : '' }}">
                            <label for="surname" class="col-xs-12 col-md-offset-2 col-md-8">Surname</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" required autofocus>

                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group no-margin{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-xs-12 col-md-offset-2 col-md-8">E-Mail Address</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group no-margin{{ $errors->has('email_confirmation') ? ' has-error' : '' }}">
                            <label for="email_confirmation" class="col-xs-12 col-md-offset-2 col-md-8">Confirm E-Mail Address</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="email_confirmation" oncopy="return false" oncut="return false" onpaste="return false" type="email_confirmation" class="form-control" name="email_confirmation" value="" required>

                                @if ($errors->has('email_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group no-margin{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-xs-12 col-md-offset-2 col-md-8">Phone Number</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="phone" type="number" class="form-control" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group no-margin{{ $errors->has('job_title') ? ' has-error' : '' }}">
                            <label for="job_title" class="col-xs-12 col-md-offset-2 col-md-8">Job Title</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="job_title" type="job_title" class="form-control" name="job_title" value="{{ old('job_title') }}" required>

                                @if ($errors->has('job_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('job_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group no-margin{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-xs-12 col-md-offset-2 col-md-8">Password</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-xs-12 col-md-offset-2 col-md-8">Confirm Password</label>

                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group no-margin">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="g-recaptcha" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 submit-block">
                        <p class="col-xs-12 submit-block-options">Select your user type from the options below</p>
                        <div class="col-xs-12 col-sm-4 col-lg-3 custom-control custom-radio">
                            <label class="custom-control-label" for="customRadio1">Editor</label>
                            <input type="radio" id="customRadio1" name="role" class="custom-control-input" value="editor">
                            <div class="whats-this">
                                <p class="pull-left">WHAT'S THIS</p>
                                <i  data-tool="tooltip" data-placement="left" data-html="true"  title="<strong>EDITOR</strong> <span class='glyphicon glyphicon glyphicon-edit'></span><br> A member of the JointWorking team, who will review, guide and advise on content." class="glyphicon glyphicon-question-sign pull-left" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-3 custom-control custom-radio">
                            <label class="custom-control-label" for="customRadio2">Viewer</label>
                            <input type="radio" id="customRadio2" name="role" class="custom-control-input" value="viewer">
                            <div class="whats-this">
                                <p class="pull-left">WHAT'S THIS</p>
                                <i  data-tool="tooltip" data-placement="left" data-html="true" title="<strong>VIEWER</strong> <span class='glyphicon glyphicon-eye-open'></span><br> A read-only user, designated for senior Teva figures. All functionality will be locked." class="glyphicon glyphicon-question-sign pull-left" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-3 custom-control custom-radio">
                            <label class="custom-control-label" for="customRadio">Author</label>
                            <input type="radio" id="customRadio3" name="role" class="custom-control-input" value="author">
                            <div class="whats-this">
                                <p class="pull-left">WHAT'S THIS</p>
                                <i  data-tool="tooltip" data-placement="left" data-html="true" title="<strong>AUTHOR</strong> <span class='glyphicon glyphicon-pencil'></span><br> A user seeking to submit their own Teva Joint Working project." class="glyphicon glyphicon-question-sign pull-left" aria-hidden="true"></i>
                            </div>

                        </div>
                        <button type="submit" class="col-md-2 col-lg-2 btn btn-primary standard-btn">
                                Submit
                        </button>

                        <div class="col-md-12 exempt-errors">
                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    </form>
                    
        </div>
        <div class="col-xs-12">
            <div class="container-fluid">
                <div style="text-align: right;" class="col-xs-offset-10 col-xs-2">
                    <p>Internal use only</p>
                    <!-- <p>Date of preparation: December 2017</p>
                    <p>UK/NHSS/17/0002a</p> -->
                    <p>Date of preparation: May 2019</p>
                    <p>UK/NHSS/19/0003</p>
                </div>
            </div>
        </div>
    </div>
    </div>
    
</div>
@endsection
