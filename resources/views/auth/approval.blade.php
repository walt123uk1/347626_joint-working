@extends('layouts.appNoHeader')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 thankyou">
                <h1 style="color:white !important;">Thank You</h1>
                <h2 style="color: white !important;">Your details have been submitted for approval.</h2>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class=" btn btn-primary standard-btn">Back to login page</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection