@extends('layouts.appNoHeader')

@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">

        @if(Session::has('notAuth'))
        <!-- Modal -->
        <div class="modal fade" id="approved-modal" tabindex="-1" role="dialog" aria-labelledby="approved-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Joint Working Portal</h4>
                </div>
                <div class="modal-body">
                    We are still waiting to approve your request to join the Joint Working Portal, please wait patiently for your request to be approved.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
        @endif

    <div class="hidden-xs col-sm-6 col-md-8 col-lg-offset-1 col-lg-6">
        <img src="{{ asset('img/circle.png') }}" class="img-responsive">
    </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <h1 style="color: #4b4959;">Welcome to...</h1>
            <img class="img-responsive" src="{{asset('img/color-logo.png')}}">
            
            <div class="text-box">
                <p>Joint Working is a collaborative approach to improving patient care, through NHS stakeholders and pharmaceutical companies working in partnership. This site is for people starting a Joint Working project. Before starting a project, or to learn more about Joint Working, contact your line manager. Please visit http://www.tevauk.com/jointworking to view current projects.</p>
            </div>

            <form id="login-form"  class="form-horizontal" method="POST" action="{{ route('login') }}">

                <div class="no-radius login-btn">
                        Login <i class="glyphicon glyphicon-log-in" aria-hidden="true"></i>
                </div>

                <div class="login-form">
                    
                    {{ csrf_field() }}

                    <div class="form-group no-margin{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-xs-offset-1 col-xs-12">Email</label>

                        <div class="col-xs-offset-1 col-xs-10">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-xs-offset-1 col-xs-12">Password</label>

                        <div class="col-xs-offset-1 col-xs-10">
                            <input id="password-forgot" type="password" autocomplete="off" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group no-margin">
                        <div class="col-md-8 col-xs-offset-1">
                            <div class="g-recaptcha" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-xs-offset-1">

                            
                            <a style="margin-bottom: 10px;" href="{{ url('/password/reset') }}">
                                <strong >Forgotten your password?</strong>
                            </a><br>
                            <a href="{{ route('register') }}">
                                <strong>Register</strong>
                            </a>
                        </div>
                    </div>
                <div class="form-group no-margin">
                    <button type="submit" class="get-rid col-xs-offset-1 col-sm-offset-8 btn btn-primary standard-btn ">Submit</button>
                </div>

                </div>
            </form>

        </div>
        <div class="footer">
            <div class="container-fluid">
                <div style="text-align: right;" class="col-xs-offset-10 col-xs-2">
                    <p>Internal use only</p>
                    <!-- <p>Date of preparation: December 2017</p>
                    <p>UK/NHSS/17/0002a</p> -->
                    <p>Date of preparation: May 2019</p>
                    <p>UK/NHSS/19/0003</p>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
