@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-offset-4 col-xs-4 title-centeral">
            <h2 class="green">Master's</h2>
            <h1 class="green"><strong>User Admin</strong></h1>
        </div>
        <div class="col-xs-12">
            <form method="get" action="/users/search">
                <div class="col-xs-12 col-md-offset-3 col-md-6">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" id="master-search" placeholder="Search User">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>
                </div>
            </form>
            <a href="/master/user-admin/new-user" class=" equalise-master-btn btn btn-primary standard-btn">Add New User</a>
            
            <ul class="col-xs-12 current list-group project-list">
                @forelse($users as $user)
                    <a  href="/master/user-admin/edit-user/{{$user->id}}"><li style="color: white !important;" class="list-group-item project-title ignore">{{$user->firstname . ' ' . $user->surname}}<span class=" pull-right glyphicon glyphicon-pencil"></span></li></a>
                @empty
                    <p>No users exist in the database</p>
                @endforelse
            </ul>
        </div>
    </div>
</div>
@endsection