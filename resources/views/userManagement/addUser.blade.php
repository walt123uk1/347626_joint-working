@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        @if(Session::has('message'))
        <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
            <div class="col-xs-10">
                <p>{{ Session::get('message') }}</p>
            </div>
            <div class="col-xs-2">
                    <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
            </div>
        </div>
        @endif

        <div class="col-md-8 col-md-offset-2">
            <div class="no-radius login-btn">
                <h2>Add User <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></h2>
            </div>

            <form class="form-horizontal" method="POST" action="/master/user-admin/new-user/submit">

                <div class="register-form">

                    {{ csrf_field() }}

                    <div class="form-group no-margin{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label for="firstname" class="col-md-offset-2 col-xs-8">Firstname</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>

                            @if ($errors->has('firstname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('surname') ? ' has-error' : '' }}">
                        <label for="surname" class="col-md-offset-2 col-xs-8">Surname</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" required autofocus>

                            @if ($errors->has('surname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-offset-2 col-xs-8">E-Mail Address</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('job_title') ? ' has-error' : '' }}">
                        <label for="job_title" class="col-md-offset-2 col-xs-8">Job Title</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="job_title" type="text" class="form-control" name="job_title" value="{{ old('job_title') }}" required>

                            @if ($errors->has('job_title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('job_title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-offset-2 col-xs-8">Phone Number</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="phone" type="number" class="form-control" name="phone" value="{{ old('phone') }}" required>

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('role') ? ' has-error' : '' }}">
                        <label for="role" class="col-md-offset-2 col-xs-8">Role</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <select class="form-control" id="role" name="role">
                                <option selected disabled="true" >--Select--</option>
                                <option value="viewer">Viewer</option>
                                <option value="author">Author</option>
                                <option value="editor">Editor</option>
                                <option value="master">Master</option>
                            </select>
                        </div>

                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group no-margin{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-offset-2 col-xs-8">Password</label>
    
                            <div class="col-md-offset-2 col-xs-8">
                                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required>
    
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>



                    <button type="submit" class="col-md-2 btn btn-primary standard-btn">
                            Submit
                    </button>
            </form>
        </div>
    </div>
</div>
@endsection
