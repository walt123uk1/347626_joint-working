@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(Session::has('message'))
        <div class="col-md-8 col-md-offset-2 col-xs-12 alert-custom">
            <div class="col-xs-10">
                <p>{{ Session::get('message') }}</p>
            </div>
            <div class="col-xs-2">
                    <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
            </div>
        </div>
        @endif
        <div class="col-xs-12 col-md-offset-7 col-md-4">
            <a href="/master/user-admin/new-user" class="btn btn-primary standard-btn">Add New User</a>
        </div>
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <div class="no-radius login-btn">
                <h2>Edit User <i class="fa fa-pencil-square" aria-hidden="true"></i></h2> 
            </div>

            <form class="form-horizontal" method="POST" action="/master/user-admin/edit-user/{{$user->id}}/edit">

                <div class="register-form">

                    {{ csrf_field() }}

                    {{ method_field('PATCH') }}

                    <div class="form-group no-margin{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label for="firstname" class="col-md-offset-2 col-xs-8">Firstname</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="firstname" type="text" class="form-control" name="firstname" value="{{ $user->firstname }}" required autofocus>

                            @if ($errors->has('firstname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('surname') ? ' has-error' : '' }}">
                        <label for="surname" class="col-md-offset-2 col-xs-8">Surname</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="surname" type="text" class="form-control" name="surname" value="{{ $user->surname }}" required autofocus>

                            @if ($errors->has('surname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-offset-2 col-xs-8">E-Mail Address</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('job_title') ? ' has-error' : '' }}">
                        <label for="job_title" class="col-md-offset-2 col-xs-8">Job Title</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="job_title" type="text" class="form-control" name="job_title" value="{{ $user->job_title }}" required>

                            @if ($errors->has('job_title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('job_title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-offset-2 col-xs-8">Phone Number</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <input id="phone" type="number" class="form-control" name="phone" value="{{ $user->phone }}" required>

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('role') ? ' has-error' : '' }}">
                        <label for="role" class="col-md-offset-2 col-xs-8">Role</label>

                        <div class="col-md-offset-2 col-xs-8">
                            <select class="form-control" id="role" name="role">
                                @foreach($roles as $role)
                                    @if($user->role == $role)
                                        <option value="{{$role}}" selected>{{$role}}</option>
                                    @else
                                        <option value="{{$role}}">{{$role}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group no-margin{{ $errors->has('role') ? ' has-error' : '' }}">
                        <label for="approved" class="col-md-offset-2 col-xs-8">Approved</label>
    
                        <div class="col-md-offset-2 col-xs-8">
                            @if($user->approved == 1)
                            <label class="radio-inline">
                                <input type="radio"  name="approved" value="1" checked>Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio"  name="approved" value="0">No
                            </label>
                            @else
                            <label class="radio-inline">
                                <input type="radio"  name="approved" value="1">Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio"  name="approved" value="0"  checked>No
                            </label>
                            @endif
                        </div>
    
                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>



                    <button type="submit" class="col-md-2 btn btn-primary standard-btn">
                            Submit
                    </button>
            </form>
        </div>
    </div>
</div>
@endsection
