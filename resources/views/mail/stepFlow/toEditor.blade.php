<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   </head>
   <style>
      @media (max-width: 500px){
        .wrapper, .collapse, .inner-body{
          width: 100% !important;
        }
        .collapse{
            float: left;
        }
      }
    </style>
   <body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
      <table class="wrapper" width="500px" cellpadding="0" cellspacing="0" style="table-layout: fixed; margin: 0 auto; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; padding: 0; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
          <tr>
             <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                <table class="content" width="500px" cellpadding="0" cellspacing="0" style=" table-layout: fixed; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                  <tr>
                    <td  style=" font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">
                        <a href="{{env('APP_URL')}}" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">
                        Joint Working | TEVA
                        </a>
                    </td>
                 </tr>
                  <!-- Email Body -->
                  <tr>
                     <td class="body" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                        <table class="inner-body" align="center" width="500" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; margin: 0 auto; padding: 0; width: 500px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 500px;">
                           <!-- Body content -->
                           <tr>
                              <td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                  <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: right;"></p>
                                @if( $project->isApproved == true )
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{$project->project_name . ' - Step ' . $step}} has been approved by a signatory, you may continue onto Step {{ $step + 1 }}.</p>
                                @elseif($project->move_to_step_6 == true)
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Your resubmission of the later project Step dates has been approved, you may now move to Step 6</p>
                                @elseif($project->back_to_dates == true)
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{$project->project_name}} - Step 5 has been approved, you now need to go back and enter dates for the remaining Steps.</p>
                                @elseif( isset($project->isFinished) )
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name}} has been approved by a signatory. </p>
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">This project has now been completed</p>
                                @elseif( isset($project->step) && !isset($project->sendback) && isset($project->editorEmail))
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name . ' - Step ' . $step . ' - ' . $project->user->firstname . ' ' . $project->user->surname}} submitted for approval by {{ $project->editorEmail }}</p>
                                @elseif( isset($project->step) && !isset($project->sendback) && !isset($project->editorEmail))
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name . ' - Step ' . $step}} submitted for approval by {{ $project->user->email }}</p>
                                @elseif( isset($project->step) && isset($project->sendback) && $project->step == 7)
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name . ' - Step ' . $step}} has been sent back for amendments. Please review the dates that you have submitted</p>
                                @elseif( isset($project->step) && isset($project->sendback) )
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name . ' - Step ' . $step}} has been sent back for amendments. Please review the comments and resubmit</p>
                                @elseif( !isset($project->step) && !isset($project->sendback) && isset($project->editorEmail) )
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name . ' - ' . $project->user->firstname . ' ' . $project->user->surname}} submitted for approval by {{ $project->editorEmail }}</p>
                                @elseif( !isset($project->step) && !isset($project->sendback) && !isset($project->editorEmail) )
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name . ' - ' . $project->user->firstname . ' ' . $project->user->surname}} submitted for approval by {{ $project->user->email }}</p>
                                @elseif( !isset($project->step) && isset($project->sendback) )
                                <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{ $project->project_name}} has been sent back for amendments.  Please review the comments and resubmit </p>
                                @endif
                                 <table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 30px auto; padding: 0; text-align: center; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                                    <tr>
                                       <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                             <tr>
                                                <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                   <table border="0" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                      <tr>
                                                         <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                                            
                                                            @if( $project->isApproved == true )
                                                            <a href="{{env('APP_URL')}}/{{ $project->id }}/steps/{{$step + 1}}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            @elseif($project->move_to_step_6 == true)
                                                            <a href="{{env('APP_URL')}}/{{ $project->id }}/steps/6" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            @elseif($project->back_to_dates == true)
                                                            <a href="{{env('APP_URL')}}/projects/{{ $project->id }}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            @elseif( isset($project->isFinished) )
                                                            <a href="{{env('APP_URL')}}/projects/{{ $project->id }}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            @elseif( isset($project->step) && !isset($project->sendback) )
                                                            <a href="{{env('APP_URL')}}/{{ $project->id }}/steps/{{$step}}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            @elseif( isset($project->step) && isset($project->sendback) )
                                                            <a href="{{env('APP_URL')}}/{{ $project->id }}/steps/{{$step}}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            @elseif( !isset($project->step) && !isset($project->sendback) )
                                                            <a href="{{env('APP_URL')}}/projects/{{ $project->id }}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            @elseif( !isset($project->step) && isset($project->sendback) )
                                                            <a href="{{env('APP_URL')}}/projects/{{ $project->id }}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">View it here</a>
                                                            
                                                            @endif

                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                 <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Regards,<br>Joint Working</p>
                                 <table class="subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-top: 1px solid #EDEFF2; margin-top: 25px; padding-top: 25px;">
                                    <tr>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                        <table class="footer" align="center" width="500" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 500px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 500px;">
                           <tr>
                              <td class="content-cell" align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                 <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE; font-size: 12px; text-align: center;">TEVA | Joint Working. All rights reserved</p>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>

