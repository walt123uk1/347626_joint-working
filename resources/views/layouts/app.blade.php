<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href={{ asset('img/favicon.png') }}>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Joint Working') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">  
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body style="background-image:url({{asset('img/BG.png')}}); background-repeat: no-repeat; background-size: cover; background-attachment: fixed; background-position: center;">
    <div class="main">
    <div id="app" >
        <nav class="navbar navbar-default navbar-static-top dark">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/projects') }}">
                        <img class="img-responsive" src="{{ asset('img/small-logo.png') }}">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        
                        <li class="hidden-xs hidden-sm" ><a href="{{ route('login') }}" class="glyphicon glyphicon-home"></a></li>
                        <li class="visible-xs visible-sm"><a href="{{ route('login') }}">Home</a></li>

                        @if(Auth::user()->role == 'author')
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="/projects">Projects
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    {{-- <li><img class="img-responsive" src="{{ asset('img/triangle.png') }}"></li> --}}
                                    {{-- <li><a href="{{ route('projects') }}">Pipeline</a></li> --}}
                                    <li><a href="{{ route('new project details') }}">New Project</a></li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{ route('projects') }}">Projects</a></li>
                        @endif

                        <li><a href="/profile/{{ Auth::user()->id }}">Profile</a></li>

                        @if(Auth::user()->role == 'master')
                            <li><a href="{{ route('user admin') }}">User Admin</a></li>
                        @endif

                        @if(Auth::user()->role == 'master')
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reports
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/reports/view">View Report</a></li>
                                    <li><a href="/reports/finance">Finance Report</a></li>
                                </ul>
                            </li>
                        @endif
                        <li><a id="tutorial-vid" href="" data-toggle="modal" data-target="#tutorial-modal">Tutorial</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Modal -->
        <div class="modal fade" id="tutorial-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Tutorial</h4>
                </div>
                <div id="video-loader" class="modal-body">
                    <video controls></video>
                </div>
                <div class="modal-footer">

                </div>
                </div>
            </div>
        </div>

        @yield('content')


    </div>

    <div class="footer">
            <div class="container-fluid">
                <div style="text-align: right;" class="col-xs-offset-10 col-xs-2">
                    <p>Internal use only</p>
                    <!-- <p>Date of preparation: December 2017</p>
                    <p>UK/NHSS/17/0002a</p> -->
                    <p>Date of preparation: May 2019</p>
                    <p>UK/NHSS/19/0003</p>
                </div>
            </div>
        </div>
    </div>
    {{--  <footer class="col-xs-12">
        <p> here lies the footer</p>
    </footer>  --}}

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/jquery.awesome-cursor.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('js/autosize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/html2pdf.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" integrity="sha384-CchuzHs077vGtfhGYl9Qtc7Vx64rXBXdIAZIPbItbNyWIRTdG0oYAqki3Ry13Yzu" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js"></script>
</body>
</html>
