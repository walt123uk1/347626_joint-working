@extends('layouts.app') 
@section('content')
<i class="fa fa-pencil faker"></i>
<div class="container" id="step1">
    <div class="row">
            <div class="numbers-container hidden-xs hidden-sm">
                <p >Step <span>1</span></p>
                <p>Step <span>2</span></p>
                <p>Step <span>3</span></p>
                <p>Step <span>4</span></p>
                <p class="active">Step <span>5</span></p>
                <p>Step <span>6</span></p>
                <p>Step <span>7</span></p>
                <p>Step <span>8</span></p>
            </div>
            <div class="numbers-line hidden-xs hidden-sm">

                </div>

                @if(Session::has('save-success'))
                <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
                    <div class="col-xs-10">
                        <p>{{ Session::get('save-success') }}</p>
                    </div>
                    <div class="col-xs-2">
                            <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                    </div>
                </div>
                @endif

            @if(Session::has('test'))
            <div class="col-md-8 col-md-offset-2 col-xs-12 alert-custom">
                <div class="col-xs-10">
                    <p>{{ Session::get('test') }}</p>
                </div>
                <div class="col-xs-2">
                        <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                </div>
            </div>
            @endif

            <div class="col-xs-12 col-md-7 col-md-offset-2">
                <h1>Step {{$step}}</h1>
                <h3>Governance and Executive Summary</h3>
            </div>
            <div class="col-xs-12 col-md-3">
                <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
            </div>

            <div class="col-xs-12 col-md-10 col-md-offset-2">
            <h2>{{ $project->user->firstname . ' ' . $project->user->surname . '/' . $project->project_name  }}</h2>

            <p>Due to the regulations of the Association of the British Pharmaceutical Industry, all Joint Working projects
                    must have an executive summary published online.<br>
                    In this section, please write an executive summary of the project, following the template provided.</p>

            <form method="POST" action="/{{$project->id}}/steps/{{$step}}/submit" enctype="multipart/form-data">

                {{ csrf_field() }}
                

                @if($project->step_5_locked == 1 && empty($project->answer->where('question_id',51)->first()) && Auth::user()->role == "editor" || $project->step_5_locked == 1 && !count($project->answer->where('question_id',51)) && Auth::user()->role == "master" || $project->step_5_locked == 1 && !count($project->answer->where('question_id',51)) && Auth::user()->role == "viewer")

                {{-- locked and no answers --}}
                    
                    <label>{{ $questions[0]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[0]->id}}" name="{{'Q'. $questions[0]->id}}" cols="10" rows="3" readonly></textarea>
                    </div>

                    <label>{{ $questions[1]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[1]->id}}" name="{{'Q'. $questions[1]->id}}" cols="10" rows="3" readonly></textarea>
                    </div>

                    <label>{{ $questions[2]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[2]->id}}" name="{{'Q'. $questions[2]->id}}" cols="10" rows="3" readonly></textarea>
                    </div>

                    <label>{{ $questions[3]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[3]->id}}" name="{{'Q'. $questions[3]->id}}" cols="10" rows="3" readonly></textarea>
                    </div>

                     <div class="col-xs-6">   
                    <label>{{ $questions[4]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">
                            <input id="{{$questions[4]->id}}" type="text" class="form-control generate date-picker text_input" name="{{'Q'. $questions[4]->id}}" readonly >
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="col-xs-6">
                    <label>{{ $questions[5]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">
                            <input id="{{$questions[5]->id}}" type="text" class="form-control generate date-picker text_input" name="{{'Q'. $questions[5]->id}}" readonly >
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                    </div>
                    </div>

                    <label>{{ $questions[6]->question }}</label> 
                    <div class="form-group">
                        <div id="diligence">
                            <div class="col-xs-6"><input  id="{{$questions[6]->id}}" type="text" name="diligence[]" class="form-control generate text_input" readonly></div>
                            <div class="col-xs-6"><input type="file" name="diligenceFile[]" class="form-control generate" disabled></div>
                        </div>
                    </div>

                    <label>{{ $questions[7]->question }}</label>
                    <div class="form-group"> 
                        <div id="compliance">
                            <div class="col-xs-6"><input  id="{{$questions[7]->id}}" type="text" name="compliance[]" class="form-control generate text_input" readonly></div>
                            <div class="col-xs-6"><input type="file" name="complianceFile[]" class="form-control generate" disabled></div>
                        </div>
                    </div>

                    <label>{{ $questions[8]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[8]->id}}" name="{{'Q'. $questions[8]->id}}" cols="10" rows="3" readonly></textarea>
                    </div>

                    <label>{{ $questions[9]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[9]->id}}" name="{{'Q'. $questions[9]->id}}" cols="10" rows="3" readonly></textarea>
                    </div>
                @elseif($project->step_5_locked == 0 && empty($project->answer->where('question_id',51)->first()) && Auth::user()->role == "author")
                    {{-- unlocked and no answers --}}
                    <label>{{ $questions[0]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[0]->id}}" name="{{'Q'. $questions[0]->id}}" cols="10" rows="3">{{ old('Q'. $questions[0]->id) }}</textarea>
                        <div id="char_left_{{ $questions[0]->id }}"></div>
                        @if ($errors->has('Q'. $questions[0]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[0]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[1]->question }}</label>
                    <div class="form-group"> 
                        <textarea class="form-control generate  text_input" id="{{$questions[1]->id}}" name="{{'Q'. $questions[1]->id}}" cols="10" rows="3">{{ old('Q'. $questions[1]->id) }}</textarea>
                           <div id="char_left_{{ $questions[1]->id }}"></div>
                        @if ($errors->has('Q'. $questions[1]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[1]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[2]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[2]->id}}" name="{{'Q'. $questions[2]->id}}" cols="10" rows="3">{{ old('Q'. $questions[2]->id) }}</textarea>
                        <div id="char_left_{{ $questions[2]->id }}"></div>
                        @if ($errors->has('Q'. $questions[2]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[2]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[3]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[3]->id}}" name="{{'Q'. $questions[3]->id}}" cols="10" rows="3">{{ old('Q'. $questions[3]->id) }}</textarea>
                        <div id="char_left_{{ $questions[3]->id }}"></div>
                        @if ($errors->has('Q'. $questions[3]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[3]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <div class="col-xs-6">
                    <label>{{ $questions[4]->question }}</label>
                    <div class="form-group"> 
                        <div class="input-group date" data-provide="datepicker">
                            <input id="{{$questions[4]->id}}" type="text" class="form-control generate  date-picker" name="{{'Q'. $questions[4]->id}}" value="{{ old('Q'. $questions[4]->id) }}" >
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        @if ($errors->has('Q'. $questions[4]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[4]->id) }}</strong>
                            </span>
                        @endif 
                    </div>
                </div>

                <div class="col-xs-6">
                    <label>{{ $questions[5]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" id="{{$questions[5]->id}}" class="form-control generate  date-picker" name="{{'Q'. $questions[5]->id}}" value="{{ old('Q'. $questions[5]->id) }}" >
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        @if ($errors->has('Q'. $questions[5]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[5]->id) }}</strong>
                            </span>
                        @endif 
                    </div>
                </div>

                    <label>{{ $questions[6]->question }}</label>
                    <div class="form-group"> 
                        <div id="diligence">
                            <div class="col-xs-6"><input id="{{$questions[6]->id}}" type="text" name="diligence[]" class="form-control generate  text_input"></div>
                            <div class="col-xs-6"><input type="file" name="diligenceFile[]" class="form-control generate" multiple></div>
                            <input type="hidden" name="hiddenDiligence[]" value="">
                        </div>
                        @if ($errors->has('diligence.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligence.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('diligenceFile.*'))
                        <span class="help-block">
                            <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                        </span>
                        @endif 
                        <div class="">
                            <button id="" type="button" class=" btn btn-primary standard-btn diligence-add">Add New</button>
                        </div>
                    </div>

                    <label>{{ $questions[7]->question }}</label> 
                    <div class="form-group">
                        <div id="compliance">
                            <div class="col-xs-6"><input type="text" id="{{$questions[7]->id}}" name="compliance[]" class="form-control generate  text_input"></div>
                            <div class="col-xs-6"><input type="file" name="complianceFile[]" class="form-control generate" multiple></div>
                            <input type="hidden" name="hiddenCompliance[]" value="">
                        </div>
                        @if ($errors->has('compliance.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('compliance.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('complianceFile.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('complianceFile.*') }}</strong>
                            </span>
                        @endif 
                        <div class="">
                            <button id="" type="button" class=" btn btn-primary standard-btn compliance-add">Add New</button>
                        </div>
                    </div>

                    <label>{{ $questions[8]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[8]->id}}" name="{{'Q'. $questions[8]->id}}" cols="10" rows="3">{{ old('Q'. $questions[8]->id) }}</textarea>
                        <div id="char_left_{{ $questions[8]->id }}"></div>
                        @if ($errors->has('Q'. $questions[8]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[8]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[9]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[9]->id}}" name="{{'Q'. $questions[9]->id}}" cols="10" rows="3">{{ old('Q'. $questions[9]->id) }}</textarea>
                        <div id="char_left_{{ $questions[9]->id }}"></div>
                        @if ($errors->has('Q'. $questions[9]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[9]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    @elseif($project->step_5_locked == 0 && empty($project->answer->where('question_id',51)->first()) && Auth::user()->role != "author")

                    {{-- unlocked and no answers --}}
                    <label>{{ $questions[0]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[0]->id}}" name="{{'Q'. $questions[0]->id}}" cols="10" rows="3" readonly>{{ old('Q'. $questions[0]->id) }}</textarea>
                        <div id="char_left_{{ $questions[0]->id }}"></div>
                        @if ($errors->has('Q'. $questions[0]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[0]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[1]->question }}</label>
                    <div class="form-group"> 
                        <textarea class="form-control generate  text_input" id="{{$questions[1]->id}}" name="{{'Q'. $questions[1]->id}}" cols="10" rows="3" readonly>{{ old('Q'. $questions[1]->id) }}</textarea>
                           <div id="char_left_{{ $questions[1]->id }}"></div>
                        @if ($errors->has('Q'. $questions[1]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[1]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[2]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[2]->id}}" name="{{'Q'. $questions[2]->id}}" cols="10" rows="3" readonly>{{ old('Q'. $questions[2]->id) }}</textarea>
                        <div id="char_left_{{ $questions[2]->id }}"></div>
                        @if ($errors->has('Q'. $questions[2]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[2]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[3]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[3]->id}}" name="{{'Q'. $questions[3]->id}}" cols="10" rows="3" readonly>{{ old('Q'. $questions[3]->id) }}</textarea>
                        <div id="char_left_{{ $questions[3]->id }}"></div>
                        @if ($errors->has('Q'. $questions[3]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[3]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <div class="col-xs-6">
                    <label>{{ $questions[4]->question }}</label>
                    <div class="form-group"> 
                        <div class="input-group date" data-provide="datepicker">
                            <input id="{{$questions[4]->id}}" type="text" class="form-control generate  date-picker" name="{{'Q'. $questions[4]->id}}" value="{{ old('Q'. $questions[4]->id) }}"  readonly>
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        @if ($errors->has('Q'. $questions[4]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[4]->id) }}</strong>
                            </span>
                        @endif 
                    </div>
                </div>

                <div class="col-xs-6">
                    <label>{{ $questions[5]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" id="{{$questions[5]->id}}" class="form-control generate  date-picker" name="{{'Q'. $questions[5]->id}}" value="{{ old('Q'. $questions[5]->id) }}"  readonly>
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        @if ($errors->has('Q'. $questions[5]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[5]->id) }}</strong>
                            </span>
                        @endif 
                    </div>
                </div>

                    <label>{{ $questions[6]->question }}</label>
                    <div class="form-group"> 
                        <div id="diligence">
                            <div class="col-xs-6"><input id="{{$questions[6]->id}}" type="text" name="diligence[]" class="form-control generate  text_input" readonly></div>
                            <div class="col-xs-6"><input type="file" name="diligenceFile[]" class="form-control generate" multiple disabled></div>
                            <input type="hidden" name="hiddenDiligence[]" value="">
                        </div>
                        @if ($errors->has('diligence.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligence.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('diligenceFile.*'))
                        <span class="help-block">
                            <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                        </span>
                        @endif 
                        <div class="">
                            <button id="" type="button" class=" btn btn-primary standard-btn diligence-add">Add New</button>
                        </div>
                    </div>

                    <label>{{ $questions[7]->question }}</label> 
                    <div class="form-group">
                        <div id="compliance">
                            <div class="col-xs-6"><input type="text" id="{{$questions[7]->id}}" name="compliance[]" class="form-control generate  text_input" readonly></div>
                            <div class="col-xs-6"><input type="file" name="complianceFile[]" class="form-control generate" multiple disabled></div>
                            <input type="hidden" name="hiddenCompliance[]" value="">
                        </div>
                        @if ($errors->has('compliance.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('compliance.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('complianceFile.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('complianceFile.*') }}</strong>
                            </span>
                        @endif 
                        <div class="">
                            <button id="" type="button" class=" btn btn-primary standard-btn compliance-add">Add New</button>
                        </div>
                    </div>

                    <label>{{ $questions[8]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[8]->id}}" name="{{'Q'. $questions[8]->id}}" cols="10" rows="3" readonly>{{ old('Q'. $questions[8]->id) }}</textarea>
                        <div id="char_left_{{ $questions[8]->id }}"></div>
                        @if ($errors->has('Q'. $questions[8]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[8]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[9]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[9]->id}}" name="{{'Q'. $questions[9]->id}}" cols="10" rows="3" readonly>{{ old('Q'. $questions[9]->id) }}</textarea>
                        <div id="char_left_{{ $questions[9]->id }}"></div>
                        @if ($errors->has('Q'. $questions[9]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[9]->id) }}</strong>
                            </span>
                        @endif 
                    </div>


                @elseif($project->step_5_locked == 1 || Auth::user()->role == "editor" || Auth::user()->role == "master" || Auth::user()->role == "viewer")

                    {{-- locked and answers --}}
                    <label>{{ $questions[0]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[0]->id}}" name="{{'Q'. $questions[0]->id}}" cols="10" rows="3" readonly>{{ $questions[0]->answer->where('project_id', $project->id)->first()->answer_text }}</textarea>
                    </div>

                    <label>{{ $questions[1]->question }}</label>
                    <div class="form-group"> 
                        <textarea class="form-control generate  text_input" id="{{$questions[1]->id}}" name="{{'Q'. $questions[1]->id}}" cols="10" rows="3" readonly>{{ $questions[1]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                    </div>

                    <label>{{ $questions[2]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[2]->id}}" name="{{'Q'. $questions[2]->id}}" cols="10" rows="3" readonly>{{ $questions[2]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                    </div>

                    <label>{{ $questions[3]->question }}</label>
                    <div class="form-group"> 
                        <textarea class="form-control generate  text_input" id="{{$questions[3]->id}}" name="{{'Q'. $questions[3]->id}}" cols="10" rows="3" readonly>{{ $questions[3]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                    </div>

                    <div class="col-xs-6">
                    <label>{{ $questions[4]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" id="{{$questions[4]->id}}" class="form-control generate date-picker text_input" name="{{'Q'. $questions[4]->id}}" value="{{ $questions[4]->answer->where('project_id', $project->id)->first()->answer_date->format('d/m/Y')}}" readonly >
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label>{{ $questions[5]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" id="{{$questions[5]->id}}" class="form-control generate date-picker text_input" name="{{'Q'. $questions[5]->id}}" value="{{ $questions[5]->answer->where('project_id', $project->id)->first()->answer_date->format('d/m/Y')}}" readonly >
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div> 
                    </div>
                </div>

                    <label>{{ $questions[6]->question }}</label> 
                    <div class="form-group">
                        <div id="diligence">
                            @foreach($diligences as $diligence)
                                <div class="row">
                                    <div class="col-xs-6"><input type="text" id="{{$questions[6]->id}}" class="form-control generate  text_input" name="diligence[]" id="" value="{{ $diligence->description }}" readonly></div>
                                    @if($diligence->file_name != null)
                                        <div class="col-xs-6 clearfix bump_down"><a target="_blank" href="{{ asset('storage/'. $diligence->file_name) }}" download="{{$diligence->file_name_unhashed}}"><span class="glyphicon glyphicon-file"></span>{{ $diligence->file_name_unhashed }}</a></div>
                                    @endif
                                    <input type="hidden" name="hiddenDiligence[]" value="{{$diligence->id}}">
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <label>{{ $questions[7]->question }}</label> 
                    <div class="form-group">
                        <div id="compliance">
                            @foreach($compliances as $compliance)
                                <div class="row">
                                    <div class="col-xs-6"><input id="{{$questions[7]->id}}" type="text" class="form-control generate text_input" name="compliance[]" value="{{ $compliance->description }}" readonly></div>
                                    @if($compliance->file_name != null)
                                    <div class="col-xs-6 clearfix bump_down"><a target="_blank" href="{{ asset('storage/'. $compliance->file_name) }}" download="{{$diligence->file_name_unhashed}}"><span class="glyphicon glyphicon-file"></span>{{ $compliance->file_name_unhashed }}</a></div>
                                    @endif
                                    <input type="hidden" name="hiddenCompliance[]" value="{{$compliance->id}}">
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <label>{{ $questions[8]->question }}</label>
                    <div class="form-group"> 
                        <textarea class="form-control generate  text_input" id="{{$questions[8]->id}}" name="{{'Q'. $questions[8]->id}}" cols="10" rows="3" readonly>{{ $questions[8]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                    </div>

                    <label>{{ $questions[9]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[9]->id}}" name="{{'Q'. $questions[9]->id}}" cols="10" rows="3" readonly>{{ $questions[9]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                    </div>

                @elseif($project->step_5_locked == 0 && count($project->answer->where('question_id',51)))

                    {{-- unlocked and answers --}}

                    <label>{{ $questions[0]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[0]->id}}" name="{{'Q'. $questions[0]->id}}" cols="10" rows="3">{{ $questions[0]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                        <div id="char_left_{{ $questions[0]->id }}"></div>
                        @if ($errors->has('Q'. $questions[0]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[0]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[1]->question }}</label>
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[1]->id}}" name="{{'Q'. $questions[1]->id}}" cols="10" rows="3">{{ $questions[1]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                        <div id="char_left_{{ $questions[1]->id }}"></div>
                        @if ($errors->has('Q'. $questions[1]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[1]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[2]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[2]->id}}" name="{{'Q'. $questions[2]->id}}" cols="10" rows="3">{{ $questions[2]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                        <div id="char_left_{{ $questions[2]->id }}"></div>
                        @if ($errors->has('Q'. $questions[2]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[2]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[3]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[3]->id}}" name="{{'Q'. $questions[3]->id}}" cols="10" rows="3">{{ $questions[3]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                        <div id="char_left_{{ $questions[3]->id }}"></div>
                        @if ($errors->has('Q'. $questions[3]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[3]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <div class="col-xs-6">
                    <label>{{ $questions[4]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">
                            @if ($questions[4]->answer->where('project_id', $project->id)->first()->answer_date =='') 
                                <input id="{{$questions[4]->id}}" type="text" class="form-control generate date-picker text_input" name="{{'Q'. $questions[4]->id}}" value="{{ $questions[4]->answer->where('project_id', $project->id)->first()->answer_date}}">
                            @else
                                <input id="{{$questions[4]->id}}" type="text" class="form-control generate date-picker text_input" name="{{'Q'. $questions[4]->id}}" value="{{ $questions[4]->answer->where('project_id', $project->id)->first()->answer_date->format('d/m/Y')}}">
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        @if ($errors->has('Q'. $questions[4]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[4]->id) }}</strong>
                            </span>
                        @endif 
                    </div>
                </div>

                <div class="col-xs-6">
                    <label>{{ $questions[5]->question }}</label> 
                    <div class="form-group">
                        <div class="input-group date" data-provide="datepicker">

                            @if ($questions[4]->answer->where('project_id', $project->id)->first()->answer_date =='') 
                                <input id="{{$questions[5]->id}}" type="text" class="form-control generate date-picker text_input" name="{{'Q'. $questions[5]->id}}" value="{{ $questions[5]->answer->where('project_id', $project->id)->first()->answer_date}}">
                            @else
                                <input id="{{$questions[5]->id}}" type="text" class="form-control generate date-picker text_input" name="{{'Q'. $questions[5]->id}}" value="{{ $questions[5]->answer->where('project_id', $project->id)->first()->answer_date->format('d/m/Y')}}">             
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        @if ($errors->has('Q'. $questions[5]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[5]->id) }}</strong>
                            </span>
                        @endif 
                    </div>
                </div>

                    <label>{{ $questions[6]->question }}</label> 
                    <div class="form-group">
                        <div id="diligence">
                            @forelse($diligences as $diligence)
                            <div class="row">
                                <div class="col-xs-6"><input id="{{$questions[6]->id}}" type="text" class="form-control generate  text_input" name="diligence[]" id="" value="{{ $diligence->description }}"></div>
                                <div class="col-xs-2">
                                @if($diligence->file_name != null)
                                    <a href="{{ asset('storage/'. $diligence->file_name) }}"><span class="glyphicon glyphicon-file"></span>{{ $diligence->file_name_unhashed }}</a>
                                @endif
                                </div>
                                <div class="col-xs-4">
                                    <input type="file" class="form-control generate " name="diligenceFile[]" id="" value="">
                                </div>
                                <input type="hidden" name="hiddenDiligence[]" value="{{$diligence->id}}">
                            </div>
                            @empty
                            @endforelse
                        </div>
                        @if ($errors->has('diligence.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligence.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('diligenceFile.*'))
                        <span class="help-block">
                            <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                        </span>
                        @endif 
                        <div class="">
                            <button id="" type="button" class=" btn btn-primary standard-btn diligence-add">Add New</button>
                        </div>
                    </div>

                    <label>{{ $questions[7]->question }}</label> 
                    <div class="form-group">
                        <div id="compliance">
                            @forelse($compliances as $compliance)
                            <div class="row">
                                <div class="col-xs-6"><input id="{{$questions[7]->id}}" type="text" class="form-control generate text_input" name="compliance[]" value="{{ $compliance->description }}"></div>
                                <div class="col-xs-2">
                                @if($compliance->file_name != null)
                                    <a href="{{ asset('storage/'. $compliance->file_name) }}"><span class="glyphicon glyphicon-file"></span>{{ $compliance->file_name_unhashed }}</a>
                                @endif
                                </div>
                                <div class="col-xs-4">
                                    <input type="file" class="form-control generate " name="complianceFile[]" id="" value="">
                                </div>
                                <input type="hidden" name="hiddenCompliance[]" value="{{$compliance->id}}">
                            </div>
                            @empty
                            @endforelse
                        </div>
                        <div class="">
                            <button id="" type="button" class=" btn btn-primary standard-btn compliance-add">Add New</button>
                        </div>
                        @if ($errors->has('compliance.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('compliance.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('complianceFile.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('complianceFile.*') }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[8]->question }}</label> 
                    <div class="form-group">
                        <textarea class="form-control generate  text_input" id="{{$questions[8]->id}}" name="{{'Q'. $questions[8]->id}}" cols="10" rows="3">{{ $questions[8]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                        <div id="char_left_{{ $questions[8]->id }}"></div>
                        @if ($errors->has('Q'. $questions[8]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[8]->id) }}</strong>
                            </span>
                        @endif 
                    </div>

                    <label>{{ $questions[9]->question }}</label>
                    <div class="form-group"> 
                        <textarea class="form-control generate  text_input" id="{{$questions[9]->id}}" name="{{'Q'. $questions[9]->id}}" cols="10" rows="3">{{ $questions[9]->answer->where('project_id', $project->id)->first()->answer_text}}</textarea>
                        <div id="char_left_{{ $questions[9]->id }}"></div>
                        @if ($errors->has('Q'. $questions[9]->id))
                            <span class="help-block">
                                <strong>{{ $errors->first('Q'. $questions[9]->id) }}</strong>
                            </span>
                        @endif 
                    </div>
                @endif

                    @if(Auth::user()->role == 'master' && $project->step_5_status == 3)
                        <div class="col-xs-12">
                            <button type="submit" name="submitButton" value="amend-master" class="submit-amendments btn btn-primary standard-btn">
                                Submit Amendments
                            </button>

                            <button type="submit" name="submitButton" value="approve" class="approve-step btn btn-primary standard-btn">
                                Approve Step
                            </button>
                        </div>
                    @elseif(Auth::user()->role == 'editor' && $project->step_5_status == 2)
                        <div class="col-xs-12">

                            <button type="submit" name="submitButton" value="amend-editor" class="submit-amendments btn btn-primary standard-btn">
                                Submit Amendments
                            </button>

                            
                            <button type="submit" name="submitButton" value="signatory" class="submit-to-signatory btn btn-primary standard-btn">
                                Submit to Signatory
                            </button>

                        </div>
                    @elseif(Auth::user()->role == 'author' && $project->step_5_locked == 0)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="submit" id="step5-submit" class="btn btn-primary standard-btn">
                            Submit
                        </button>
    
                        <button type="submit" name="submitButton" value="save" class="btn btn-primary standard-btn save-step step-4-special">
                            Save
                        </button> 
                    </div>
                    @endif 
                    <p class="center-text"><strong>The Joint Working agreement must include a clause that requires the recipient organisation to
                            consent to disclosure of Transfers of Value (TOVs) in accordance with the ABPI Code.</strong></p>
                </div>
            </form>

        @if($project->step_5_status == 2 or $project->step_5_status == 3)
            @if(Auth::user()->role == 'editor' && $project->step_5_status == 2)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif  
            @if(Auth::user()->role == 'master' && $project->step_5_status == 3)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif 
        @endif
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                <div>
                    <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                    <div class="tabs">
                        <div id="active-tab" class="tab">
                            <p class="active">Active <span class="fa fa-bolt"></span></p>
                        </div>
                        <div id="archived-tab" class="tab">
                            <p>Actioned <span class="fa fa-check-square-o"></span></p>
                        </div>
                        </div>
                    <div id="notes_list"></div>
                    <div id="archived_notes_list"></div>
                </div>
            </div>
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control  text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="{{$step}}">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="dialog-confirm" title="Warning!">
                {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
                <p>Are you sure you want to permanently delete this note?</p>
            </div> 
    </div>
</div>
@endsection

