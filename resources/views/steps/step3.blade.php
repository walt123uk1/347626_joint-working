@extends('layouts.app') 
@section('content')
<i class="fa fa-pencil faker"></i>
<div class="container">
    <div class="row">
        <div class="numbers-container hidden-xs hidden-sm">
            <p >Step <span>1</span></p>
            <p>Step <span>2</span></p>
            <p class="active">Step <span>3</span></p>
            <p>Step <span>4</span></p>
            <p>Step <span>5</span></p>
            <p>Step <span>6</span></p>
            <p>Step <span>7</span></p>
            <p>Step <span>8</span></p>
        </div>
        <div class="numbers-line hidden-xs hidden-sm">

        </div>

        @if(Session::has('save-success'))
        <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
            <div class="col-xs-10">
                <p>{{ Session::get('save-success') }}</p>
            </div>
            <div class="col-xs-2">
                    <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
            </div>
        </div>
        @endif
            

        <div class="col-xs-12 col-md-7 col-md-offset-2">
            <h1>Step {{$step}}</h1>
            <h3>Project Proposal</h3>
        </div>
        <div class="col-xs-12 col-md-3">
            <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
        </div>
        
        <div class="col-xs-12 col-md-10 col-md-offset-2">
            <h2>{{ $project->user->firstname . ' ' . $project->user->surname . '/' . $project->project_name  }}</h2>
            <p>In this section, please answer the questions provided to form a full project proposal.</br>Once submitted, the proposal will be distributed to the editing team for evaluation.</p>
            <p>The first section has already been completed for you from Step 2.</p>

            @if (count($answer_step2_18)) <p><label>NHS challenge: {{ $answer_step2_18[0] }}</label></p> @endif
            @if (count($answer_step2_19)) <p><label>Solution offered by project: {{ $answer_step2_19[0] }}</label></p> @endif
            @if (count($answer_step2_20)) <p><label>Location(s): {{ $answer_step2_20[0] }}</label></p> @endif
            @if (count($answer_step2_21)) <p><label>Stakeholder(s): {{ $answer_step2_21[0] }}</label></p> @endif
        </div>    
        <div class="col-xs-12 col-md-10 col-md-offset-2">
            <form method="POST"  id="{{$project->id}}_{{$step}}" action="/{{$project->id}}/steps/{{$step}}/submit">

                {{ csrf_field() }}

                @foreach ($questions as $question)
                <label>{{ $question->question }}</label>
                <div class="form-group">
                    @forelse ($question->answer->where('project_id', '=', $project->id) as $answer)  
                        <?php 
                            $x=0;
                            $all_text = $answer->answer_text;
                         ?>
                        @forelse ($answer->annotation->where('answer_id', '=', $answer->id) as $annotation)                             
                        @empty   
                        @endforelse
                        @if($project->step_3_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor') 
                            <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3" readonly><?php echo $all_text; ?></textarea>                 
                        @else                        
                            <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3">{{ old('Q' . $question->id, $all_text)  }}</textarea>
                            <div id="char_left_{{ $question->id }}"></div>
                            @if ($errors->has('Q'. $question->id))
                                <span class="help-block">
                                    <strong>{{ $errors->first('Q'. $question->id) }}</strong>
                                </span>
                            @endif
                        @endif
                    @empty
                        @if($project->step_3_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                            <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3" readonly></textarea>
                        @else                        
                            <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3">{{ old('Q' . $question->id)}}</textarea>
                            <div id="char_left_{{ $question->id }}"></div>
                            @if ($errors->has('Q'. $question->id))
                                <span class="help-block">
                                    <strong>{{ $errors->first('Q'. $question->id) }}</strong>
                                </span>
                            @endif
                        @endif
                    @endforelse 
                </div>
                @endforeach
                @if(Auth::user()->role == 'master' && $project->step_3_status == 3)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-master" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>  
                        <button type="submit" name="submitButton" value="approve" class="approve-step btn btn-primary standard-btn">
                            Approve Step
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'editor' && $project->step_3_status == 2)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-editor" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>
                        <button type="submit" name="submitButton" value="signatory" class="submit-to-signatory btn btn-primary standard-btn">
                            Submit to Signatory
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'author' && $project->step_3_locked == 0)
                <div class="col-xs-12">
                    <button type="submit" name="submitButton" value="submit" id="step3-submit" class="btn btn-primary standard-btn">
                        Submit
                    </button>

                    <button type="submit" name="submitButton" value="save" class="btn btn-primary standard-btn save-step">
                        Save
                    </button> 
                </div>
                @endif
            </form>
        </div>        
        @if($project->step_3_status == 2 or $project->step_3_status == 3)
            @if(Auth::user()->role == 'editor' && $project->step_3_status == 2)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif  
            @if(Auth::user()->role == 'master' && $project->step_3_status == 3)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif 
        @endif  
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                <div>
                    <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                    <div class="tabs">
                        <div id="active-tab" class="tab">
                            <p class="active">Active <span class="fa fa-bolt"></span></p>
                        </div>
                        <div id="archived-tab" class="tab">
                            <p>Actioned <span class="fa fa-archive"></span></p>
                        </div>
                        </div>
                    <div id="notes_list"></div>
                    <div id="archived_notes_list"></div>
                </div>
            </div>
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="{{$step}}">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>  
        <div id="dialog-confirm" title="Warning!">
                {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
                <p>Are you sure you want to permanently delete this note?</p>
            </div> 
    </div>
</div>
@endsection