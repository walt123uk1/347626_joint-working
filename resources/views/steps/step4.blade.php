@extends('layouts.app') 
@section('content')
<i class="fa fa-pencil faker"></i>
<div class="container">
    <div class="row">
            <div class="numbers-container hidden-xs hidden-sm">
                <p >Step <span>1</span></p>
                <p>Step <span>2</span></p>
                <p>Step <span>3</span></p>
                <p class="active">Step <span>4</span></p>
                <p>Step <span>5</span></p>
                <p>Step <span>6</span></p>
                <p>Step <span>7</span></p>
                <p>Step <span>8</span></p>
            </div>
            <div class="numbers-line hidden-xs hidden-sm">

                </div>

                @if(Session::has('save-success'))
                <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
                    <div class="col-xs-10">
                        <p>{{ Session::get('save-success') }}</p>
                    </div>
                    <div class="col-xs-2">
                            <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                    </div>
                </div>
                @endif
            
            {{-- @if ($errors->any())
                <div class="alert alert-custom">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                </div>
            @endif --}}



            <div class="col-xs-12 col-md-7 col-md-offset-2">
                <h1>Step {{$step}}</h1>
                <h3>Project Initiation Document</h3>
            </div>
            <div class="col-xs-12 col-md-3">
                <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
            </div>


            <div class="col-xs-12 col-md-10 col-md-offset-2">
            
            <h2>{{ $project->user->firstname . ' ' . $project->user->surname . '/' . $project->project_name  }}</h2>
            <p>Now that the Project Proposal is ready to progress, it is time to complete the Project Initiation Document (PID). The PID will
            be used as an outline of the planned approach to meet the project objectives. </p>
            <p>The PID is a very important document and filling it in as comprehensively as possible can save time at later stages and will
            prevent poorly conceived projects being initiated.</p>
            <p>Please complete the form below to produce the PID for the project.</p> 
            <p></p> 
            <div>
                <p>Click on the links below to access further questions</p>
                <div class="page-errors">
                    <p>The following page(s) contain errors:</p>
                    <ul></ul>
                </div>
                <ul class="pagination">
                    <li><a href="#" class='current' style="pointer-events: none;">Page 1 of 5</a></li>
                    <li><a href="#" class='pg_1'>1</a></li>
                    <li><a href="#" class='pg_2'>2</a></li>
                    <li><a href="#" class='pg_3'>3</a></li>
                    <li><a href="#" class='pg_4'>4</a></li>
                    <li><a href="#" class='pg_5'>5</a></li>
                    <li><a href="#" class='prev'>Prev</a></li>
                    <li><a href="#" class='next'>Next</a></li>
                </ul>
            </div>
            <form method="POST"  id="{{$project->id}}_{{$step}}" action="/{{$project->id}}/steps/{{$step}}/submit">

                {{ csrf_field() }}
                
                @foreach ($questions as $question)
                @if ($question->id == 30) 
                    <div class="page_1 page">               
                @elseif ($question->id == 36) 
                    <div class="page_2 page">             
                @elseif ($question->id == 41) 
                    <div class="page_3 page">             
                @elseif ($question->id == 46) 
                    <div class="page_4 page">             
                @elseif ($question->id == 49) 
                    <div class="page_5 page">
                @endif  
                            <?php 
                                echo "<label>".$question->question."</label>";
                             ?>      
                        <div class="form-group">     
                            @forelse ($question->answer->where('project_id', '=', $project->id) as $answer)  
                                <?php
                                    $x=0;
                                    $all_text = $answer->answer_text;
                                ?>                        
                                @forelse ($answer->annotation->where('answer_id', '=', $answer->id) as $annotation)                             
                                @empty   
                                @endforelse
                                @if($project->step_4_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor') 
                                    <textarea class="form-control generate  text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3" readonly><?php echo $all_text; ?></textarea>                 
                                @else                        
                                    <textarea class="form-control generate  text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3">{{ old('Q' . $question->id, $all_text)  }}</textarea>
                                    <div id="char_left_{{ $question->id }}"></div>
                                    @if ($errors->has('Q'. $question->id))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Q'. $question->id) }}</strong>
                                        </span>
                                    @endif 
                                @endif
                            @empty
                                @if($project->step_4_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                                    <textarea class="form-control generate  text_input" id="{{ $question->id }}" name="{{'Q'. $question->id }}" cols="10" rows="3" readonly></textarea>
                                @else                        
                                    <textarea class="form-control generate  text_input" id="{{ $question->id }}" name="{{'Q'. $question->id }}" cols="10" rows="3">{{ old('Q' . $question->id)  }}</textarea>
                                    <div id="char_left_{{ $question->id }}"></div>
                                    @if ($errors->has('Q'. $question->id))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Q'. $question->id) }}</strong>
                                        </span>
                                    @endif 
                                @endif
                            @endforelse    
                        </div>      
                @if ($question->id == 35) 
                    </div>                   
                @elseif ($question->id == 40) 
                    </div>                 
                @elseif ($question->id == 45) 
                    </div>                 
                @elseif ($question->id == 48) 
                    </div>                 
                @elseif ($question->id == 50) 
                    </div>
                @endif 
                @endforeach

                <div>
                    <p>Click on the links below to access further questions</p>
                    <ul class="pagination">
                        <li><a href="#" class='current' style="pointer-events: none;">Page 1 of 5</a></li>
                        <li><a href="#" class='pg_1 p-active'>1</a></li>
                        <li><a href="#" class='pg_2'>2</a></li>
                        <li><a href="#" class='pg_3'>3</a></li>
                        <li><a href="#" class='pg_4'>4</a></li>
                        <li><a href="#" class='pg_5'>5</a></li>
                        <li><a href="#" class='prev'>Prev</a></li>
                        <li><a href="#" class='next'>Next</a></li>
                    </ul>
                </div>
                <div id='button_section'>
                    @if(Auth::user()->role == 'master' && $project->step_4_status == 3)
                        <div class="col-xs-12">
                            <button type="submit" name="submitButton" value="amend-master" class="submit-amendments btn btn-primary standard-btn">
                                Submit Amendments
                            </button>  
                            <button type="submit" name="submitButton" value="approve" class="approve-step btn btn-primary standard-btn">
                                Approve Step
                            </button>
                        </div>
                    @elseif(Auth::user()->role == 'editor' && $project->step_4_status == 2)
                        <div class="col-xs-12">
                            <button type="submit" name="submitButton" value="amend-editor" class="submit-amendments btn btn-primary standard-btn">
                                Submit Amendments
                            </button>
                            <button type="submit" name="submitButton" value="signatory" class="submit-to-signatory btn btn-primary standard-btn">
                                Submit to Signatory
                            </button>
                        </div>
                    @elseif(Auth::user()->role == 'author' && $project->step_4_locked == 0)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="submit" id="step4-submit" class="btn btn-primary standard-btn">
                            Submit
                        </button>
    
                        <button type="submit" name="submitButton" value="save" class="btn btn-primary standard-btn save-step">
                            Save
                        </button> 
                    </div>
                    @endif
                </div>
            </form>
        </div>
        @if($project->step_4_status == 2 or $project->step_4_status == 3)
            @if(Auth::user()->role == 'editor' && $project->step_4_status == 2)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif  
            @if(Auth::user()->role == 'master' && $project->step_4_status == 3)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif     
        @endif      
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                    <div>
                        <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                        <div class="tabs">
                            <div id="active-tab" class="tab">
                                <p class="active">Active <span class="fa fa-bolt"></span></p>
                            </div>
                            <div id="archived-tab" class="tab">
                                <p>Actioned <span class="fa fa-archive"></span></p>
                            </div>
                            </div>
                        <div id="notes_list"></div>
                        <div id="archived_notes_list"></div>
                    </div>
                </div>
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control  text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="{{$step}}">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="dialog-confirm" title="Warning!">
                {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
                <p>Are you sure you want to permanently delete this note?</p>
            </div> 
    </div>
</div>
@endsection