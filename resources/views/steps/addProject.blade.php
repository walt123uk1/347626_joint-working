@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

    <div class="col-md-9 how-portal-works">
        <div data-tool="tooltip" data-placement="right" title="Complete all the free text boxes as comprehensively as possible, please ensure the information is accurate and is in line with ABPI guidelines. The Joint Working team will receive a notification when you click submit. As Editors, they will review your content and may suggest amendments as appropriate. You will receive an email notification when the Editors have approved your progression to the next step. This will also appear on your dashboard. The Signatory will approve you to the next step once all content is agreed. The Signatory is the only person who can give interim and final approvals through each step of the Joint Working project. A similar process is undertaken for each step." class="col-xs-12 col-sm-4 how-it-works">
            <span class="glyphicon glyphicon-question-sign pull-right"></span><p class="pull-left">HOW THE<br>PORTAL WORKS</p>
        </div>
        </div>
        <div class="col-md-3">
            <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
        </div>

        <div class="col-xs-12 ">
                <h3 class="project-details-text">Please enter the details of your proposed Joint Working project.</h3>
                <p>Best practice time is usually 9 to 12 weeks for full project roll-out, up to step 5.</p>
        </div>
            <form class="col-xs-12" id="add_project_form" method="post" action="/projects/new/submit">

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('project_name') ? ' has-error' : '' }}">
                    <label for="project_name" class="col-xs-8">Name of project</label>

                    <div class="col-xs-8">
                        <input class="form-control generate " type="text" name="project_name" value="{{ old('project_name') }}" >
                    </div>

                    @if ($errors->has('project_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('project_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('approach_date') ? ' has-error' : '' }}">
                    <label for="approach_date" class="col-xs-8">Date of initial formal approach with internal shareholders</label>

                    <div class="col-xs-8">
                        <div class="input-group date" data-provide="datepicker" style="margin-bottom: 20px;">
                            <input class="form-control generate date-picker" type="text" name="approach_date" value="{{ old('approach_date') }}" >
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>    
                    </div>

                    @if ($errors->has('approach_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('approach_date') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="col-xs-6">
                    <p>Projected timelines for each step</p>
                </div>

                <div class="col-xs-6">
                    <p>Date of commencement:</p>
                </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_1_date') ? ' has-error' : '' }}">
                        <label for="step_1_date" class="col-xs-12 col-md-6 step-text">Step 1 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>IS THIS JOINT WORKING?</strong><br> In the first stage, a checklist is completed, confirming that the project is truly Joint Working and to identify any potential issues that may arise." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">
                        <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control generate date-picker" name="step_1_date" value="{{ old('step_1_date') }}" >
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('step_1_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_1_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_2_date') ? ' has-error' : '' }}">
                        <label for="step_2_date" class="col-xs-12 col-md-6 step-text">Step 2 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>IDEA GENERATION</strong><br> A form will be completed at
                            this stage describing the initial
                            idea behind the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">
                        <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control generate date-picker" name="step_2_date" value="{{ old('step_2_date') }}" >
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('step_2_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_2_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_3_date') ? ' has-error' : '' }}">
                        <label for="step_3_date" class="col-xs-12 col-md-6 step-text">Step 3 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT PROPOSAL</strong><br> A full project proposal with NHS partners is developed
                            at this stage, with feedback allowing the Joint Working
                            team to have input into the content of the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">
                        <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control generate date-picker" name="step_3_date" value="{{ old('step_3_date') }}" >
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('step_3_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_3_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_4_date') ? ' has-error' : '' }}">
                        <label for="step_4_date" class="col-xs-12 col-md-6 step-text">Step 4 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT INITIATION DOCUMENT</strong><br> A template is provided for the Project Initiation
                            Document, which needs to be completed before
                            project commencement." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">
                        <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control generate date-picker" name="step_4_date" value="{{ old('step_4_date') }}" >
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('step_4_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_4_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_5_date') ? ' has-error' : '' }}">
                        <label for="step_5_date" class="col-xs-12 col-md-6 step-text">Step 5 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>GOVERNANCE & EXECUTIVE SUMMARY</strong><br> Completion of an Executive Summary at this stage of
                            the process is necessary to satisfy ABPI regulations
                            before the project commences." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">
                        <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control generate date-picker" name="step_5_date" value="{{ old('step_5_date') }}" >
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('step_5_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_5_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar uneditable col-xs-12">

                    <div class="form-group{{ $errors->has('step_6_date') ? ' has-error' : '' }}">
                        <label for="step_6_date" class="col-xs-12 col-md-6 step-text">Step 6 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT MANAGEMENT</strong><br> This step is needed to input the
                            necessary Zinc job numbers and
                            purchase orders." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">
                        <div class="input-group date" >
                                <input type="text" class="form-control generate" name="step_6_date" value="{{ old('step_6_date') }}" readonly>
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('step_6_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_6_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar uneditable col-xs-12">

                        <div class="form-group{{ $errors->has('step_7_date') ? ' has-error' : '' }}">
                            <label for="step_7_date" class="col-xs-12 col-md-6 step-text">Step 7 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT REVIEW</strong><br> This step will be ongoing during project
                                commencement to ensure that reviews and meetings
                                are being held in a timely manner, consisting of
                                reviews every 3 months during the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>
    
                            <div class="col-xs-12 col-md-4">
                            <div class="input-group date" >
                                    <input type="text" class="form-control generate " name="step_7_date" value="{{ old('step_7_date') }}" readonly>
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
    
                            @if ($errors->has('step_7_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('step_7_date') }}</strong>
                                </span>
                            @endif
                        </div>
    
                    </div>

                    <div class="project_date_bar uneditable  col-xs-12">

                        <div class="form-group{{ $errors->has('step_8_date') ? ' has-error' : '' }}">
                            <label for="step_8_date" class="col-xs-12 col-md-6 step-text">Step 8 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>WRITE UP</strong><br> At completion, this
                                step allows the author
                                to record and publish the
                                findings of the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>
    
                            <div class="col-xs-12 col-md-4">
                            <div class="input-group date" >
                                    <input type="text" class="form-control generate" name="step_8_date" value="{{ old('step_8_date') }}" readonly>
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                </div>
                            </div>
    
                            @if ($errors->has('step_8_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('step_8_date') }}</strong>
                                </span>
                            @endif
                        </div>
    
                    </div>
                    <div class="col-xs-12">
                            <button type="submit" value="submit" class="btn btn-primary standard-btn">
                                Submit
                            </button>
                    </div>
            </form>
    </div>
</div>
@endsection