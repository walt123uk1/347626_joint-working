@extends('layouts.app') 
@section('content')
<i class="fa fa-pencil faker"></i>
<div class="container">    
    <div class="row">
        <div class="numbers-container hidden-xs hidden-sm">
            <p >Step <span>1</span></p>
            <p class="active">Step <span>2</span></p>
            <p>Step <span>3</span></p>
            <p>Step <span>4</span></p>
            <p>Step <span>5</span></p>
            <p>Step <span>6</span></p>
            <p>Step <span>7</span></p>
            <p>Step <span>8</span></p>
        </div>
        <div class="numbers-line hidden-xs hidden-sm">

        </div>

        @if(Session::has('save-success'))
        <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
            <div class="col-xs-10">
                <p>{{ Session::get('save-success') }}</p>
            </div>
            <div class="col-xs-2">
                    <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
            </div>
        </div>
        @endif

            <div class="col-xs-12 col-md-7 col-md-offset-2">
                <h1>Step {{$step}}</h1> 
                <h3>Idea Generation</h3>
            </div>
            <div class="col-xs-12 col-md-3">
                <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
            </div>

            <div class="col-xs-12 col-md-10 col-md-offset-2">

            <h2>{{ $project->user->firstname . ' ' . $project->user->surname . '/' . $project->project_name  }}</h2>
            <p>At this stage, we are aiming to determine the initial idea behind the Joint Working project.</br>
                The idea should have been born out of disc-ussions with NHS stakeholders to help improve patient outcomes.</p>
            <p>You must now begin the due diligence process, please contact thirdpartyprogrameu@tevapharm.com for any related
                queries.</p>
            <p>Please complete the boxes below as comprehensively as possible. The Joint Working team will review the content
                and may suggest changes as appropriate.
            </p>

            <form method="POST" id="{{$project->id}}_{{$step}}" action="/{{$project->id}}/steps/{{$step}}/submit">       
                {{ csrf_field() }} 
                @foreach ($questions as $question) 
                    <label>{{ $question->question }}</label>
                    <div class="form-group">     
                        @forelse ($question->answer->where('project_id', '=', $project->id) as $answer)            
                            @forelse ($answer->annotation->where('answer_id', '=', $answer->id) as $annotation)                             
                                <!-- add html for any annotations -->
                            <!--     <span id="annotate_{{ $annotation->id }}" style="position: relative; top: {{ $annotation->posn_y }}px; left: {{ $annotation->posn_x }}px; z-index: 1024;" class="stooltip glyphicon glyphicon-comment"></span>  -->
                            @empty                        
                            @endforelse
                            <?php 
                                $x=0;
                                $all_text = $answer->answer_text;
                             ?> 
                            @if($project->step_2_locked == 1 || Auth::user()->role == 'master' || Auth::user()->role == 'editor')
                                <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3" readonly><?php echo $all_text; ?></textarea>
                            @else
                                <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3">{{ old('Q' . $question->id, $all_text)  }}</textarea>
                                <div id="char_left_{{ $question->id }}"></div>
                                @if ($errors->has('Q'. $question->id))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Q'. $question->id) }}</strong>
                                    </span>
                                @endif
                            @endif
                        @empty
                            @if($project->step_2_locked == 1 || Auth::user()->role == 'master' || Auth::user()->role == 'editor')
                            <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3" readonly></textarea>
                            @else
                                <textarea class="form-control generate text_input" id="{{ $question->id }}" name="{{'Q'. $question->id}}" cols="10" rows="3">{{old('Q' . $question->id)}}</textarea>
                            <div id="char_left_{{ $question->id }}"></div>
                            @if ($errors->has('Q'. $question->id))
                                <span class="help-block">
                                    <strong>{{ $errors->first('Q'. $question->id) }}</strong>
                                </span>
                            @endif
                            @endif 
                        @endforelse
                    </div>
                @endforeach 
                @if(Auth::user()->role == 'master' && $project->step_2_status == 3)
                <div class="col-xs-12">
                    <button type="submit" name="submitButton" value="amend-master" class="submit-amendments btn btn-primary standard-btn">
                        Submit Amendments
                    </button>


                    <button type="submit" name="submitButton" value="approve" class="approve-step btn btn-primary standard-btn">
                        Approve Step
                    </button>
                </div>
                @elseif(Auth::user()->role == 'editor' && $project->step_2_status == 2)
                <div class="col-xs-12">

                    <button type="submit" name="submitButton" value="amend-editor" class="submit-amendments btn btn-primary standard-btn">
                        Submit Amendments
                    </button>


                    <button type="submit" name="submitButton" value="signatory" class="submit-to-signatory btn btn-primary standard-btn">
                        Submit to Signatory
                    </button>

                </div>
                @elseif(Auth::user()->role == 'author' && $project->step_2_locked == 0)
                <div class="col-xs-12">
                    <button type="submit" name="submitButton" value="submit" id="step2-submit" class="btn btn-primary standard-btn">
                        Submit
                    </button>

                    <button type="submit" name="submitButton" value="save" class="btn btn-primary standard-btn save-step">
                        Save
                    </button>
                </div>
                @endif
            </form>
        </div>
        <div class="col-md-offset-2 col-xs-9">
            <h2>Teva’s Third Party Due Diligence Programme (TPDD)</h2>
            <p>Any vendors you engage to support the delivery of your project will need to comply with Teva’s Third Party Due
                Diligence Programme because Teva can be held responsible for improper conduct of non–Teva partners that perform
                activities on our behalf. We investigate the background and reputation of prospective third parties to ensure
                that they are capable of performing the work requested and will do so ethically.</p>
            <p>The Teva Third Party Due Diligence Programme applies to new and reviewing relationships with Third Party representatives.
                </p>
            <h2>If you are not sure:</h2>
            <ul>
                <li>Whether a relationship is ‘in scope’</li>
                <li>How to complete the online form</li>
                <li>Why Teva needs to complete Due Diligence</li>
            </ul>
            <p>You must contact Local Compliance or reach the Third Party Team directly by emailing
                <a href='mailto:thirdpartyprogrameu@tevapharm.com'>thirdpartyprogrameu@tevapharm.com</a>
            </p>
            <p>Please retain all correspondence for your records.</p>
        </div>

        @if($project->step_2_status == 2 or $project->step_2_status == 3)
            @if(Auth::user()->role == 'editor' && $project->step_2_status == 2)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif  
            @if(Auth::user()->role == 'master' && $project->step_2_status == 3)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif 
        @endif   
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                <div>
                    <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                    <div class="tabs">
                        <div id="active-tab" class="tab">
                            <p class="active">Active <span class="fa fa-bolt"></span></p>
                        </div>
                        <div id="archived-tab" class="tab">
                            <p>Actioned <span class="fa fa-archive"></span></p>
                        </div>
                        </div>
                    <div id="notes_list"></div>
                    <div id="archived_notes_list"></div>
                </div>
            </div>
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="user-name" name="user-name" value="{{Auth::user()->firstname}} {{Auth::user()->surname}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="{{$step}}">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div> 
    <div id="dialog-confirm" title="Warning!">
        {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
        <p>Are you sure you want to permanently delete this note?</p>
    </div> 

    </div>
</div>
@endsection