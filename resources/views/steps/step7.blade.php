@extends('layouts.app')
@section('content')
<i class="fa fa-pencil faker"></i>
<div class="container">
    <div class="row">
            <div class="numbers-container hidden-xs hidden-sm">
                <p >Step <span>1</span></p>
                <p>Step <span>2</span></p>
                <p>Step <span>3</span></p>
                <p>Step <span>4</span></p>
                <p>Step <span>5</span></p>
                <p>Step <span>6</span></p>
                <p class="active">Step <span>7</span></p>
                <p>Step <span>8</span></p>
            </div>
            <div class="numbers-line hidden-xs hidden-sm"></div>

            @if(Session::has('message-step7'))
            <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
                <div class="col-xs-10">
                    <p>{{ Session::get('message-step7') }}</p>
                </div>
                <div class="col-xs-2">
                        <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                </div>
            </div>
            @endif

            @if(Session::has('save-success'))
            <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
                <div class="col-xs-10">
                    <p>{{ Session::get('save-success') }}</p>
                </div>
                <div class="col-xs-2">
                        <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                </div>
            </div>
            @endif

            <div class="col-xs-12 col-md-7 col-md-offset-2">
            <h1>Step {{$step}}</h1>
            <h3>Project Review</h3>
            </div>
            <div class="col-xs-12 col-md-3">
                <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
            </div>

            <div class="col-xs-12 col-md-10 col-md-offset-2">
                <h2>{{ $project->user->firstname . ' ' . $project->user->surname . '/' . $project->project_name  }}</h2>
            <p>Over the lifetime of the Joint Working project, you will need to have a rolling 3-monthly review with the Head Office Joint Working Team.</p>
            <p>You should meet regularly with your line manager to discuss project progress.</p>
            <form method="POST"  id="{{$project->id}}_{{$step}}" action="/{{$project->id}}/steps/{{$step}}/submit">

                {{ csrf_field() }}

                <label>{{ $questions[0]->question }}</label></td> 
                <div id="review">
                    <?php $x = 0; ?>
                    @forelse($reviews as $review)
                        @if($project->step_7_locked == 1)
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control" name="review_id[]" id="" value="{{ $review->id }}" >
                                    <div class='input-group date' data-provide='datepicker'>
                                        <input type='text' class='form-control generate date-picker text_input' name='review[]' id="{{ $review->id }}" value="{{ $review->review_date->format('d/m/Y') }}" readonly>
                                        <div class='input-group-addon'>
                                            <span class='glyphicon glyphicon-calendar'></span>
                                        </div>
                                    </div>
                                    @if ($errors->has('review.*'))
                                        <span class="help-block">
                                            <strong><p>{{ $errors->first('review.*') }}</p></strong>
                                        </span>
                                    @endif  
                                    @if ($errors->has('review_id.*'))
                                        <span class="help-block">
                                            <strong><p>{{ $errors->first('review.*') }}</p></strong>
                                        </span>
                                    @endif  
                                </div>      
                                @if(Auth::user()->role == 'editor' && $project->step_7_status !== 3 )                  
                                    @if($review->review_attended == 0)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate">
                                                <option value="0" selected></option>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @elseif($review->review_attended == 1)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate">
                                                <option value="0"></option>
                                                <option value="yes" selected>Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @elseif($review->review_attended == 2)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate">
                                                <option value="0"></option>
                                                <option value="yes">Yes</option>
                                                <option value="no" selected>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @endif
                                @else        
                                    @if($review->review_attended == 0)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" selected></option>
                                                <option value="yes" disabled>Yes</option>
                                                <option value="no" disabled>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @elseif($review->review_attended == 1)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" disabled></option>
                                                <option value="yes" selected>Yes</option>
                                                <option value="no" disabled>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @elseif($review->review_attended == 2)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" disabled></option>
                                                <option value="yes" disabled>Yes</option>
                                                <option value="no" selected>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @endif
                                @endif
                            </div>
                        @else 
                            @if(Auth::user()->role == 'editor' || Auth::user()->role == 'master')
                                <div class="row form-group">
                                    <div class='col-xs-6'>
                                        <input type="hidden" class="form-control" name="review_id[]" id="" value="{{ $review->id }}" >
                                        <div class='input-group date' data-provide='datepicker'>
                                            <input type='text' class='form-control generate date-picker' name='review[]' id="{{ $review->id }}" value="{{ $review->review_date->format('d/m/Y') }}" readonly>
                                            <div class='input-group-addon'>
                                                <span class='glyphicon glyphicon-calendar'></span>
                                            </div>
                                        </div>
                                        @if ($errors->has('review.*'))
                                            <span class="help-block">
                                                <strong><p>{{ $errors->first('review.*') }}</p></strong>
                                            </span>
                                        @endif  
                                        @if ($errors->has('review_id.*'))
                                            <span class="help-block">
                                                <strong><p>{{ $errors->first('review.*') }}</p></strong>
                                            </span>
                                        @endif 
                                    </div>  
                                    @if($review->review_attended == 0)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" selected></option>
                                                <option value="yes" disabled>Yes</option>
                                                <option value="no" disabled>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @elseif($review->review_attended == 1)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" disabled></option>
                                                <option value="yes" selected>Yes</option>
                                                <option value="no" disabled>No</option>
                                            </select>

                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif
                                        </label>
                                    @elseif($review->review_attended == 2)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" disabled></option>
                                                <option value="yes" disabled>Yes</option>
                                                <option value="no" selected>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @endif
                                </div>
                            @else                            
                                <div class="row form-group">
                                    <div class='col-xs-6'>
                                        <input type="hidden" class="form-control" name="review_id[]" id="" value="{{ $review->id }}" >
                                        <div class='input-group date' data-provide='datepicker'>
                                            <input type='text' class='form-control generate date-picker' name='review[]' id="{{ $review->id }}" value="{{ $review->review_date->format('d/m/Y') }}">
                                            <div class='input-group-addon'>
                                                <span class='glyphicon glyphicon-calendar'></span>
                                            </div>
                                        </div>
                                        @if ($errors->has('review.*'))
                                            <span class="help-block">
                                                <strong><p>{{ $errors->first('review.*') }}</p></strong>
                                            </span>
                                        @endif  
                                        @if ($errors->has('review_id.*'))
                                            <span class="help-block">
                                                <strong><p>{{ $errors->first('review.*') }}</p></strong>
                                            </span>
                                        @endif 
                                    </div>  
                                    @if($review->review_attended == 0)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" selected></option>
                                                <option value="yes" disabled>Yes</option>
                                                <option value="no" disabled>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @elseif($review->review_attended == 1)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" disabled></option>
                                                <option value="yes" selected>Yes</option>
                                                <option value="no" disabled>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @elseif($review->review_attended == 2)
                                        <label class='col-xs-6'>Attended 
                                            <select name="review_attended[]" class="step7_check generate" readonly>
                                                <option value="0" disabled></option>
                                                <option value="yes" disabled>Yes</option>
                                                <option value="no" selected>No</option>
                                            </select>
                                            @if ($errors->has('review_attended.*'))
                                                <span class="help-block">
                                                    <strong><p>{{ $errors->first('review_attended.*') }}</p></strong>
                                                </span>
                                            @endif  
                                        </label>
                                    @endif
                                </div>
                            @endif
                        @endif   
                        <?php $x = $x +1; ?>
                    @empty   
                    @if(Auth::user()->role == 'author')   
                        <div class='row'>
                            <div class='col-xs-6'>  
                                <input type='hidden' name='review_id[]' class='form-control'>
                                <div class='input-group date' data-provide='datepicker'>
                                    <input type='text' class='form-control generate date-picker' name='review[]' value="{{ old('review.0') }}">
                                    <div class='input-group-addon'>
                                        <span class='glyphicon glyphicon-calendar'></span>
                                    </div>
                                </div>
                                @if ($errors->has('review.0'))
                                <span class="help-block">
                                    <strong><p>{{ $errors->first('review.0') }}</p></strong>
                                </span>
                            @endif  
                            </div>
                            <label class='col-xs-6'>Attended 
                                <select name='review_attended[]' class='step7_check'>
                                    <option value='0' selected></option>
                                    <option value='yes' disabled>Yes</option>
                                    <option value='no'disabled>No</option>
                                </select>
                            </label>
                        </div>
                        <div class='row'>
                            <div class='col-xs-6'>  
                                <input type='hidden' name='review_id[]' class='form-control'>
                                <div class='input-group date' data-provide='datepicker'>
                                    <input type='text' class='form-control generate date-picker' name='review[]' value="{{ old('review.1') }}">
                                    <div class='input-group-addon'>
                                        <span class='glyphicon glyphicon-calendar'></span>
                                    </div>
                                </div>
                                @if ($errors->has('review.1'))
                                <span class="help-block">
                                    <strong><p>{{ $errors->first('review.1') }}</p></strong>
                                </span>
                            @endif  
                            </div>
                            <label class='col-xs-6'>Attended 
                                <select name='review_attended[]' class='step7_check'>
                                    <option value='0' selected></option>
                                    <option value='yes' disabled>Yes</option>
                                    <option value='no'disabled>No</option>
                                </select>
                            </label>
                        </div>
                        <div class='row'>
                            <div class='col-xs-6'>  
                                <input type='hidden' name='review_id[]' class='form-control'>
                                <div class='input-group date' data-provide='datepicker'>
                                    <input type='text' class='form-control generate date-picker' name='review[]' value="{{ old('review.2') }}">
                                    <div class='input-group-addon'>
                                        <span class='glyphicon glyphicon-calendar'></span>
                                    </div>
                                </div>
                                @if ($errors->has('review.2'))
                                <span class="help-block">
                                    <strong><p>{{ $errors->first('review.2') }}</p></strong>
                                </span>
                            @endif  
                            </div>
                            <label class='col-xs-6'>Attended 
                                <select name='review_attended[]' class='step7_check'>
                                    <option value='0' selected></option>
                                    <option value='yes' disabled>Yes</option>
                                    <option value='no'disabled>No</option>
                                </select>
                            </label>
                        </div>
                        @else
                                                <div class='row'>
                            <div class='col-xs-6'>  
                                <input readonly type='hidden' name='review_id[]' class='form-control'>
                                <div class='input-group date' data-provide='datepicker'>
                                    <input readonly type='text' class='form-control generate date-picker' name='review[]' value="{{ old('review.0') }}">
                                    <div class='input-group-addon'>
                                        <span class='glyphicon glyphicon-calendar'></span>
                                    </div>
                                </div>
                                @if ($errors->has('review.0'))
                                <span class="help-block">
                                    <strong><p>{{ $errors->first('review.0') }}</p></strong>
                                </span>
                            @endif  
                            </div>
                            <label class='col-xs-6'>Attended 
                                <select name='review_attended[]' class='step7_check'>
                                    <option value='0' selected></option>
                                    <option value='yes' disabled>Yes</option>
                                    <option value='no'disabled>No</option>
                                </select>
                            </label>
                        </div>
                        <div class='row'>
                            <div class='col-xs-6'>  
                                <input readonly type='hidden' name='review_id[]' class='form-control'>
                                <div class='input-group date' data-provide='datepicker'>
                                    <input readonly type='text' class='form-control generate date-picker' name='review[]' value="{{ old('review.1') }}">
                                    <div class='input-group-addon'>
                                        <span class='glyphicon glyphicon-calendar'></span>
                                    </div>
                                </div>
                                @if ($errors->has('review.1'))
                                <span class="help-block">
                                    <strong><p>{{ $errors->first('review.1') }}</p></strong>
                                </span>
                            @endif  
                            </div>
                            <label class='col-xs-6'>Attended 
                                <select name='review_attended[]' class='step7_check'>
                                    <option value='0' selected></option>
                                    <option value='yes' disabled>Yes</option>
                                    <option value='no'disabled>No</option>
                                </select>
                            </label>
                        </div>
                        <div class='row'>
                            <div class='col-xs-6'>  
                                <input readonly type='hidden' name='review_id[]' class='form-control'>
                                <div class='input-group date' data-provide='datepicker'>
                                    <input readonly type='text' class='form-control generate date-picker' name='review[]' value="{{ old('review.2') }}">
                                    <div class='input-group-addon'>
                                        <span class='glyphicon glyphicon-calendar'></span>
                                    </div>
                                </div>
                                @if ($errors->has('review.2'))
                                <span class="help-block">
                                    <strong><p>{{ $errors->first('review.2') }}</p></strong>
                                </span>
                            @endif  
                            </div>
                            <label class='col-xs-6'>Attended 
                                <select name='review_attended[]' class='step7_check'>
                                    <option value='0' selected></option>
                                    <option value='yes' disabled>Yes</option>
                                    <option value='no'disabled>No</option>
                                </select>
                            </label>
                        </div>
                        @endif
                    @endforelse
                    {{-- @if ($errors->any())
                            <span class="help-block">
                                @foreach ($errors->all() as $error)
                                    <strong><p>{{ $error }}</p></strong>
                                @endforeach
                            </span>
                    @endif --}}
                </div>

                @if($project->step_7_locked == 1)  

                @else
                    <div class="col-xs-12">
                        <button id="step7-submit-btn" type="button" class=" btn btn-primary standard-btn review-add purple">Add New</button>
                    </div>
                @endif

                <div class="col-xs-12">
                    <label>For sections of the project the following people may need to be in attendance:</label>
                    <p>IHM and their line manager</p>
                    <p>Director of NHS Strategy</p>
                    <p>Associate Director, NHS Strategy</p>
                </div>
                @if(Auth::user()->role == 'master' && $project->step_7_status == 3)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-master" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>  
                        <button type="submit" name="submitButton" value="approve" class="approve-step btn btn-primary standard-btn">
                            Approve Step
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'editor' && $project->step_7_status == 2)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-editor" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>
                        <button type="submit" name="submitButton" value="signatory" class="submit-to-signatory btn btn-primary standard-btn">
                            Submit to Signatory
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'author' && $project->step_7_locked == 0)
                <div class="col-xs-12">
                    <button type="submit" name="submitButton" value="submit" id="step7-submit" class="btn btn-primary standard-btn">
                        Submit
                    </button>
                    <button type="submit" name="submitButton" value="save" class="btn btn-primary standard-btn">
                        Save
                    </button> 
                </div>
                @endif
            </form>
        </div>
        @if($project->step_7_status == 2 or $project->step_7_status == 3)
            @if(Auth::user()->role == 'editor' && $project->step_7_status == 2)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif  
            @if(Auth::user()->role == 'master' && $project->step_7_status == 3)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif 
        @endif 
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                    <div>
                        <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                        <div class="tabs">
                            <div id="active-tab" class="tab">
                                <p class="active">Active <span class="fa fa-bolt"></span></p>
                            </div>
                            <div id="archived-tab" class="tab">
                                <p>Actioned <span class="fa fa-archive"></span></p>
                            </div>
                            </div>
                        <div id="notes_list"></div>
                        <div id="archived_notes_list"></div>
                    </div>
                </div>
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control  text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="review">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="{{$step}}">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>  
        <div id="dialog-confirm" title="Warning!">
                {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
                <p>Are you sure you want to permanently delete this note?</p>
            </div>  
    </div>
</div>
@endsection