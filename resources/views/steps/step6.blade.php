@extends('layouts.app')
@section('content')
<i class="fa fa-pencil faker"></i>
<div class="container">
    <div class="row">

            <div class="numbers-container hidden-xs hidden-sm">
                <p >Step <span>1</span></p>
                <p>Step <span>2</span></p>
                <p>Step <span>3</span></p>
                <p>Step <span>4</span></p>
                <p>Step <span>5</span></p>
                <p class="active">Step <span>6</span></p>
                <p>Step <span>7</span></p>
                <p>Step <span>8</span></p>
            </div>
            <div class="numbers-line hidden-xs hidden-sm">

                </div>

            @if(Session::has('message-step6'))
            <div class="col-xs-12 alert-custom">
                <div class="col-xs-10">
                    <p>{{ Session::get('message-step6') }}</p>
                </div>
                <div class="col-xs-2">
                        <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                </div>
            </div>
            @endif

            @if(Session::has('save-success'))
            <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
                <div class="col-xs-10">
                    <p>{{ Session::get('save-success') }}</p>
                </div>
                <div class="col-xs-2">
                        <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                </div>
            </div>
            @endif

            <div class="col-xs-12 col-md-7 col-md-offset-2">
                <h1>Step {{$step}}</h1>
                <h3>Project Management</h3>
            </div>
            <div class="col-xs-12 col-md-3">
                <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
            </div>

            <div class="col-xs-12 col-md-10 col-md-offset-2">
            <h2>{{ $project->user->firstname . ' ' . $project->user->surname . '/' . $project->project_name  }}</h2>
            <form method="POST"  id="{{$project->id}}_{{$step}}" action="/{{$project->id}}/steps/{{$step}}/submit" enctype="multipart/form-data">

                {{ csrf_field() }}
                <label>{{ $questions[0]->question }}</label>

                <div id="zinc" class="block-parent">
                    <?php $x = 0; ?>
                    @forelse($zincs as $zinc)
                        @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control text_input" name="zinc_id[]" id="" value="{{ $zinc->id }}" readonly>
                                    <input type="text" class="form-control generate text_input" name="zinc[]" id="{{ $questions[0]->id }}" value="{{ $zinc->description }}" readonly>
                                    @if ($errors->has('zinc.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zinc.*') }}</strong>
                                    </span>
                                @endif 
                                </div>
                            </div>
                        @else 
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control text_input" name="zinc_id[]" id="" value="{{ $zinc->id }}">
                                    <input type="text" class="form-control generate text_input" name="zinc[]" id="{{ $questions[0]->id }}" value="{{ $zinc->description }}" >
                                    @if ($errors->has('zinc.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zinc.*') }}</strong>
                                    </span>
                                @endif 
                                </div>
                            </div>
                        @endif   
                        <?php $x = $x + 1; ?>
                    @empty         
                        <div class="row form-group">
                            @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control  text_input" name="zinc_id[]" id="" value="" readonly>
                                    <input type="text" class="form-control generate text_input" name="zinc[]" id="{{ $questions[0]->id }}" value="" readonly>
                                </div>
                            @else
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control  text_input" name="zinc_id[]" id="" value="">
                                    <input type="text" class="form-control generate text_input" name="zinc[]" id="{{ $questions[0]->id }}" value="">
                                </div>
                            @endif
                            @if ($errors->has('zinc.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('zinc.*') }}</strong>
                            </span>
                        @endif 
                        </div>           
                    @endforelse
                </div>               
                @if($project->step_6_locked == 1)  

                @else
                    <div class="col-xs-12">
                        <button id="step6-submit-btn" type="button" class=" btn btn-primary standard-btn zinc-add purple btn-fix-pos">Add New</button>
                    </div>
                @endif
                <div class="row form-group">
                    <label class='col-xs-3'>{{ $questions[1]->question }}</label>
                    <label class='col-xs-3'>{{ $questions[2]->question }}</label>
                    <label class='col-xs-6'>{{ $questions[3]->question }}</label>
                </div>
                <div id="po" class="block-parent">
                    <?php $x = 0; ?>
                    @forelse($pos as $po)
                        @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                            <div class="row form-group">
                                <div class='col-xs-3'>
                                    <input type="hidden" class="form-control text_input" name="po_id[]" id="" value="{{ $po->id }}" readonly>
                                    <input type="text" class="form-control generate text_input" name="po[]" id="{{ $questions[1]->id }}" value="{{ $po->description }}" readonly>
                                </div>
                                <div class='col-xs-4'>
                                    <input type="text" class="form-control text_input" name="po_who[]" id="{{ $questions[2]->id }}" value="{{ $po->who }}" readonly>
                                </div>
                                <div class='col-xs-5'>
                                    <input type="text" class="form-control text_input" name="po_purpose[]" id="{{ $questions[3]->id }}" value="{{ $po->purpose }}" readonly>
                                </div>
                            </div>
                        @else 
                            <div class="row form-group">
                                <div class='col-xs-3'>
                                    <input type="hidden" class="form-control text_input" name="po_id[]" id="" value="{{ $po->id }}">
                                    <input type="text" class="form-control generate text_input" name="po[]" id="{{ $questions[1]->id }}" value="{{ $po->description }}" >
                                    @if ($errors->has('po.*'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('po.*') }}</strong>
                                        </span>
                                    @endif 
                                </div>
                                <div class='col-xs-3'>
                                    <input type="text" class="form-control text_input" name="po_who[]" id="{{ $questions[2]->id }}" value="{{ $po->who }}" >
                                    @if ($errors->has('po_who.*'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('po_who.*') }}</strong>
                                        </span>
                                    @endif 
                                </div>
                                <div class='col-xs-6'>
                                    <input type="text" class="form-control text_input" name="po_purpose[]" id="{{ $questions[3]->id }}" value="{{ $po->purpose }}" >
                                    @if ($errors->has('po_purpose.*'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('po_purpose.*') }}</strong>
                                        </span>
                                    @endif 
                                </div>
                            </div>
                        @endif   
                        @if ($errors->has('po.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('po.*') }}</strong>
                            </span>
                        @endif 
                        <?php $x = $x + 1; ?>
                    @empty   
                        @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')   
                            <div class="row form-group">
                                <div class='col-xs-3'>
                                    <input type="hidden" class="form-control text_input" name="po_id[]" id="" value="" readonly>
                                    <input type="text" class="form-control generate text_input" name="po[]"  id="{{ $questions[1]->id }}" value="" readonly>
                                </div>
                                <div class='col-xs-3'>
                                    <input type="text" class="form-control text_input" name="po_who[]" id="{{ $questions[2]->id }}" value="" readonly>
                                    @if ($errors->has('po_who.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_who.*') }}</strong>
                                    </span>
                                @endif 
                                </div>
                                <div class='col-xs-6'>
                                    <input type="text" class="form-control text_input" name="po_purpose[]" id="{{ $questions[3]->id }}" value="" readonly>
                                    @if ($errors->has('po_purpose.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_purpose.*') }}</strong>
                                    </span>
                                @endif 
                                </div>
                                @if ($errors->has('po.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po.*') }}</strong>
                                    </span>
                                @endif
                            </div>      
                        @else             
                            <div class="row form-group">
                                <div class='col-xs-3'>
                                    <input type="hidden" class="form-control text_input" name="po_id[]" id="" value="">
                                    <input type="text" class="form-control generate text_input" name="po[]" id="{{ $questions[1]->id }}" value="" >
                                </div>
                                <div class='col-xs-3'>
                                    <input type="text" class="form-control text_input" name="po_who[]" id="{{ $questions[2]->id }}" value="" >
                                    @if ($errors->has('po_who.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_who.*') }}</strong>
                                    </span>
                                @endif 
                                </div>
                                <div class='col-xs-6'>
                                    <input type="text" class="form-control text_input" name="po_purpose[]" id="{{ $questions[3]->id }}" value="" >
                                    @if ($errors->has('po_purpose.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_purpose.*') }}</strong>
                                    </span>
                                @endif 
                                </div>
                                @if ($errors->has('po.*'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po.*') }}</strong>
                                    </span>
                                @endif
                            </div>
                        @endif
                    @endforelse
                </div>
                @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')  
                @else
                    <div class="col-xs-12">
                        <button id="step6-submit-btn" type="button" class=" btn btn-primary standard-btn po-add purple btn-fix-pos">Add New</button>
                    </div>
                @endif
                <label>{{ $questions[4]->question }}</label> 
                <div id="diligence" class="block-parent">
                    <?php $x = 0; ?>
                    @forelse($diligences as $diligence)
                        @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control text_input" name="hiddenDiligence[]" id="" value="{{ $diligence->id }}" readonly>
                                    <input type="text" class="form-control generate text_input" name="diligence[]"  id="{{ $questions[4]->id }}" value="{{ $diligence->description }}" readonly>
                                </div>
                                <div class="col-xs-2">
                                    <p class="remove-margin">&nbsp;</p>
                                    @if($diligence->file_name != null)
                                        <a href="{{ asset('storage/'. $diligence->file_name) }}" download="{{$diligence->file_name_unhashed}}" ><span class="glyphicon glyphicon-file"></span>{{ $diligence->file_name_unhashed }}</a>
                                    @endif
                                </div>
                                <div class='col-xs-4'>
                                        <p class="remove-margin">&nbsp;</p>
                                    <input type="file" class="form-control" name="diligenceFile[]" id="" value="" disabled>
                                </div>
                            </div>
                            @if ($errors->has('diligence.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligence.*') }}</strong>
                            </span>
                            @endif 
                            @if ($errors->has('diligenceFile.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                                </span>
                            @endif 
                        @else 
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control  text_input" name="hiddenDiligence[]" id="" value="{{ $diligence->id }}">
                                    <input type="text" class="form-control generate text_input" name="diligence[]" id="{{ $questions[4]->id }}" value="{{ $diligence->description }}" >
                                </div>
                                <div class="col-xs-2">
                                    @if($diligence->file_name != null)
                                        <a href="{{ asset('storage/'. $diligence->file_name) }}" download="{{$diligence->file_name_unhashed}}"><span class="glyphicon glyphicon-file"></span>{{ $diligence->file_name_unhashed }}</a>
                                    @endif
                                    </div>
                                <div class='col-xs-4'>
                                    <input type="file" class="form-control" name="diligenceFile[]" id="" value="" >
                                </div>
                            </div>
                            @if ($errors->has('diligence.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligence.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('diligenceFile.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                            </span>
                        @endif 
                        @endif   
                        @if ($errors->has('diligence.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligence.*') }}</strong>
                            </span>
                        @endif 
                        @if ($errors->has('diligenceFile.*'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                            </span>
                        @endif 
                        <?php $x = $x + 1; ?>
                    @empty        
                        @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control text_input" name="hiddenDiligence[]" id="" value="" readonly>
                                    <input type="text" class="form-control generate text_input" name="diligence[]" id="{{ $questions[4]->id }}" value="" readonly>
                                </div>
                                <div class='col-xs-4'>
                                    <input type="file" class="form-control" name="diligenceFile[]" id="" value="" disabled>
                                </div>
                            </div>  
                            @if ($errors->has('diligence.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('diligence.*') }}</strong>
                                </span>
                            @endif 
                            @if ($errors->has('diligenceFile.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                                </span>
                            @endif                             
                        @else 
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control  text_input" name="hiddenDiligence[]" id="" value="">
                                    <input type="text" class="form-control generate text_input" name="diligence[]" id="{{ $questions[4]->id }}" value="" >
                                </div>
                                <div class='col-xs-4'>
                                    <input type="file" class="form-control" name="diligenceFile[]" id="" value="" >
                                </div>
                            </div>  
                            @if ($errors->has('diligence.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('diligence.*') }}</strong>
                                </span>
                            @endif 
                            @if ($errors->has('diligenceFile.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('diligenceFile.*') }}</strong>
                                </span>
                            @endif      
                        @endif 
                    @endforelse
                </div>
                @if($project->step_6_locked == 1)  
                @else
                    <div class="col-xs-12">
                        <button id="" type="button" class=" btn btn-primary standard-btn diligence-add purple btn-fix-pos">Add New</button>
                    </div>
                @endif
                <label>{{ $questions[5]->question }}</label> 
                <div id="compliance" class="block-parent">
                    <?php $x = 0; ?>
                    @forelse($compliances as $compliance)
                        @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control text_input" name="hiddenCompliance[]" id="" value="{{ $compliance->id }}" readonly>
                                    <input type="text" class="form-control generate text_input" name="compliance[]" id="{{ $questions[5]->id }}" value="{{ $compliance->description }}" readonly>
                                </div>
                                <div class="col-xs-2">
                                        <p class="remove-margin">&nbsp;</p>
                                @if($compliance->file_name != null)
                                    <a href="{{ asset('storage/'. $compliance->file_name) }}" download="{{$compliance->file_name_unhashed}}"><span class="glyphicon glyphicon-file"></span>{{ $compliance->file_name_unhashed }}</a>
                                @endif
                            </div>
                                <div class='col-xs-4'>
                                        <p class="remove-margin">&nbsp;</p>
                                    <input type="file" class="form-control" name="complianceFile[]" id="" value="" disabled>
                                </div>
                            </div>
                            @if ($errors->has('compliance.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('compliance.*') }}</strong>
                                </span>
                            @endif   
                            @if ($errors->has('complianceFile.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('complianceFile.*') }}</strong>
                                </span>
                            @endif   
                        @else 
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control  text_input" name="hiddenCompliance[]" id="" value="{{ $compliance->id }}">
                                    <input type="text" class="form-control generate text_input" name="compliance[]" id="{{ $questions[5]->id }}" value="{{ $compliance->description }}" >
                                </div>
                                <div class="col-xs-2">
                                    @if($compliance->file_name != null)
                                    <a href="{{ asset('storage/'. $compliance->file_name) }}" download="{{$compliance->file_name_unhashed}}"><span class="glyphicon glyphicon-file"></span>{{ $compliance->file_name_unhashed }}</a>
                                    @endif
                                </div>
                                <div class='col-xs-4'>
                                    <input type="file" class="form-control" name="complianceFile[]" id="" value="" >
                                </div>
                            </div>
                            @if ($errors->has('compliance.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('compliance.*') }}</strong>
                                </span>
                            @endif   
                            @if ($errors->has('complianceFile.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('complianceFile.*') }}</strong>
                                </span>
                            @endif   
                        @endif 
                        <?php $x = $x + 1; ?>
                    @empty    
                        @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor') 
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control  text_input" name="hiddenCompliance[]" id="" value="" readonly>
                                    <input type="text" class="form-control generate text_input" name="compliance[]" id="{{ $questions[5]->id }}" value="" readonly>
                                </div>
                                <div class='col-xs-4'>
                                    <input type="file" class="form-control " name="complianceFile[]" id="" value="" disabled>
                                </div>
                            </div>  
                            @if ($errors->has('compliance.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('compliance.*') }}</strong>
                                </span>
                            @endif   
                            @if ($errors->has('complianceFile.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('complianceFile.*') }}</strong>
                                </span>
                            @endif            
                        @else                              
                            <div class="row form-group">
                                <div class='col-xs-6'>
                                    <input type="hidden" class="form-control  text_input" name="hiddenCompliance[]" id="" value="">
                                    <input type="text" class="form-control generate text_input" name="compliance[]" id="{{ $questions[5]->id }}" value="" >
                                </div>
                                <div class='col-xs-4'>
                                    <input type="file" class="form-control " name="complianceFile[]" id="" value="" >
                                </div>
                            </div>  
                            @if ($errors->has('compliance.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('compliance.*') }}</strong>
                                </span>
                            @endif   
                            @if ($errors->has('complianceFile.*'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('complianceFile.*') }}</strong>
                                </span>
                            @endif 
                        @endif    
                    @endforelse
                </div>
                
                @if($project->step_6_locked == 1 || Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')  
                @else
                    <div class="col-xs-12">
                        <button id="" type="button" class=" btn btn-primary standard-btn compliance-add purple btn-fix-pos">Add New</button>
                    </div>
                @endif
                @if(Auth::user()->role == 'master' && $project->step_6_status == 3)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-master" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>  
                        <button type="submit" name="submitButton" value="approve" class="approve-step btn btn-primary standard-btn">
                            Approve Step
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'editor' && $project->step_6_status == 2)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-editor" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>
                        <button type="submit" name="submitButton" value="signatory" class="submit-to-signatory btn btn-primary standard-btn">
                            Submit to Signatory
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'author' && $project->step_6_status == 1)
                <div class="col-xs-12">
                    <button type="submit" name="submitButton" value="submit" id="step6-submit" class=" btn btn-primary standard-btn">
                        Submit
                    </button>
                    <button type="submit" name="submitButton" value="save" class="btn btn-primary standard-btn">
                        Save
                    </button> 
                </div>
                @endif
            </form>
        </div>
        @if($project->step_6_status == 2 or $project->step_6_status == 3)
            @if(Auth::user()->role == 'editor' && $project->step_6_status == 2)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif  
            @if(Auth::user()->role == 'master' && $project->step_6_status == 3) 
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif 
        @endif 
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                <div>
                    <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                    <div class="tabs">
                        <div id="active-tab" class="tab">
                            <p class="active">Active <span class="fa fa-bolt"></span></p>
                        </div>
                        <div id="archived-tab" class="tab">
                            <p>Actioned <span class="fa fa-check-square-o"></span></p>
                        </div>
                        </div>
                    <div id="notes_list"></div>
                    <div id="archived_notes_list"></div>
                </div>
            </div>
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control  text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="{{$step}}">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div> 
        <div id="dialog-confirm" title="Warning!">
                {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
                <p>Are you sure you want to permanently delete this note?</p>
            </div>   
    </div>
</div>
@endsection