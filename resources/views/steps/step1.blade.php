@extends('layouts.app')

@section('content')
<div class="container" id="step1">
    <div class="row">
    <div class="numbers-container hidden-xs hidden-sm">
        <p class="active">Step <span>1</span></p>
        <p>Step <span>2</span></p>
        <p>Step <span>3</span></p>
        <p>Step <span>4</span></p>
        <p>Step <span>5</span></p>
        <p>Step <span>6</span></p>
        <p>Step <span>7</span></p>
        <p>Step <span>8</span></p>
    </div>
    <div class="numbers-line hidden-xs hidden-sm">

    </div>

            <div class="col-xs-12 col-md-7 col-md-offset-2">
                <h1>Step 1</h1>
                <h3>Is this Joint Working?</h3>
            </div>
            <div class="col-xs-12 col-md-3">
                <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
            </div>

            @if(Session::has('message-step1'))
            <div class="col-md-10 col-md-offset-2 col-xs-12 alert-custom">
                    <div class="col-xs-10">
                        <p>{{ Session::get('message-step1') }}</p>
                    </div>
                    <div class="col-xs-2">
                            <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                    </div>
                </div>
            @endif
            <div class="col-xs-12 col-md-10 col-md-offset-2">
                <h2>{{ $project->user->firstname . ' ' . $project->user->surname . '/' . $project->project_name  }}</h2>
                <p>To be classified as Joint Working, projects need to satisfy the requirements of the ABPI Joint Working checklist.<br>
                        The checklist should be reviewed by all interested parties, including the NHS stakeholder.<br>
                        The first part of the checklist is the “Red Questions”. For a project to be viewed as a Joint Working project and proceed,
                        the answer to all red questions must be 'Yes'.</p>
            </div>
            <div class="col-xs-12 col-md-10 col-md-offset-2">
            <form method="POST" action="/{{$project->id}}/steps/{{$step}}/submit">

                {{ csrf_field() }}

                @if(isset($redQuestions))
                <table class="table table-striped">
                    <caption class="red-bg">Please complete the Red Questions</caption>
                    <tbody>   
                        @foreach ($redQuestions as $question )    
             
                            <tr>
                                <td>
                                    <p>{{$question->question_no}}</p>
                                    
                                </td>
                                <td>
                                    <label>{{ $question->question }}</label>
                                </td>
                                @if($question->answer_type == 'radio')
                                    <td style="white-space: nowrap; " class="step1-questions">        
                                        @forelse ($question->answer->where('project_id', '=', $project->id) as $answer)                                         
                                            @if($project->step_1_locked == 1)                                        
                                                @if($answer->answer_boolean == 1)
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-yes generate checked" value="yes" checked disabled>Yes
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-no" value="no" disabled>No
                                                @elseif($answer->answer_boolean == 0)
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-yes" value="yes" disabled>Yes
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-no generate checked" value="no" checked disabled>No                            
                                                @endif
                                            @else
                                                @if($answer->answer_boolean == 1)
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-yes generate checked" value="yes" checked>Yes
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-no " value="no">No
                                                @elseif($answer->answer_boolean == 0)
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-yes " value="yes">Yes
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-no generate checked" value="no" checked>No                            
                                                @endif
                                            @endif
                                        @empty
                                            @if(Auth::user()->role == "author")  
                                                <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-yes " value="yes">Yes
                                                <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}"  class="step1-red-no "value="no">No    
                                                <input class="generate" value="" style="display:none">       
                                            @else
                                                <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-red-yes " value="yes" disabled>Yes
                                                <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}"  class="step1-red-no "value="no" disabled>No  
                                                <input class="generate" value="" style="display:none">     
                                            @endif                          
                                        @endforelse         
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>                    
                @if($project->step_1_locked != 1 && Auth::user()->role == 'author') 
                    @if(Session::has('save-success'))
                    <div style="margin-top: 15px" class=" col-xs-12 alert-custom">
                        <div class="col-xs-10">
                            <p>{{ Session::get('save-success') }}</p>
                        </div>
                        <div class="col-xs-2">
                                <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                        </div>
                    </div>    
                    @endif
                    <div style="margin-top: 20px" class="col-md-12 col-xs-12 alert-custom step1-red-message">
                        <div class="col-xs-10">
                            <p>All Red Questions must be answered ‘Yes’ for the project to be classified as Joint Working. Review your options with your line manager.</p>
                        </div>
                        <div class="col-xs-2">
                                <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                        </div>
                    </div>
                    <button id="step1-proceed-btn" type="button" class="btn btn-primary standard-btn">Proceed</button>
                    <button type="submit" name="submitButton" value="save" id="save-to-hide" class="btn btn-primary standard-btn">
                        Save
                    </button>  
                @endif
                @if (count($errors) > 0)
                    <span class="help-block">
                        <strong>All Amber questions must be answered</strong>
                    </span>
                @endif
                <div id="amber-questions">
                    <table class="table table-striped">
                        <tbody>
                            <caption class="orange-bg">Please complete the Amber Questions</caption>
                            @foreach ($amberQuestions as $question)
                                <tr>
                                    <td>
                                        <p>{{$question->question_no}}</p>
                                    </td>
                                    <td>
                                        <label>{{ $question->question }}</label>
                                    </td>
                                    @if($question->answer_type == 'radio')
                                        <td style="white-space: nowrap; " class="step1-questions">
                                            @forelse ($question->answer->where('project_id', '=', $project->id) as $answer)                                       
                                                @if($project->step_1_locked == 1)     
                                                    @if($answer->answer_boolean == 1)
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-yes generate checked" value="yes" checked disabled>Yes
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-no " value="no" disabled>No
                                                    @elseif($answer->answer_boolean == 0)
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-yes " value="yes" disabled>Yes
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-no generate checked" value="no" checked disabled>No                                  
                                                    @endif
                                                @else
                                                    @if($answer->answer_boolean == 1)
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-yes generate checked" value="yes" checked>Yes
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-no " value="no">No
                                                    @elseif($answer->answer_boolean == 0)
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-yes " value="yes">Yes
                                                        <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-no generate checked" value="no" checked>No                                  
                                                    @endif                                            
                                                @endif    
                                            @empty
                                                @if(Auth::user()->role == "author")  
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-yes " value="yes">Yes
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-no" value="no">No   
                                                    <input class="generate" value="" style="display:none">        
                                                @else
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-yes " value="yes" disabled>Yes
                                                    <input type="{{ $question->answer_type }}" name="{{'Q'.$question->id }}" id="{{'Q'.$question->id }}" class="step1-amber-no" value="no" disabled>No 
                                                    <input class="generate" value="" style="display:none">       
                                                @endif                                     
                                            @endforelse
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif  
                    @if(Auth::user()->role == 'master' && $project->step_1_status == 3)

                            <button type="submit" name="submitButton" value="amend-master" class=" btn btn-primary standard-btn">
                                Submit Amendments
                            </button>

                            <button type="submit" name="submitButton" value="approve" class=" btn btn-primary standard-btn">
                                Approve Step
                            </button>

                    @elseif(Auth::user()->role == 'editor' && $project->step_1_status == 2)

                            <button type="submit" name="submitButton" value="amend-editor" class=" btn btn-primary standard-btn">
                                Submit Amendments
                            </button>

                            
                            <button type="submit" name="submitButton" value="signatory" class=" btn btn-primary standard-btn">
                                Submit to Signatory
                            </button>


                    @elseif(Auth::user()->role == 'author' && $project->step_1_locked == 0)

                        <button type="submit" name="submitButton" value="submit" id="step1-submit" class="btn btn-primary standard-btn">
                            Submit
                        </button>
    
                        <button type="submit" name="submitButton" value="save" class="btn btn-primary standard-btn">
                            Save
                        </button> 

                    @endif    
                              
                    <div style="margin-top: 15px" class=" col-xs-12 alert-custom amber-unchecked">
                        <div class="col-xs-10">
                            <p>You cannot leave an answer empty, please review any unchecked answers.</p>
                        </div>
                        <div class="col-xs-2">
                                <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
                        </div>
                    </div>  

                </div>
            </form>
            <h2>Teva’s Third Party Due Diligence Programme (TPDD)</h2>
            <p>Any vendors you engage to support the delivery of your project will need to comply with Teva’s Third Party Due Diligence
                    Programme because Teva can be held responsible for improper conduct of non–Teva partners that perform activities on
                    our behalf. We investigate the background and reputation of prospective third parties to ensure that they are capable of
                    performing the work requested and will do so ethically.<br>
                    The Teva Third Party Due Diligence Programme applies to new and reviewing relationships with Third Party
                    representatives.</p>
                    <p>If you are not sure:</p>
                    <ul>
                        <li>Whether a relationship is ‘in scope’</li>
                        <li>How to complete the online form</li>
                        <li>Why Teva needs to complete Due Diligence</li>
                    </ul>
                    <p>You must contact Local Compliance or reach the Third Party Team directly by emailing</p>
                    <p><a href="mailto:thirdpartyprogrameu@tevapharm.com">thirdpartyprogrameu@tevapharm.com</a></p>
                    <p>Please retain all correspondence for your records.</p>
        </div>
<!--         @if($project->step_1_status != 1)
            @if(Auth::user()->role != 'author')
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div>
            @else            
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'data-tool="tooltip" data-delay='{"show":"5000", "hide":"3000"}'class="add-annotation hide_annote">
                    <span class="glyphicon glyphicon-plus"></span>
                </div>
            @endif
            
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                <div>
                    <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                    <div id="notes_list"></div>
                </div>
            </div>
        @endif  -->
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="{{$step}}">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>   
        <div id="dialog-confirm" title="Warning!">
                {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
                <p>Are you sure you want to permanently delete this note?</p>
            </div> 

    </div>
</div>
@endsection