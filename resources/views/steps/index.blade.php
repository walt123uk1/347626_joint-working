@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(Auth::user()->role == 'master')
        <div class="col-xs-offset-4 col-xs-4 title-centeral">
            <h2 class="green">Master's</h2>
            <h1 class="green"><strong>Projects</strong></h1>
        </div>
        @elseif(Auth::user()->role == 'editor')
        <div class="col-xs-offset-4 col-xs-4 title-centeral">
            <h2 class="green">Editor's</h2>
            <h1 class="green"><strong>Projects</strong></h1>
        </div>
        @elseif(Auth::user()->role == 'author')
        <div class="col-xs-offset-4 col-xs-4 title-centeral">
            <h2 class="green">Author's</h2>
            <h1 class="green"><strong>Projects</strong></h1>
        </div>
        @elseif(Auth::user()->role == 'viewer')
        <div class="col-xs-offset-4 col-xs-4 title-centeral">
            <h2 class="green">Viewer's</h2>
            <h1 class="green"><strong>Projects</strong></h1>
        </div>
        @endif
        <form method="get" action="/projects/search">
            <div class="col-xs-12 col-md-offset-3 col-md-6">
                <div class="input-group">
                    <input id="search-value" name="search" type="text" class="form-control" placeholder="Search Project">
                    <span class="input-group-btn">
                        <button id="project-search" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                </div>
            </div>
        </form>
        <div class="col-xs-offset-2 col-xs-10 visible-lg visible-md visible-xl" style="margin-top: 20px; margin-bottom: 20px;">
            <div class="col-xs-2"><span class="center-text"><span class="status" style="color:red;">&#9679;</span>&nbsp;<span class="annoying-text">Not Completed</span></span></div><div class="col-xs-2"><span class="center-text"><span class="status" style="color:purple;">&#9679;</span>&nbsp;<span class="annoying-text">Pending Author</span></span></div><div class="col-xs-2"><span class="center-text"><span class="status" style="color:#e5e500;">&#9679;</span>&nbsp;<span class="annoying-text">Pending Editor</span></span></div><div class="col-xs-2"><span class="center-text"><span class="status" style="color:orange;">&#9679;</span>&nbsp;<span class="annoying-text">Pending Master</span></span></div><div class="col-xs-2"><span class="center-text"><span class="status" style="color:green;">&#9679;</span>&nbsp; <span class="annoying-text">Approved</span></span></div>
        </div>
        <div class="col-sm-offset-2 col-xs-12 col-md-8 visible-sm visible-xs" style="margin-top: 20px; margin-bottom: 20px;">
            <div class="col-xs-12 col-sm-3"><span class="center-text"><span class="status" style="color:red;">&#9679;</span>&nbsp;<span class="annoying-text">Not Completed</span></span></div><div class="col-xs-12 col-sm-3"><span class="center-text"><span class="status" style="color:purple;">&#9679;</span>&nbsp;<span class="annoying-text">Pending Author</span></span></div><div class="col-xs-12 col-sm-3"><span class="center-text"><span class="status" style="color:#e5e500;">&#9679;</span>&nbsp;<span class="annoying-text">Pending Editor</span></span></div><div class="col-xs-12 col-sm-3 visible-xs"><span class="center-text"><span class="status" style="color:orange;">&#9679;</span>&nbsp;<span class="annoying-text">Pending Master</span></span></div><div class="col-xs-12 col-sm-3 visible-xs"><span class="center-text"><span class="status" style="color:green;">&#9679;</span>&nbsp; <span class="annoying-text">Approved</span></span></div>
        </div>
        <div class="col-sm-offset-3 col-xs-12 col-md-8 visible-sm" style="margin-top: 20px; margin-bottom: 20px;">
           <div class="col-xs-12 col-sm-3"><span class="center-text"><span class="status" style="color:orange;">&#9679;</span>&nbsp;<span class="annoying-text">Pending Master</span></span></div><div class="col-xs-12 col-sm-3"><span class="center-text"><span class="status" style="color:green;">&#9679;</span>&nbsp; <span class="annoying-text">Approved</span></span></div>
        </div>
        @if(Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
            <div class="col-xs-12">
                <div class="master-bar">
                    <div class="bar-top">
                        <h4>Latest Steps Awaiting Approval</h4>
                    </div>
                    <div class="bar-bottom">
                        <div class="row">
                            <span class="hidden-xs hidden-sm col-md-3 sort">Steps <span class="glyphicon glyphicon-sort"></span></span><span class="hidden-xs hidden-sm col-md-3 sort">Project Name <span class="glyphicon glyphicon-sort"></span></span><span class="hidden-xs hidden-sm col-md-3 sort">Email <span class="glyphicon glyphicon-sort"></span></span><span class="hidden-xs hidden-sm col-md-3">View</span>
                        </div>
                    </div>
                </div>
                <ul class="list-group project-list sort-container">
                    {{-- Load in all the projects with any steps needing approval --}}
                    @forelse($projectsForApproval as $project)
                        {{-- Loop over the step statuses and display the steps if they need to be approved, miss out 1; step 1 doesn't need approving --}}
                        @for($i = 0; $i <= 8; $i++)
                            @if($project->{'step_'. $i .'_status'} == $stepStatus && $i != 1)
                                <li class="list-group-item clearfix waiting-approval">
                                    <div class="col-xs-3 sort-col-1 hidden-xs">Step {{$i}}</div>
                                    <div class="col-xs-3 sort-col-2">{{$project->project_name}}</div>
                                    <div class="col-xs-3 sort-col-3 hidden-xs">{{$project->user->email}}</div>
                                    @if($i == 0)
                                        <a href="/projects/{{ $project->id }}" class="btn standard-btn"><span class="fa fa-eye"></span></a>
                                    @else
                                        <a href="/{{$project->id}}/steps/{{$i}}" class="btn standard-btn"><span class="fa fa-eye"></span></a>
                                    @endif
                                </li>
                            @endif
                        @endfor
                    @empty
                    <div class="empty-ting">
                        <p>There are currently no steps in need of approval.</p>
                    </div>
                    @endforelse
                </ul>
            </div>
        @endif
        <div class="col-xs-12">
                <div class="master-bar">
                    <div class="bar-top">
                        @if(Auth::user()->role == 'master' ||  Auth::user()->role == 'editor')
                            <h4>All Live Projects</h4>
                        @else
                            <h4>All Projects</h4>
                        @endif
                    </div>
                    <div class="bar-bottom">
                        <div class="row">
                            <span class="hidden-xs hidden-sm col-md-9">Steps</span><span class="col-md-3 hidden-xs hidden-sm"><span style='margin-left:-10px'>Status</span><span style='margin-left:54px'>View</span><span style='margin-left:28px'>Download</span></span>
                        </div>
                    </div>
                </div>
            <ul class="current list-group project-list">

                @forelse($projects as $project)
                @if($project->step_8_status == 4)
                <li id="{{$project->id}}-clicked" class="list-group-item project-title clearfix"><h2 class="green"><strong>{{$project->project_name }}</strong> {{ $project->user->firstname . " " . $project->user->surname}}<button data-project="{{$project->id}}" class="btn standard-btn full-pdf"><span class="glyphicon glyphicon-download-alt"> </span></button><span class="pull-right glyphicon glyphicon-collapse-down toggle-icon"></span></h2> </li>
                @else
                <li id="{{$project->id}}-clicked" class="list-group-item project-title clearfix"><h2 class="green"><strong>{{$project->project_name }}</strong> {{ $project->user->firstname . " " . $project->user->surname}}<span class="pull-right glyphicon glyphicon-collapse-down toggle-icon"></span></h2> </li>
                @endif
                    <div class="steps-accordian">
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p>Project Details</p> </div>
                            @if($project->step_0_status == 1)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/projects/{{$project->id}}" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_0_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/projects/{{$project->id}}" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_0_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/projects/{{$project->id}}" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_0_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/projects/{{$project->id}}" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/projects/{{$project->id}}" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p>Step 1 </p> </div>
                            @if($project->step_1_status == 1)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/1" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_1_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/1" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_1_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/1" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_1_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/1" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/1" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p href="/{{$project->id}}/steps/2">Step 2</p></div>
                            @if($project->step_2_status == 1 )
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/2" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_2_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/2" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_2_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/2" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_2_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/2" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/2" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p href="/{{$project->id}}/steps/3">Step 3</p></div>
                            @if($project->step_3_status == 1)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/3" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_3_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/3" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_3_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/3" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_3_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/3" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/3" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p href="/{{$project->id}}/steps/4">Step 4</p></div>
                            @if($project->step_4_status == 1)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/4" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_4_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/4" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_4_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/4" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_4_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/4" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/4" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p href="/{{$project->id}}/steps/5">Step 5</p></div>
                            @if($project->step_5_status == 1)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/5" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_5_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/5" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_5_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/5" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_5_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/5" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/5" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p href="/{{$project->id}}/steps/6">Step 6</p></div>
                            @if($project->step_6_status == 1)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/6" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_6_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/6" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_6_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/6" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_6_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/6" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/6" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p href="/{{$project->id}}/steps/7">Step 7</p></div>
                            @if($project->step_7_status == 1 )
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/7" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_7_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/7" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_7_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/7" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_7_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/7" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/7" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                        <li class="list-group-item clearfix"><div class="col-xs-4  col-md-9"><p href="/{{$project->id}}/steps/8">Step 8</p></div>
                            @if($project->step_8_status == 1)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:purple;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/8" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_8_status == 2)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:#e5e500;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/8" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                            </div>
                            @elseif($project->step_8_status == 3)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:orange;">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/8" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>
                            </div>
                            @elseif($project->step_8_status == 0)
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color:red;">&#9679;</span> 
                            </div>
                            @else
                            <div class="col-xs-8  col-md-3">
                                <span class="status" style="color: green">&#9679;</span> 
                                <a href="/{{$project->id}}/steps/8" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a> 
                                <button data-url="/{{$project->id}}/steps/8" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>
                            </div>
                            @endif
                        </li>
                    </div>

                    @empty
                        <p>You have no projects, Create one by selecting 'New Project'.</p>

                @endforelse

            </ul>
        </div>
    </div>
</div>

@endsection
