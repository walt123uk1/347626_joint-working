@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            
        <div class="col-md-9 how-portal-works">
            <div  data-tool="tooltip" data-placement="right" title="Complete all the free text boxes as comprehensively as possible, please ensure the information is accurate and is in line with ABPI guidelines. The Joint Working team will receive a notification when you click submit. As Editors, they will review your content and may suggest amendments as appropriate. You will receive an email notification when the Editors have approved your progression to the next step. This will also appear on your dashboard. The Signatory will approve you to the next step once all content is agreed. The Signatory is the only person who can give interim and final approvals through each step of the Joint Working project. A similar process is undertaken for each step." class="col-xs-12 col-sm-4 how-it-works">
                <span class="glyphicon glyphicon-question-sign pull-left"></span><p class="pull-left">HOW THE<br>PORTAL WORKS</p>
            </div>
        </div>
        <div class="col-md-3">
            <img class="img-responsive logo-jw-most" src="{{asset('img/color-logo.png')}}">
        </div>

        <div class="col-xs-12 ">
                <h3 class="project-details-text">Please enter the details of your proposed Joint Working project.</h3>
                <p>Best practice time is usually 9 to 12 weeks for full project roll-out, up to step 5.</p>
        </div>
            <form class="col-xs-12" id="add_project_form" method="post" action="/projects/{{$project->id}}/update">

                {{ csrf_field() }}

                {{ method_field('PATCH') }}

                <div class="form-group{{ $errors->has('project_name') ? ' has-error' : '' }}">
                    <label for="project_name" class="col-xs-8">Name of project</label>


        @if($project->step_5_status == 4)
                <div class="col-xs-12 col-md-8">
                    @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                        <input class="form-control generate text_input" type="text" id="project_name" name="project_name" value="{{ $project->project_name }}" readonly >
                    @else
                        <input class="form-control generate text_input" type="text" id="project_name" name="project_name" value="{{ $project->project_name }}" readonly>
                    @endif    
                </div>

                @if ($errors->has('project_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('project_name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('approach_date') ? ' has-error' : '' }}">
                <label for="approach_date" class="col-xs-8">Date of initial formal approach with internal shareholders</label>
                <div class="col-xs-12 col-md-8">
                    <div class="input-group date" data-provide="datepicker" style="margin-bottom: 20px;">
                        @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                            <input style="margin-bottom: 20px;" class="form-control generate text_input" type="text" id="approach_date" name="approach_date" value="{{ $project->approach_date->format('d/m/Y') }}" readonly>
                        @else
                            <input style="margin-bottom: 20px;" id="approach_date" class="form-control generate text_input" type="text" name="approach_date" value="{{ $project->approach_date->format('d/m/Y') }}" readonly>
                        @endif
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                    </div>    
                </div>
                @if ($errors->has('approach_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('approach_date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="col-xs-6">
                <p>Projected timelines for each step</p>
            </div>

            <div class="col-xs-6">
                <p>Date of commencement:</p>
            </div>

            <div class="project_date_bar uneditable col-xs-12">

                <div class="form-group{{ $errors->has('step_1_date') ? ' has-error' : '' }}">
                        <label for="step_1_date" class="col-xs-12 col-md-6 step-text">Step 1 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>IS THIS JOINT WORKING?</strong><br> In the first stage, a checklist is completed, confirming that the project is truly Joint Working and to identify any potential issues that may arise." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate text_input" id="step_1_date" name="step_1_date" value="{{ $project->step_1_date->format('d/m/Y') }}" readonly>
                            @else
                                <input class="form-control" type="text text_input" id="step_1_date" name=" step_1_date" value="{{ $project->step_1_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_1_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_1_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar uneditable col-xs-12">

                <div class="form-group{{ $errors->has('step_2_date') ? ' has-error' : '' }}">
                        <label for="step_2_date" class="col-xs-12 col-md-6 step-text">Step 2 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>IDEA GENERATION</strong><br> A form will be completed at
                            this stage describing the initial
                            idea behind the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate text_input" id="step_2_date" name="step_2_date" value="{{ $project->step_2_date->format('d/m/Y') }}" readonly>
                            @else
                                <input class="form-control generate text_input" type="text" id="step_2_date" name="step_2_date" value="{{ $project->step_2_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_2_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_2_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar uneditable col-xs-12">

                <div class="form-group{{ $errors->has('step_3_date') ? ' has-error' : '' }}">
                        <label for="step_3_date" class="col-xs-12 col-md-6 step-text">Step 3 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT PROPOSAL</strong><br> A full project proposal with NHS partners is developed
                            at this stage, with feedback allowing the Joint Working
                            team to have input into the content of the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate text_input" id="step_3_date" name="step_3_date" value="{{ $project->step_3_date->format('d/m/Y') }}" readonly>
                            @else
                                <input class="form-control generate text_input" type="text" id="step_3_date" name="step_3_date" value="{{ $project->step_3_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_3_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_3_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar uneditable col-xs-12">

                <div class="form-group{{ $errors->has('step_4_date') ? ' has-error' : '' }}">
                    <label for="step_4_date" class="col-xs-12 col-md-6 step-text">Step 4 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT INITIATION DOCUMENT</strong><br> A template is provided for the Project Initiation
                        Document, which needs to be completed before
                        project commencement." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate text_input" id="step_4_date" name="step_4_date" value="{{ $project->step_4_date->format('d/m/Y') }}" readonly>
                            @else
                                <input class="form-control generate text_input" type="text" id="step_4_date" name="step_4_date" value="{{ $project->step_4_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_4_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_4_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar uneditable col-xs-12">

                <div class="form-group{{ $errors->has('step_5_date') ? ' has-error' : '' }}">
                        <label for="step_5_date" class="col-xs-12 col-md-6 step-text">Step 5 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>GOVERNANCE & EXECUTIVE SUMMARY</strong><br> Completion of an Executive Summary at this stage of
                            the process is necessary to satisfy ABPI regulations
                            before the project commences." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate text_input" id="step_5_date" name="step_5_date" value="{{ $project->step_5_date->format('d/m/Y') }}" readonly>
                            @else
                                <input class="form-control generate text_input" type="text" id="step_5_date" name="step_5_date" value="{{ $project->step_5_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_5_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_5_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_6_date') ? ' has-error' : '' }}">
                            <label for="step_6_date" class="col-xs-12 col-md-6 step-text">Step 6 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT MANAGEMENT</strong><br> This step is needed to input the
                                necessary Zinc job numbers and
                                purchase orders." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">

                            <div class="input-group date" data-provide="datepicker">
                                @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                    @if ($project->step_6_date =='') 
                                      <input type="text" class="form-control generate date-picker text_input" id="step_6_date" name="step_6_date" value="{{ $project->step_6_date }}" >
                                    @else
                                       <input type="text" class="form-control generate date-picker text_input" id="step_6_date" name="step_6_date" value="{{ $project->step_6_date->format('d/m/Y') }}" >
                                    @endif
                                @else
                                    <input class="form-control generate text_input" type="text" id="step_6_date" name="step_6_date" value="{{ $project->step_6_date->format('d/m/Y') }}" readonly>
                                @endif
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>

                        </div>

                        @if ($errors->has('step_6_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_6_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_7_date') ? ' has-error' : '' }}">
                            <label for="step_7_date" class="col-xs-12 col-md-6 step-text">Step 7 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT REVIEW</strong><br> This step will be ongoing during project
                                commencement to ensure that reviews and meetings
                                are being held in a timely manner, consisting of
                                reviews every 3 months during the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">

                            <div class="input-group date" data-provide="datepicker">
                                @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                    @if ($project->step_7_date =='') 
                                    <input type="text" class="form-control generate date-picker text_input" id="step_7_date" name="step_7_date" value="{{ $project->step_7_date}}" >
                                    @else
                                    <input type="text" class="form-control generate date-picker text_input" id="step_7_date" name="step_7_date" value="{{ $project->step_7_date->format('d/m/Y') }}" >
                                    @endif
                                @else
                                    <input class="form-control generate text_input" type="text" id="step_7_date" name="step_7_date" value="{{ $project->step_7_date->format('d/m/Y') }}" readonly>
                                @endif
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>

                        </div>

                        @if ($errors->has('step_7_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_7_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="project_date_bar col-xs-12">

                    <div class="form-group{{ $errors->has('step_8_date') ? ' has-error' : '' }}">
                            <label for="step_8_date" class="col-xs-12 col-md-6 step-text">Step 8 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>WRITE UP</strong><br> At completion, this
                                step allows the author
                                to record and publish the
                                findings of the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">

                            <div class="input-group date" data-provide="datepicker">
                                @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                    @if ($project->step_8_date =='') 
                                        <input type="text" class="form-control generate date-picker text_input" id="step_8_date" name="step_8_date" value="{{ $project->step_8_date }}" >
                                    @else
                                        <input type="text" class="form-control generate date-picker text_input" id="step_8_date" name="step_8_date" value="{{ $project->step_8_date->format('d/m/Y') }}" >
                                    @endif
                                @else
                                    <input class="form-control generate text_input" type="text" id="step_8_date" name="step_8_date" value="{{ $project->step_8_date->format('d/m/Y') }}" readonly>
                                @endif
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('step_8_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_8_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
        @else
                <div class="col-xs-12 col-md-8">
                    @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                        <input class="form-control generate text_input" type="text" id="project_name" name="project_name" value="{{ $project->project_name }}" >
                    @else
                        <input class="form-control generate text_input" type="text" id="project_name" name="project_name" value="{{ $project->project_name }}" readonly>
                    @endif
                </div>

                @if ($errors->has('project_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('project_name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('approach_date') ? ' has-error' : '' }}">
                <label for="approach_date" class="col-xs-8">Date of initial formal approach with internal shareholders</label>

                <div class="col-xs-12 col-md-8">
                    <div class="input-group date" data-provide="datepicker" style="margin-bottom: 20px;">
                        @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                            <input class="form-control generate date-picker text_input" type="text" id="approach_date" name="approach_date" value="{{ $project->approach_date->format('d/m/Y') }}" >
                        @else
                            <input class="text_input form-control" type="text" id="approach_date" name="approach_date" value="{{ $project->approach_date->format('d/m/Y') }}" readonly>
                        @endif
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                    </div>    
                </div>

                @if ($errors->has('approach_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('approach_date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="col-xs-6">
                <p>Projected timelines for each step</p>
            </div>

            <div class="col-xs-6">
                <p>Date of commencement:</p>
            </div>

            <div class="project_date_bar col-xs-12">

                <div class="form-group{{ $errors->has('step_1_date') ? ' has-error' : '' }}">
                        <label for="step_1_date" class="col-xs-12 col-md-6 step-text">Step 1 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>IS THIS JOINT WORKING?</strong><br> In the first stage, a checklist is completed, confirming that the project is truly Joint Working and to identify any potential issues that may arise." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group date" data-provide="datepicker">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate date-picker text_input" id="step_1_date" name="step_1_date" value="{{ $project->step_1_date->format('d/m/Y') }}" >
                            @else
                                <input class="form-control generate text_input" type="text" id="step_1_date" name="step_1_date" value="{{ $project->step_1_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                    </div>

                    @if ($errors->has('step_1_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_1_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar col-xs-12">

                <div class="form-group{{ $errors->has('step_2_date') ? ' has-error' : '' }}">
                        <label for="step_2_date" class="col-xs-12 col-md-6 step-text">Step 2 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>IDEA GENERATION</strong><br> A form will be completed at
                            this stage describing the initial
                            idea behind the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group date" data-provide="datepicker">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate date-picker text_input" id="step_2_date" name="step_2_date" value="{{ $project->step_2_date->format('d/m/Y') }}" >
                            @else
                                <input class="form-control generate text_input" type="text" id="step_2_date" name="step_2_date" value="{{ $project->step_2_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_2_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_2_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar col-xs-12">

                <div class="form-group{{ $errors->has('step_3_date') ? ' has-error' : '' }}">
                        <label for="step_3_date" class="col-xs-12 col-md-6 step-text">Step 3 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT PROPOSAL</strong><br> A full project proposal with NHS partners is developed
                            at this stage, with feedback allowing the Joint Working
                            team to have input into the content of the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group date" data-provide="datepicker">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate date-picker text_input" id="step_3_date" name="step_3_date" value="{{ $project->step_3_date->format('d/m/Y') }}" >
                            @else
                                <input class="form-control generate text_input" type="text" id="step_3_date" name="step_3_date" value="{{ $project->step_3_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_3_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_3_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar col-xs-12">

                <div class="form-group{{ $errors->has('step_4_date') ? ' has-error' : '' }}">
                        <label for="step_4_date" class="col-xs-12 col-md-6 step-text">Step 4 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT INITIATION DOCUMENT</strong><br> A template is provided for the Project Initiation Document, which needs to be completed before project commencement." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group date" data-provide="datepicker">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate date-picker text_input" id="step_4_date" name="step_4_date" value="{{ $project->step_4_date->format('d/m/Y') }}" >
                            @else
                                <input class="form-control generate text_input" type="text" id="step_4_date" name="step_4_date" value="{{ $project->step_4_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_4_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_4_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="project_date_bar col-xs-12">

                <div class="form-group{{ $errors->has('step_5_date') ? ' has-error' : '' }}">
                        <label for="step_5_date" class="col-xs-12 col-md-6 step-text">Step 5 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>GOVERNANCE & EXECUTIVE SUMMARY</strong><br> Completion of an Executive Summary at this stage of
                            the process is necessary to satisfy ABPI regulations
                            before the project commences." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                    <div class="col-xs-12 col-md-4">

                        <div class="input-group date" data-provide="datepicker">
                            @if($project->step_0_locked == 0 && Auth::user()->role == 'author')
                                <input type="text" class="form-control generate date-picker text_input" id="step_5_date" name="step_5_date" value="{{ $project->step_5_date->format('d/m/Y') }}" >
                            @else
                                <input class="form-control generate text_input" type="text" id="step_5_date" name="step_5_date" value="{{ $project->step_5_date->format('d/m/Y') }}" readonly>
                            @endif
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>

                    </div>

                    @if ($errors->has('step_5_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('step_5_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div> 
                <div class="project_date_bar uneditable col-xs-12">

                    <div class="form-group{{ $errors->has('step_6_date') ? ' has-error' : '' }}">
                            <label for="step_6_date" class="col-xs-12 col-md-6 step-text">Step 6 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT MANAGEMENT</strong><br> This step is needed to input the
                                necessary Zinc job numbers and
                                purchase orders." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">

                            <div class="input-group date">
                                <input class="form-control generate text_input" type="text" id="step_6_date" name="step_6_date" value="{{ $project->step_6_date }}" readonly>
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>

                        </div>

                        @if ($errors->has('step_6_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_6_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
    
                <div class="project_date_bar uneditable col-xs-12">

                    <div class="form-group{{ $errors->has('step_7_date') ? ' has-error' : '' }}">
                            <label for="step_7_date" class="col-xs-12 col-md-6 step-text">Step 7 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>PROJECT REVIEW</strong><br> This step will be ongoing during project
                                commencement to ensure that reviews and meetings
                                are being held in a timely manner, consisting of
                                reviews every 3 months during the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">

                            <div class="input-group date">
                                <input class="form-control generate text_input" type="text" id="step_7_date" name="step_7_date" value="{{ $project->step_7_date }}" readonly>
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>

                        </div>

                        @if ($errors->has('step_7_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_7_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
    
                <div class="project_date_bar uneditable col-xs-12">

                    <div class="form-group{{ $errors->has('step_8_date') ? ' has-error' : '' }}">
                            <label for="step_8_date" class="col-xs-12 col-md-6 step-text">Step 8 &nbsp;<span data-tool="tooltip" data-placement="right" data-html="true" title="<strong>WRITE UP</strong><br> At completion, this
                                step allows the author
                                to record and publish the
                                findings of the project." class="glyphicon glyphicon-question-sign dark"> </span><span>&nbsp; What's this?</span></label>

                        <div class="col-xs-12 col-md-4">

                            <div class="input-group date">
                                <input class="form-control generate text_input" type="text" id="step_8_date" name="step_8_date" value="{{ $project->step_8_date }}" readonly>
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </div>
                            </div>

                        </div>

                        @if ($errors->has('step_8_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('step_8_date') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
                @endif

                @if(Auth::user()->role == 'master' && $project->step_0_status == 3)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-master" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>
                        <button type="submit" name="submitButton" value="approve" class="approve-step btn btn-primary standard-btn">
                            Approve Project Inititation
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'editor' && $project->step_0_status == 2)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="amend-editor" class="submit-amendments btn btn-primary standard-btn">
                            Submit Amendments
                        </button>
                        <button type="submit" name="submitButton" value="signatory" class="submit-to-signatory btn btn-primary standard-btn">
                            Submit to Signatory
                        </button>
                    </div>
                @elseif(Auth::user()->role == 'author' && $project->step_0_status == 1)
                    <div class="col-xs-12">
                        <button type="submit" name="submitButton" value="submit" class=" btn btn-primary standard-btn">
                            Submit
                        </button>
                    </div>
                @endif
            </form>
        @if($project->step_0_status == 2 or $project->step_0_status == 3)
            @if(Auth::user()->role == 'editor' && $project->step_0_status == 2) 
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif  
            @if(Auth::user()->role == 'master' && $project->step_0_status == 3)  
                <div id="add-annotation" data-tool="tooltip" title="Add Annotation" data-placement="left" data-delay='{"show":"2000", "hide":"0"}'class="add-annotation">
                    <span class="glyphicon glyphicon-plus"></span>
                </div> 
            @endif 
        @endif    
            <span id="openNav" class="open-nav">Annotations</span>
            <div id="mySidenav" class="sidenav">
                <div>
                    <h2>Annotations <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a></h2>
                    <div class="tabs">
                        <div id="active-tab" class="tab">
                            <p class="active">Active <span class="fa fa-bolt"></span></p>
                        </div>
                        <div id="archived-tab" class="tab">
                            <p>Actioned <span class="fa fa-archive"></span></p>
                        </div>
                        </div>
                    <div id="notes_list"></div>
                    <div id="archived_notes_list"></div>
                </div>
            </div>
        <!-- Modal -->
        <div id="sticky-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Note</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/annotation/noteset">

                            {{ csrf_field() }}

                            <textarea class="form-control generate text_input_a" name="note" cols="10" rows="3"></textarea>
                            <input type="hidden" id="annote_id" name="annote_id" value="">
                            <input type="hidden" id="answer_id" name="answer_id" value="">
                            <input type="hidden" id="user" name="user" value="{{Auth::user()->role}}">
                            <input type="hidden" id="user-name" name="user-name" value="{{Auth::user()->firstname}} {{Auth::user()->surname}}">
                            <input type="hidden" id="project" name="project" value="{{$project->id}}">
                            <input type="hidden" id="step" name="step" value="0">
                            <input type="hidden" id="x_coord" name="x_coord" value="">
                            <input type="hidden" id="y_coord" name="y_coord" value="">
                            <input type="hidden" id="type" name="type" value="">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="modal_close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="" type="submit" class="btn btn-success btn-submit" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="dialog-confirm" title="Warning!">
                {{-- <span class="glyphicon glyphicon-warning-sign"></span> --}}
                <p>Are you sure you want to permanently delete this note?</p>
            </div> 
    </div>
</div>
@endsection