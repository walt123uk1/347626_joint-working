@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>Error 403</h1>
        <h2>Sorry, but you do not have the permission to access this page.</h2>
    </div>
</div>
@endsection
