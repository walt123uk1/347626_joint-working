@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>Error 404</h1>
        <h2>Sorry, but the page you are looking for cannot be found.</h2>
    </div>
</div>
@endsection
