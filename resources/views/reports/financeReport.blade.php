@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-xs-offset-4 col-xs-4 title-centeral">
            <h2 class="green">Finance</h2>
            <h1 class="green"><strong>Reports</strong></h1>
        </div>

        <form method="get" action="/projects/search">
            <div class="col-xs-12 col-md-offset-3 col-md-6">
                <div class="input-group">
                    <input id="finance-search" name="search" type="text" class="form-control" placeholder="Search Project">
                    <span class="input-group-btn">
                        <button id="project-search" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                </div>
            </div>
        </form>

        <div class="col-xs-12">
            <div class="master-bar margin-on-that">
                <div class="bar-top col-xs-12 ">
                    <div class="col-xs-10">
                        <h4>Latest Steps Awaiting Approval</h4>
                    </div>
                    <div class="col-xs-2">
                        <button class="btn finance-button f-active ">Active</button>
                        <button class="btn finance-button f-closed ">Closed</button>
                    </div>
                </div>
                <div class="bar-bottom">
                    <div class="row">
                        <span class="hidden-xs hidden-sm col-md-3 sort">Project Name <span class="glyphicon glyphicon-sort"></span></span></span><span class="hidden-xs hidden-sm col-md-2 sort">Author <span class="glyphicon glyphicon-sort"></span></span><span class="hidden-xs hidden-sm col-md-2 sort">Costs <span class="glyphicon glyphicon-sort"></span></span><span class="hidden-xs hidden-sm col-md-2 sort">Date Commenced <span class="glyphicon glyphicon-sort"></span></span><span class="hidden-xs hidden-sm col-md-2 sort">Date Completed <span class="glyphicon glyphicon-sort"></span></span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12">
            <ul class="list-group finance-reports sort-container">
                @forelse($projects as $project)

                    @if($project->step_8_status == 4)

                        <li class="list-group-item clearfix closed-projects">
                            <div class="col-xs-3 sort-col-1">
                                <p class="clickthrough" data-link="/projects#{{$project->id}}-clicked">{{$project->project_name}}</p>
                            </div>
                            <div class="col-xs-2 sort-col-2">
                                <p>{{$project->user->firstname . ' ' . $project->user->surname }}</p>
                            </div>
                            <div class="col-xs-2 sort-col-3">
                                @if($project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first() != null)
                                    <p>£{{ $project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first()}}</p>
                                @endif
                            </div>
                            <div class="col-xs-2 sort-col-4">
                                <p>{{ $project->start_date}}</p>
                            </div>
                            <div class="col-xs-2 sort-col-5">
                                <p>{{ $project->end_date}}</p>
                            </div>
                        </li>

                    @else
                        <li class="list-group-item clearfix open-projects">
                            <div class="col-xs-3 sort-col-1">
                                <p class="clickthrough" data-link="/projects#{{$project->id}}-clicked">{{$project->project_name}}</p>
                            </div>
                            <div class="col-xs-2 sort-col-2">
                                <p>{{$project->user->firstname . ' ' . $project->user->surname }}</p>
                            </div>
                            <div class="col-xs-2 sort-col-3">
                                @if($project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first() != null)
                                    <p>£{{ $project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first()}}</p>
                                @else
                                <p>N/A</p>
                                @endif
                            </div>
                            <div class="col-xs-2 sort-col-4">
                                <p>{{ $project->start_date}}</p>
                            </div>
                        </li>
                    @endif
                @empty
                    <p>There are no projects</p>
                @endforelse
            </ul>
        </div>

    </div>
</div>
@endsection
