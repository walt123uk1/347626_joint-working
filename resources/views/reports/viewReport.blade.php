@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-xs-offset-4 col-xs-4 title-centeral">
            <h2 class="green">View</h2>
            <h1 class="green"><strong>Reports</strong></h1>
        </div>

        <form method="get" action="/projects/search">
            <div class="col-xs-12 col-md-offset-3 col-md-6">
                <div class="input-group">
                    <input id="view-search" name="search" type="text" class="form-control" placeholder="Search by Project Name">
                    <span class="input-group-btn">
                        <button id="project-search" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                </div>
            </div>
        </form>

        <div class="col-xs-12">
            <div class="master-bar margin-on-that">
                <div class="bar-top col-xs-12 ">
                    <div class="col-xs-10">
                        <h4>Latest Steps Awaiting Approval</h4>
                    </div>
                    <div class="col-xs-2">
                        <button class="btn finance-button f-active ">Active</button>
                        {{--  <button class="btn finance-button closed ">Closed</button>  --}}
                    </div>
                </div>
                <div class="bar-bottom">
                    <div class="row">
                        <span class="hidden-xs hidden-sm col-md-4 sort">Project Name <span class="glyphicon glyphicon-sort"></span></span></span><span class="hidden-xs hidden-sm col-md-6 sort">Step <span class="glyphicon glyphicon-sort"></span></span><span class="hidden-xs hidden-sm col-md-2 sort">PDF</span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12">
            <ul class="list-group finance-reports sort-container">
                @forelse($currentSteps as $array)
                <li class="list-group-item clearfix open-projects">
                    <div class="col-xs-4 sort-col-1">
                    <a href="/{{$array[0]->id}}/steps/{{$array[1]}}">{{$array[0]->project_name}}</a>
                    </div>
                    <div class="col-xs-5 sort-col-2">
                        <a href="/{{$array[0]->id}}/steps/{{$array[1]}}">Step {{$array[1]}}</a>
                    </div>
                    <div class="col-xs-3 sort-col-2">
                        <button data-project="{{$array[0]->id}}" class="btn standard-btn full-pdf report-btn"><span class="glyphicon glyphicon-download-alt"> </span></button>
                    </div>
                </li>
                @empty
                    <p>No currently active steps</p>
                @endforelse
            </ul>
        </div>

    </div>
</div>
@endsection
