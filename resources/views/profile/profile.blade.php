@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        @if(Session::has('message'))
        <div class="col-md-8 col-md-offset-2 col-xs-12 alert-custom">
            <div class="col-xs-10">
                <p>{{ Session::get('message') }}</p>
            </div>
            <div class="col-xs-2">
                    <span aria-label="Close" data-dismiss="alert" class="close ok-close pull-right">OK</span>
            </div>
        </div>
        @endif

        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <div class="no-radius login-btn">
                <h2>My Profile <i class="fa fa-pencil-square" aria-hidden="true"></i></h2>
            </div>

            <form class="form-horizontal" method="POST" action="/profile/update/{{$user->id}}">

                <div class="register-form">

                    {{ csrf_field() }}

                    {{ method_field('PATCH') }}

                    <div class="form-group no-margin{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label for="firstname" class="col-xs-12 col-md-offset-2 col-md-8">Firstname</label>

                        <div class="col-md-offset-2 col-xs-12 col-md-8">
                            <input id="firstname" type="text" class="form-control" name="firstname" value="{{ $user->firstname }}" required autofocus>

                            @if ($errors->has('firstname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('surname') ? ' has-error' : '' }}">
                        <label for="surname" class="col-md-offset-2 col-xs-12 col-md-8">Surname</label>

                        <div class="col-md-offset-2 col-xs-12 col-md-8">
                            <input id="surname" type="text" class="form-control" name="surname" value="{{ $user->surname }}" required autofocus>

                            @if ($errors->has('surname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-offset-2 col-xs-12 col-md-8">E-Mail Address</label>

                        <div class="col-md-offset-2 col-xs-12 col-md-8">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('job_title') ? ' has-error' : '' }}">
                        <label for="job_title" class="col-md-offset-2 col-xs-12 col-md-8">Job Title</label>

                        <div class="col-md-offset-2 col-xs-12 col-md-8">
                            <input id="job_title" type="text" class="form-control" name="job_title" value="{{ $user->firstname}}" required>

                            @if ($errors->has('job_title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('job_title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-offset-2 col-xs-12 col-md-8">Phone Number</label>

                        <div class="col-md-offset-2 col-xs-12 col-md-8">
                            <input id="phone" type="number" class="form-control" name="phone" value="{{ $user->phone }}" required>

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-offset-2 col-xs-12 col-md-8">New Password</label>

                        <div class="col-md-offset-2 col-xs-12 col-md-8">
                            <input id="password" type="password" class="form-control" name="password" value="" >

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-margin">
                            <label for="password-confirm" class="col-md-offset-2 col-xs-12 col-md-8">Confirm Password</label>

                            <div class="col-md-offset-2 col-xs-12 col-md-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                @if ($errors->has('password_conformation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_conformation') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="col-md-2 btn btn-primary standard-btn">
                            Submit
                    </button>
            </form>
        </div>
    </div>
</div>
@endsection
