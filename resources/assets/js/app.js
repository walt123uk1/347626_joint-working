/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

$(document).on('click', '.project-title', function () {
    if (!$(this).hasClass('ignore')) {
        $(this).next('.steps-accordian').slideToggle();
        $(this).find('.toggle-icon').toggleClass('glyphicon-collapse-up');
        $(this).find('.toggle-icon').toggleClass('glyphicon-collapse-down');
    }
});


$(document).ready(function () {

    $('.date-picker').attr("autocomplete", "off");

    $('.input-group-addon').parent().find('.form-control').text().split('-').reverse().join('-');

    $('#approved-modal').modal('show');

    $('.navbar .dropdown').hover(function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    }, function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp()
    });

    $('[data-tool="tooltip"]').tooltip({
        container: "body"
    });

    //base url, server name, host name
    var url = window.location.href;
    if (url.indexOf(":") == 4) {
        var baseUrl = 'http://' + window.location.host;
    } else {
        var baseUrl = 'https://' + window.location.host;
    }

    $('#pw-reset-form').hide();
    $('.steps-accordian').hide();


    $('#pw-reset').on('click', function () {
        $('#login-form').hide();
        $('#pw-reset-form').show();
    });

    $('#tutorial-vid').click(function () {
        $('#video-loader video').attr('src', '/video/dummy.mp4');
    });
    if(!$('#step1-proceed-btn').is(':visible')){
        $('#amber-questions').show();
    }

    $('#step1-submit').on('click', function(e){

        var areAmberChecked = 0;

        $('#amber-questions input').each(function(){

            //Add each unchecked box up, if there are more than 5 unchecked boxes (there is a yes and a no radio button) then there is an option that shoulod be checked therefore show the error

            if(!$(this).is(':checked')){
                areAmberChecked ++;
            }

            if(areAmberChecked > 5){
                e.preventDefault();
                $('.amber-unchecked').show();
            }
            
        });
    });
        
    $('#step1-proceed-btn').click(function () {
        var x = 1;
        $('.step1-red-yes').each(function () {
            if ($(this).is(':checked')) {} else {
                x = 0;
            }
        });
        if (x == 1) { //all yes then show amber
            $('.step1-red-message').hide();
            $('#save-to-hide').hide();
            $('#amber-questions').show();
        } else { //not all yes then error message and no show of amber 
            $('.step1-red-message').show();
            $('#amber-questions').hide();
        }
    });
    $('.step1-red-no').click(function () {
        $('#amber-questions').hide();
    });

    $('.save-step').on('click', function () {
        localStorage.setItem('page', $('.p-active').first().text());
    });

    $('#step1-red-message-close').click(function () {
        $('.step1-red-message').hide();
    });

    $('.ok-close').click(function () {
        $('.alert-custom').hide();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    $('#button_section').show();
    var step = $('#step').val();
    $('#step' + step + '-submit').hide();
    if (localStorage.getItem('page') && localStorage.getItem('page') != 1) {
        var pageNoToShow = localStorage.getItem('page');
        $('.page_1').hide();
        $('.page_2').hide();
        $('.page_3').hide();
        $('.page_4').hide();
        $('.page_5').hide();
        $('.pg_1').removeClass('p-active');
        $('.pg_' + pageNoToShow).addClass('p-active');
        $('.page_' + pageNoToShow).show();
        if (pageNoToShow == 5) {
            var step = $('#step').val();
            if ($("#notes_list > a").length <= 0) {
                console.log('show');
                $('#step' + step + '-submit').show();
                $('.submit-to-signatory').show();
                $('.approve-step').show();
                $('.submit-amendments').hide();
            } else {
                $('#step' + step + '-submit').hide();
                $('.submit-to-signatory').hide();
                $('.approve-step').hide();
                $('.submit-amendments').show();
            }
            $('.prev').show();
            $('.next').hide();
        }
        localStorage.removeItem('page');
    } else {
        $('.page_1').show();
        $('.page_2').hide();
        $('.page_3').hide();
        $('.page_4').hide();
        $('.page_5').hide();
        $('.pg_1').addClass('p-active');
        localStorage.removeItem('page');
        $('.prev').hide();
        var step = $('#step').val();
        $('#step' + step + '-submit').hide();
    }
    $('.pagination li a').click(function () {
        var current_page = $('.p-active').html();
        var current_page_no = $(this).html();
        $('#button_section').show();
        if ($(this).html() == 'Next') {
            if (current_page != 5) {
                var next_page = parseFloat(current_page) + 1;
                $('li a').removeClass('p-active');
                $('.pg_' + next_page).addClass('p-active');
                $('.page_1').hide();
                $('.page_2').hide();
                $('.page_3').hide();
                $('.page_4').hide();
                $('.page_5').hide();
                $('.page_' + next_page).show();
                var p = parseInt(current_page) + 1;
                $('.current').html('Page ' + p + ' of 5');
                $('.prev').show();
                var step = $('#step').val();
                $('#step' + step + '-submit').hide();
            }
            if (next_page == 5 || current_page == 5) {
                $('#button_section').show();
                $('.next').hide();
                var step = $('#step').val();
                if ($("#notes_list > a").length <= 0) {
                    $('#step' + step + '-submit').show();
                    $('.submit-to-signatory').show();
                    $('.approve-step').show();
                    $('.submit-amendments').hide();
                } else {
                    $('.submit-to-signatory').hide();
                    $('.approve-step').hide();
                    $('.submit-amendments').show();
                }
                $('.prev').show();
                $('.next').hide();
            }
        } else if ($(this).html() == 'Prev') {
            if (current_page != 1) {
                $('li a').removeClass('p-active');
                var prev_page = parseFloat(current_page) - 1;
                $('.pg_' + prev_page).addClass('p-active');
                $('.page_1').hide();
                $('.page_2').hide();
                $('.page_3').hide();
                $('.page_4').hide();
                $('.page_5').hide();
                $('.page_' + prev_page).show();
                var p = parseInt(current_page) - 1;
                $('.current').html('Page ' + p + ' of 5');
                $('.next').show();
                var step = $('#step').val();
                $('#step' + step + '-submit').hide();
            }
            if (next_page == 2 || current_page == 2) {
                $('#button_section').show();
                $('.prev').hide();
                var step = $('#step').val();
                $('#step' + step + '-submit').hide();
            }
        } else {
            $('li a').removeClass('p-active');
            $('.pg_' + current_page_no).addClass('p-active');
            $('.page_1').hide();
            $('.page_2').hide();
            $('.page_3').hide();
            $('.page_4').hide();
            $('.page_5').hide();
            $('.page_' + $(this).html()).show();
            $('.current').html('Page ' + current_page_no + ' of 5');
            if ($(this).html() == 5) {
                $('#button_section').show();
                $('.next').hide();
                $('.prev').show();
                var step = $('#step').val();
                if ($("#notes_list > a").length <= 0) {
                    $('#step' + step + '-submit').show();
                    $('.submit-to-signatory').show();
                    $('.approve-step').show();
                    $('.submit-amendments').hide();
                } else {
                    $('#step' + step + '-submit').hide();
                    $('.submit-to-signatory').hide();
                    $('.approve-step').hide();
                    $('.submit-amendments').show();
                }
            } else if ($(this).html() == 1) {
                $('.prev').hide();
                $('.next').show();
                $('#button_section').show();
                var step = $('#step').val();
                $('#step' + step + '-submit').hide();
            } else {
                $('.prev').show();
                $('.next').show();
                $('#button_section').show();
                var step = $('#step').val();
                $('#step' + step + '-submit').hide();
            }
        }
    });
    

    //If errors appear on any page on step 4
    if($('.help-block').length > 0 && $('h1').text() == 'Step 4'){
        $('.page').hide();


        $('.page-errors').show();

        var pageErrorsText = '';

        $('.help-block').each( function(index){
            var firstPageWithAnError = '';

            //Calculate the first page were an error occurs and show that page, as well as adding the active class to the correct pagination button
            if(index == 0){
                firstPageWithAnError = $(this).closest('.page').attr('class').split(' ')[0];

                $('.pg_1').removeClass('p-active');
                $('.pg_' + firstPageWithAnError.replace('page_', '')).addClass('p-active');
                
                $('.' + firstPageWithAnError).show();
            }
            var pageNumber = 'Page ' + $(this).closest('.page').attr('class').split(' ')[0].replace('page_', '');

            //Make sure page is only outputted once for each page that contains an error
            if(pageErrorsText.indexOf(pageNumber) == -1){
                pageErrorsText += '<li>' + pageNumber + '</li>';
            }
    
            $('.page-errors ul').html(pageErrorsText);
        });
    }else if($('.help-block').length > 0 && $('h1').text() != 'Step 4'){
        //Position page at error
        $(document).scrollTop($('body').find('.help-block').offset().top - 300);

    }

    $(document).on('click', '.diligence-add', function () {
        $('#diligence').append("<div class='row'><div class='col-xs-6'> <input type='text' name='diligence[]' class='form-control text_input'></div><div class='col-xs-6'> <input type='file' name='diligenceFile[]' class='form-control text_input'></div><input type='hidden' name='hiddenDiligence[]' value=''></div>");
    });
    $(document).on('click', '.compliance-add', function () {
        $('#compliance').append("<div class='row'><div class='col-xs-6'><input type='text' name='compliance[]' class='form-control text_input'></div><div class='col-xs-6'><input type='file' name='complianceFile[]' class='form-control text_input'></div><input type='hidden' name='hiddenCompliance[]' value=''></div>");
    });
    $(document).on('click', '.review-add', function () {

        $('#review').append("<div class='row'><div class='col-xs-6'><input type='hidden' name='review_id[]' class='form-control'><div class='input-group date' data-provide='datepicker'><input type='text' class='form-control generate date-picker' name='review[]'><div class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></div></div></div><label class='col-xs-6'>Attended <select name='review_attended[]' class='step7_check'><option value='0' selected></option><option value='yes' disabled>Yes</option><option value='no'disabled>No</option></select></label></div>");

        if ($('.date-picker[readonly]').length == 0) {
            $('.date-picker').datepicker({
                dateFormat: 'dd/mm/yy',
            });
        }
    });
    $(document).on('click', '.zinc-add', function () {
        $('#zinc').append("<div class='row'><div class='col-xs-6'><input type='hidden' name='zinc_id[]' class='form-control'><input type='text' name='zinc[]' class='form-control'></div></div>");
    });
    $(document).on('click', '.po-add', function () {
        $('#po').append("<div class='row'><div class='col-xs-3'><input type='hidden' class='form-control' name='po_id[]' id='' value='' ><input type='text' class='form-control' name='po[]' id='' value='' ></div><div class='col-xs-3'><input type='text' class='form-control' name='po_who[]' id='' value='' ></div><div class='col-xs-6'><input type='text' class='form-control' name='po_purpose[]' id='' value='' ></div></div>");
    });

    $('textarea').keyup(function () {
        var length = $(this).val().length;
        if (length >= 1) {
            $('#char_left_' + $(this).attr('id')).html('Characters : ' + length);
        }
    });

    var annote = 0;
    $('#add-annotation').on('click', function () {
        if (annote == 0) {
            $('body').css({
                'cursor': 'url(/img/black-cursor.png) 0 32, auto'
            });
            $('.text_input').css({
                'cursor': 'url(/img/green-cursor.png) 0 32, auto'
            });
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                $('body').css({
                    'cursor': 'url(/img/black-cursor.cur), auto'
                });
                $('.text_input').css({
                    'cursor': 'url(/img/green-cursor.cur), auto'
                });
            }
            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                //alert();
                $('body').css({
                    'cursor': 'url(/img/black-cursor.cur), auto'
                });
                $('.text_input').css({
                    'cursor': 'url(/img/green-cursor.cur), auto'
                });
            }
            annote = 1;
        } else {
            $('body').css('cursor', '');
            $('.text_input').css('cursor', '');
            annote = 0;
        }
        $('.live').show();
        $('.archived').hide();
        $('.text_input').attr('data-toggle', 'modal');
        $('.text_input').attr('data-target', '#sticky-modal');
        document.getElementById("mySidenav").style.right = "-260px";
        document.getElementById("openNav").style.right = "-50px";
        if ( $( "#add-annotation" ).length ) {document.getElementById("add-annotation").style.right = "-22px";}
    });
    $('.text_input').on('click', function (e) {
        e.preventDefault();
        $('.container').css('cursor', '');
        $('.text_input').css('cursor', '');
        if (annote == 1) {
            var offset = $(this).offset();
            var absoluteX = (e.pageX - offset.left - 8);
            var absoluteY = (e.pageY - offset.top - 26);
            $(this).parent().prepend(
                '<span id="temp" style="display : none" class="stooltip fa fa-comment-o live"></span>'
            );
            $('#annote_id').attr('value', '');
            $('#answer_id').attr('value', $(this).attr('id'));
            $('#position').attr('value', $(this).prop('selectionStart'));
            $('#x_coord').attr('value', absoluteX);
            $('#y_coord').attr('value', absoluteY);
            $('#type').attr('value', 'new');
            $('body').css('cursor', '');
            $('.text_input').css('cursor', '');
            $("textarea[name='note']").val('');
            annote = 0;
        }
    });
    $('#sticky-modal').on('shown.bs.modal', function () {
        window.setTimeout(function () {
            $("textarea[name='note']").focus();
        }.bind(this), 100);
    })
    $('#sticky-modal').on('hidden.bs.modal', function () {
        $('.text_input').removeAttr('data-toggle', 'modal');
        $('.text_input').removeAttr('data-target', '#sticky-modal');
    });

    $("#modal_close").click(function (e) {
        $('#temp').remove();
    });
    $("#sticky-modal .close").click(function(e) {
        $('#temp').remove();
    });
    $(".btn-submit").click(function (e) {
        e.preventDefault();
        var annote_id = $("input[name=annote_id]").val();
        var answer_id = $("input[name=answer_id]").val();
        var position = $("input[name=position]").val();
        var project = $("input[name=project]").val();
        var step = $("input[name=step]").val();
        var note = $("textarea[name=note]").val();
        var x_coord = $("input[name=x_coord]").val();
        var y_coord = $("input[name=y_coord]").val();
        var type = $("input[name=type]").val();
        if (type == 'edit') {
            $.ajax({
                type: 'PUT',
                url: '/annotation/update/' + annote_id,
                data: {
                    note: note
                },
                success: function (data) {}
            });
        }
        if (type == 'new') {
            // alert("Answer: " + answer_id + " project: " + project + " note: " + note + $('[name="_token"]').val());
            $.ajax({
                type: 'POST',
                url: '/annotation/noteset',
                data: {
                    answer_id: answer_id,
                    posn_x: x_coord,
                    posn_y: y_coord,
                    note: note,
                    project: project,
                    step: step
                },
                success: function (data) {
                    $('#temp').attr('id', 'annote_' + data.id);
                    $('#annote_' + data.id).css('position', 'absolute');
                    $('#annote_' + data.id).css('top', parseInt(y_coord));
                    $('#annote_' + data.id).css('left', parseInt(x_coord));
                    $('#annote_' + data.id).css('z-index', 100);
                    $('#annote_' + data.id).css('display', 'inline');
                    if (user == 'author') { //no edit or delete function here
                        $('#notes_list').append(
                            '<a href="#annote_' + data.id + '"><div data-id="annote_text_' + data.id + '" class="annotation-block annote_text not-select" id="annote_div_' + data.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + data.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + data.note + '</p></div></a>'
                        );
                    } else {
                        $('#notes_list').append(
                            '<a href="#annote_' + data.id + '"><div data-id="annote_text_' + data.id + '" class="annotation-block annote_text not-select" id="annote_div_' + data.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="edit_' + data.id + '" class="edit-it glyphicon glyphicon-edit" data-tool="tooltip" data-placement="left" title="Edit Annotation" data-toggle="modal" data-target="#sticky-modal"></span><span id="delete_' + data.id + '" class="bin glyphicon glyphicon-trash" data-tool="tooltip" data-placement="left" title="Delete Annotation"></span></div><p>' + data.note + '</p></div></a>'
                        );
                    }
                    var step = $('#step').val();
                    if ($("#notes_list > a").length <= 0) {
                        $('#step' + step + '-submit').show();
                        $('.submit-to-signatory').show();
                        $('.approve-step').show();
                        $('.submit-amendments').hide();
                    } else {
                        $('#step' + step + '-submit').hide();
                        $('.submit-to-signatory').hide();
                        $('.approve-step').hide();
                        $('.submit-amendments').show();
                    }
                }
            });
        }
    });
    //
    $("#openNav").on('click', function () {
        $('.stooltip').removeClass('highlight_annote');
        $("#notes_list").show();
        $('.live').show();
        $('.archived').hide();
        $("#archived_notes_list").hide();
        $('#active-tab').find('p').addClass('active');
        $('#archived-tab').find('p').removeClass('active');
        $('body').css('cursor', '');
        $('.text_input').css('cursor', '');
        if ($("#openNav").css('right') == '200px') {
            $('.stooltip').removeClass('highlight_annote');
            document.getElementById("mySidenav").style.right = "-260px";
            document.getElementById("openNav").style.right = "-50px";
            if ( $( "#add-annotation" ).length ) {document.getElementById("add-annotation").style.right = "-22px";}
        } else {
            document.getElementById("mySidenav").style.right = "0px";
            document.getElementById("openNav").style.right = "200px";
            if ( $( "#add-annotation" ).length ) {document.getElementById("add-annotation").style.right = "228px";}
        }
    });

    //populate page with comments icon if a step page or projects/
    if (window.location.href.match('steps') != null || window.location.href.match('projects/') != null) {
        if (window.location.href.match('projects/new/project') != null) {
            //don't find annotations if it's a new project
        } else {
            var project = $('#project').val();
            var step = $('#step').val();
            var user = $('#user').val();
            var username = $('#user-name').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                type: 'get',

                url: baseUrl + "/annotation/noteget/" + project + "/" + step,

                data: {

                },
                success: function (data) {
                    $.each(data, function (key, value) {
                        if (value.archive == 1) { //if archived don't show icon
                            $('#' + value.answer_id).parent().prepend(
                                '<span id="annote_' + value.id + '" class="stooltip fa fa-comment-o archived" style="display: none; position: absolute; top: ' + value.posn_y + 'px; left: ' + value.posn_x + 'px; z-index: 100;"></span>'
                            );
                        } else {
                            $('#' + value.answer_id).parent().prepend(
                                '<span id="annote_' + value.id + '" class="stooltip fa fa-comment-o live" style="position: absolute; top: ' + value.posn_y + 'px; left: ' + value.posn_x + 'px; z-index: 100;"></span>'
                            );
                        }
                    });
                    //populate side panel with comments
                    $('#notes_list').html('');
                    $('#archived_notes_list').html('');
                    $.each(data, function (key, value) {
                        if (user == 'author') { //no edit or delete function here
                            if (value.archive == 0) {
                                $('#notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + value.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            } else {
                                $('#archived_notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + value.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            }
                        } else {
                            if (value.archive == 0) {
                                $('#notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="edit_' + value.id + '" class="edit-it glyphicon glyphicon-edit" data-tool="tooltip" data-placement="left" title="Edit Annotation" data-toggle="modal" data-target="#sticky-modal"></span><span id="delete_' + value.id + '" class="bin glyphicon glyphicon-trash" data-tool="tooltip" data-placement="left" title="Delete Annotation"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            } else {
                                $('#archived_notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + value.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            }
                        }
                    });
                    var step = $('#step').val();
                    if ($("#notes_list > a").length <= 0) {
                        $('#step' + step + '-submit').show();
                        $('.submit-to-signatory').show();
                        $('.approve-step').show();
                        $('.submit-amendments').hide();
                    } else {
                        $('#step' + step + '-submit').hide();
                        $('.submit-to-signatory').hide();
                        $('.approve-step').hide();
                        $('.submit-amendments').show();
                    }
                    if (step == 4 && !$('.p-active').hasClass('pg_5')) {
                        $('#step' + step + '-submit').hide();
                    }
                },
                error: function () {
                    // console.log();
                }
            });
        }
    };

    $(".form-group").on("click", ".stooltip", function (event) {
        $('.stooltip').removeClass('highlight_annote');
        $(this).addClass('highlight_annote');
        $("#notes_list").show();
        $("#archived_notes_list").hide();
        $('#active-tab').find('p').addClass('active');
        $('#archived-tab').find('p').removeClass('active');
        //populate side panel with comments
        var project = $('#project').val();
        var step = $('#step').val();
        var user = $('#user').val();
        var username = $('#user-name').val();
        // get id to identify which to highlight in side panel
        var this_id = $(this).attr('id').split("_");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: 'get',

            url: baseUrl + "/annotation/noteget/" + project + "/" + step,

            data: {

            },
            success: function (data) {
                $('#notes_list').html('');
                $('#archived_notes_list').html('');
                $.each(data, function (key, value) {
                    //Match to note
                    if (parseInt(this_id[1]) == value.id) {
                        if (user == 'author') { //no edit or delete function here
                            if (value.archive == 0) {
                                $('#notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + value.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            } else {
                                $('#archived_notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + value.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            }
                        } else {
                            if (value.archive == 0) {
                                $('#notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="edit_' + value.id + '" class="edit-it glyphicon glyphicon-edit" data-tool="tooltip" data-placement="left" title="Edit Annotation" data-toggle="modal" data-target="#sticky-modal"></span><span id="delete_' + value.id + '" class="bin glyphicon glyphicon-trash" data-tool="tooltip" data-placement="left" title="Delete Annotation"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            } else {
                                $('#archived_notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="edit_' + value.id + '" class="edit-it glyphicon glyphicon-edit" data-tool="tooltip" data-placement="left" title="Edit Annotation" data-toggle="modal" data-target="#sticky-modal"></span><span id="delete_' + value.id + '" class="bin glyphicon glyphicon-trash" data-tool="tooltip" data-placement="left" title="Delete Annotation"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            }
                        }
                        //Everyone apart from clicked
                    } else {
                        if (user == 'author') { //no edit or delete function here
                            if (value.archive == 0) {
                                $('#notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + value.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            } else {
                                $('#archived_notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="archive_' + value.id + '" class="archive-it fa fa-check-square-o"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            }
                        } else {
                            if (value.archive == 0) {
                                $('#notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="edit_' + value.id + '" class="edit-it glyphicon glyphicon-edit" data-tool="tooltip" data-placement="left" title="Edit Annotation" data-toggle="modal" data-target="#sticky-modal"></span><span id="delete_' + value.id + '" class="bin glyphicon glyphicon-trash" data-tool="tooltip" data-placement="left" title="Delete Annotation"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            } else {
                                $('#archived_notes_list').append(
                                    '<a href="#annote_' + value.id + '"><div data-id="annote_text_' + value.id + '" class="annotation-block annote_text not-select" id="annote_div_' + value.id + '"><div class="annotation-left"><h4>Note</h4></div><div class="annotation-right"><span id="edit_' + value.id + '" class="edit-it glyphicon glyphicon-edit" data-tool="tooltip" data-placement="left" title="Edit Annotation" data-toggle="modal" data-target="#sticky-modal"></span><span id="delete_' + value.id + '" class="bin glyphicon glyphicon-trash" data-tool="tooltip" data-placement="left" title="Delete Annotation"></span></div><p>' + value.note + '</p><p>' + 'Annoted by - ' + value.firstname + ' ' + value.surname + '</p></div></a>'
                                );
                            }
                        }
                    }
                });
            },
            error: function () {
                // console.log();
            }
        });
        document.getElementById("mySidenav").style.right = "0px";
        document.getElementById("openNav").style.right = "200px";
        if ( $( "#add-annotation" ).length ) {document.getElementById("add-annotation").style.right = "228px";}
    });

    $("#notes_list").on("click", ".glyphicon-edit", function (event) {
        $('.stooltip').removeClass('highlight_annote');
        document.getElementById("mySidenav").style.right = "-260px";
        document.getElementById("openNav").style.right = "-50px";
        if ( $( "#add-annotation" ).length ) {document.getElementById("add-annotation").style.right = "-22px";}
        var this_id = $(this).attr('id').split("_");
        $('#type').attr('value', 'edit');
        $('#annote_id').attr('value', this_id[1]);
        // alert($('#annote_div_'+this_id[1] +'> p:eq(0)'));
        $("textarea[name='note']").val($('#annote_div_' + this_id[1] + '> p:eq(0)').html());
    });

    $("#notes_list").on("click", ".glyphicon-trash", function (event) {
        var this_id = $(this).attr('id').split("_");
        //alert('delete -'+this_id[1]);

        $("#dialog-confirm").dialog({
            closeOnEscape: false,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Delete Note": function () {
                    $(this).dialog("close");

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        type: 'delete',

                        url: baseUrl + "/annotation/delete/" + this_id[1],

                        data: {

                        },
                        success: function (data) {
                            //remove icon
                            $('#annote_' + this_id[1]).remove();
                            //remove from list
                            $('#annote_div_' + this_id[1]).remove();
                            $("a[href='#annote_"+this_id[1]+"']").remove();
                            var step = $('#step').val();
                            if ($("#notes_list > a").length <= 0) {
                                $('#step' + step + '-submit').show();
                                $('.submit-to-signatory').show();
                                $('.approve-step').show();
                                $('.submit-amendments').hide();
                            } else {
                                $('#step' + step + '-submit').hide();
                                $('.submit-to-signatory').hide();
                                $('.approve-step').hide();
                                $('.submit-amendments').show();
                            }
                        },
                        error: function () {
                            // console.log();
                        }
                    });
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
    $("#notes_list").show();
    $("#archived_notes_list").hide();
    $("#notes_list").on("click", ".fa-check-square-o", function (event) {
        var this_id = $(this).attr('id').split("_");
        //alert('archive -'+this_id[1]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: 'put',

            url: baseUrl + "/annotation/archive/" + this_id[1],

            data: {

            },
            success: function (data) {
                var text = "<a href='#annote_" + this_id[1] + "'>" + $('#annote_div_' + this_id[1]).parent().html() + "</a>";
                $(text).appendTo("#archived_notes_list");
                $('#notes_list #annote_div_' + this_id[1]).parent().remove();
                $('#annote_' + this_id[1]).removeClass('live');
                $('#annote_' + this_id[1]).addClass('archived');
                $('#archive_notes_list').hide();
                $('#annote_' + this_id[1]).hide();
                $('#notes_list').show();
                var step = $('#step').val();
                if ($("#notes_list > a").length <= 0) {
                    $('#step' + step + '-submit').show();
                    $('.submit-to-signatory').show();
                    $('.approve-step').show();
                    $('.submit-amendments').hide();
                } else {
                    $('#step' + step + '-submit').hide();
                    $('.submit-to-signatory').hide();
                    $('.approve-step').hide();
                    $('.submit-amendments').show();
                }
            },
            error: function () {
                // console.log();
            }
        });
    });

    $("#notes_list").on("click", ".annote_text", function (event) {
        var this_id = $(this).attr('data-id').split("_");
        $('.annote_text').removeClass('select');
        $('.annote_text').addClass('not-select');
        $(this).removeClass('not-select');
        $('.archived').hide();
        $(this).addClass('select');
        $('.stooltip').removeClass('highlight_annote');
        $('#annote_' + this_id[2]).addClass('highlight_annote');
        if ($('#step').val() == 4) {
            //hide all pages & buttons
            $('.page_1').hide();
            $('.page_2').hide();
            $('.page_3').hide();
            $('.page_4').hide();
            $('.page_5').hide();
            $('#step4-submit').hide();
            //show the page with the comment on
            $('#annote_' + this_id[2]).parent().parent().show();
            //make pagination correct            
            var this_page = $('#annote_' + this_id[2]).parent().parent().attr('class').split(' ')[0].split('_');
            $('li a').removeClass('p-active');
            $('.pg_' + this_page[1]).addClass('p-active');
            $('.current').html('Page ' + this_page[1] + ' of 5');
            if (this_page[1] == 5) {
                $('#step4-submit').show();
            }
        }
    });

    $("#archived_notes_list").on("click", ".annote_text", function (event) {
        var this_id = $(this).attr('data-id').split("_");
        $('.live').hide();
        $('.archived').hide();
        $('.annote_text').removeClass('select');
        $('.annote_text').addClass('not-select');
        $(this).removeClass('not-select');
        $(this).addClass('select');
        $('.stooltip').removeClass('highlight_annote');
        $('#annote_' + this_id[2]).show();
        $('#annote_' + this_id[2]).addClass('highlight_annote');
        if ($('#step').val() == 4) {
            //hide all pages & buttons
            $('.page_1').hide();
            $('.page_2').hide();
            $('.page_3').hide();
            $('.page_4').hide();
            $('.page_5').hide();
            $('#step4-submit').hide();
            //show the page with the comment on
            $('#annote_' + this_id[2]).parent().parent().show();
            //make pagination correct by splitting on the first class which contains the number needed to highlight the correct page         
            var this_page = $('#annote_' + this_id[2]).parent().parent().attr('class').split(' ')[0].split('_');
            $('li a').removeClass('p-active');
            // alert(this_page);
            $('.pg_' + this_page[1]).delay(1000).addClass('p-active');
            $('.current').html('Page ' + this_page[1] + ' of 5');
            console.log(this_page[1]);
            if (this_page[1] == 5) {
                $('#step4-submit').show();
            }
        }
    });

    $('#active-tab').click(function () {
        $(this).find('p').addClass('active');
        $('#archived-tab').find('p').removeClass('active');
        $('#notes_list').show();
        $('#archived_notes_list').hide();
        $('.live').show();
        $('.archived').hide();
        $('.stooltip').removeClass('highlight_annote');
        $('.annotation-block').removeClass('select not-select');
    });

    $('#archived-tab').click(function () {
        $(this).find('p').addClass('active');
        $('#active-tab').find('p').removeClass('active');
        $('#notes_list').hide();
        $('#archived_notes_list').show();
        $('.live').hide();
        $('.archived').hide();
        $('.stooltip').removeClass('highlight_annote');
        $('.annotation-block').removeClass('select');
    });


    /* Set the width of the side navigation to 0 */
    $("#closeNav").on('click', function () {
        $('.stooltip').removeClass('highlight_annote');
        $('.live').show();
        $('.archived').hide();
        document.getElementById("mySidenav").style.right = "-260px";
        document.getElementById("openNav").style.right = "-50px";
        if ( $( "#add-annotation" ).length ) {document.getElementById("add-annotation").style.right = "-22px";}
    });


    $('.finance-button.f-active').click(function () {
        $('.open-projects').css('display', 'block');
        $('.closed-projects').hide();
    });

    $('.finance-button.f-closed').click(function () {
        $('.closed-projects').css('display', 'block');
        $('.open-projects').hide();
    });

    if ($('.date-picker').length) {
        if ($('.date-picker[readonly]').length == 0) {
            $('.date-picker').datepicker({
                dateFormat: 'dd/mm/yy',
            });
        }
    }

    $('.input-group-addon').click(function () {
        if ($(this).closest('.date').find('.date-picker[readonly]').length == 0) {
            $(this).closest('.date').find('.date-picker').datepicker('show');
        }
    });

    $('#search-value').on('keyup', function () {
        $('.current.list-group.project-list li').remove();
        if ($('#search-value').val() == '' || $('#search-value').val() == ' ') {
            $('.current.list-group.project-list li').remove();
        } else {
            $('.current.list-group.project-list li').remove();

            var $value = $('#search-value').val();

            $.ajax({


                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                type: 'get',

                url: baseUrl + "/projects/search",

                data: {
                    'search': $value
                },

                success: function (data) {
                    $('.current.list-group.project-list li').remove();


                    $.each(data, function (key, value) {


                        var step_0_status = '';
                        if (value.step_0_status == 1) {

                            step_0_status = '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="/projects/' + value.id + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';


                        } else if (value.step_0_status == 0) {
                            step_0_status = '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';
                        } else if (value.step_0_status == 2) {
                            step_0_status = '<div class="col-xs-3">' +
                                '<span class="status" style="color:#e5e500;">&#9679;</span>' +
                                '<a href="/projects/' + value.id + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_0_status == 3) {
                            step_0_status = '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="/projects/' + value.id + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_0_status = '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="/projects/' + value.id + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/projects/' + value.id + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_1_status = '';
                        if (value.step_1_status == 0) {

                            step_1_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 1 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_1_status == 1) {

                            step_1_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 1 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: purple">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/1' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';

                        } else if (value.step_1_status == 2) {

                            step_1_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 1 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/1' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';

                        } else if (value.step_1_status == 3) {

                            step_1_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 1 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: orange">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/1' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';

                        } else {

                            step_1_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 1 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/1' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/1' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_2_status = '';
                        if (value.step_2_status == 0) {

                            step_2_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 2 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_2_status == 1) {
                            step_2_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 2 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/2' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_2_status == 2) {
                            step_2_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 2 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/2' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_2_status == 3) {
                            step_2_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 2 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/2' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_2_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 2 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/2' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/2' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_3_status = '';
                        if (value.step_3_status == 0) {

                            step_3_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 3 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_3_status == 1) {
                            step_3_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 3 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/3' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_3_status == 3) {
                            step_3_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 3 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/3' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_3_status == 3) {
                            step_3_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 3 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/3' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_3_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 3 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/3' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/3' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_4_status = '';
                        if (value.step_4_status == 0) {

                            step_4_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 4 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_4_status == 1) {
                            step_4_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 4 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/4' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_4_status == 4) {
                            step_4_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 4 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/4' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_4_status == 3) {
                            step_4_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 4 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/4' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_4_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 4 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/4' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/4' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_5_status = '';
                        if (value.step_5_status == 0) {

                            step_5_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 5 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_5_status == 1) {
                            step_5_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 5 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/5' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_5_status == 5) {
                            step_5_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 5 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/5' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_5_status == 3) {
                            step_5_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 5 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/5' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_5_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 5 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/5' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/5' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_6_status = '';
                        if (value.step_6_status == 0) {

                            step_6_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 6 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_6_status == 1) {
                            step_6_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 6 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/6' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_6_status == 6) {
                            step_6_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 6 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/6' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_6_status == 3) {
                            step_6_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 6 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/6' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_6_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 6 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/6' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/6' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_7_status = '';
                        if (value.step_7_status == 0) {

                            step_7_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 7 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_7_status == 1) {
                            step_7_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 7 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/7' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_7_status == 7) {
                            step_7_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 7 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/7' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_7_status == 3) {
                            step_7_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 7 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/7' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_7_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 7 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/7' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/7' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }

                        var step_8_status = '';
                        if (value.step_8_status == 0) {

                            step_8_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 8 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:red;">&#9679;</span>' +
                                '</div>';

                        } else if (value.step_8_status == 1) {
                            step_8_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 8 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:purple;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/8' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_8_status == 8) {
                            step_8_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 8 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: #e5e500;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/8' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else if (value.step_8_status == 3) {
                            step_8_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 8 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color:orange;">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/8' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '</div>';
                        } else {
                            step_8_status = '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Step 8 </p> </div>' +
                                '<div class="col-xs-3">' +
                                '<span class="status" style="color: green">&#9679;</span>' +
                                '<a href="' + value.id + '/steps/8' + '" class="btn standard-btn"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                                '<button data-url="/' + value.id + '/steps/8' + '" class="btn standard-btn pdf-gen-button"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                                '</div>';
                        }


                        $('.current.list-group.project-list').append('<li class="list-group-item project-title clearfix"><h2 class="green"><strong>' + value.project_name + '</strong> ' + value.firstname + ' ' + value.surname + '<span class="pull-right glyphicon glyphicon-collapse-down"></span></h2></li>' +
                            '<div class="steps-accordian">' +
                            '<li class="list-group-item clearfix"><div class="col-xs-9"><p>Project Details</p> </div>' +
                            step_0_status + '</li>' +
                            step_1_status + '</li>' +
                            step_2_status + '</li>' +
                            step_3_status + '</li>' +
                            step_4_status + '</li>' +
                            step_5_status + '</li>' +
                            step_6_status + '</li>' +
                            step_7_status + '</li>' +
                            step_8_status + '</li></div>'
                        );


                        console.log(value);
                    });

                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }

            });
        }

    });

    $('#master-search').on('keyup', function () {
        $('.current.list-group.project-list li').remove();
        if ($('#master-search').val() == '' || $('#master-search').val() == ' ') {
            $('.current.list-group.project-list li').remove();
        } else {
            $('.current.list-group.project-list li').remove();

            var $value = $('#master-search').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                type: 'get',

                url: baseUrl + "/users/search",

                data: {
                    'search': $value
                },

                success: function (data) {
                    $('.current.list-group.project-list li').remove();


                    $.each(data, function (key, value) {

                        $('.current.list-group.project-list').append('<li class="list-group-item project-title ignore">' +
                            '<a href="/master/user-admin/edit-user/1">' + value.firstname + ' ' + value.surname + '<span class=" pull-right glyphicon glyphicon-pencil"></span></a>&nbsp;' +
                            '</li>'
                        );

                        console.log(value);
                    });

                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }

            });
        }
    });

    //Report Search
    $('#view-search').on('keyup', function () {
        $('.finance-reports li').remove();
        if ($('#view-search').val() == '' || $('#view-search').val() == ' ') {
            $('.finance-reports li').remove();
        } else {
            $('.finance-reports li').remove();

            var $value = $('#view-search').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                type: 'get',

                url: baseUrl + "/reports/view/search",

                data: {
                    'search': $value
                },

                success: function (data) {
                    $('.finance-reports li').remove();


                    $.each(data, function (key, value) {

                        $('.finance-reports').append('<li class="list-group-item clearfix open-projects">'+
                        '<div class="col-xs-4 sort-col-1">' +
                        '<a href="/' + value[0].id + '/steps/'+ value[1] +'">'+ value[0].project_name +'</a>' +
                        '</div>' +
                        '<div class="col-xs-5 sort-col-2">' +
                        '<a href="/' + value[0].id + '/steps/'+ value[1] +'">Step '+ value[1] +'</a>' +
                        '</div>' +
                        '<div class="col-xs-3 sort-col-2">' +
                        '<button data-project="/' + value[0].id + '" class="btn standard-btn full-pdf report-btn"><span class="glyphicon glyphicon-download-alt"> </span></button>' +
                        '</div>' +
                        '</li>'
                        );

                        console.log(value);
                    });

                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }

            });
        }
    });

    $('#finance-search').on('keyup', function () {
        $('.finance-reports li').remove();
        if ($('#finance-search').val() == '' || $('#finance-search').val() == ' ') {
            $('.finance-reports li').remove();
        } else {
            $('.finance-reports li').remove();

            var $value = $('#finance-search').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                type: 'get',

                url: baseUrl + "/reports/view/search-finance",

                data: {
                    'search': $value
                },

                success: function (data) {
                    $('.finance-reports li').remove();


                    $.each(data, function (key, value) {

                        console.log(value);

                        if(value[0].step_8_status == 4){
                            $('.finance-reports').append('<li class="list-group-item clearfix closed-projects">' +
                            '<div class="col-xs-3 sort-col-1">' +
                                '<p class="clickthrough" data-link="/projects#' + value[0].id + '-clicked">' + value[0].project_name + '</p>' +
                            '</div>' +
                            '<div class="col-xs-2 sort-col-2">' +
                                '<p>' + value[0].firstname + ' ' + value[0].surname +'</p>' +
                            '</div>' +
                            '<div class="col-xs-2 sort-col-3 append-costs">' +
                            '<p>' + value[1] + '</p>' +
                                // @if($project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first() != null)
                                //    ' <p>£{{ $project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first()}}</p>' +
                                // @endif
                           ' </div>' +
                           ' <div class="col-xs-2 sort-col-4">' +
                               '<p>' + value[0].start_date + '</p>' +
                            '</div>' +
                            '<div class="col-xs-2 sort-col-5">' +
                                '<p>' + value[0].end_date + '</p>' +
                           ' </div>' +
                        '</li>'
                        );

                        $('.append-costs').append('<p>' + value[0].answer_text + '</p>');

                        }else{
                            $('.finance-reports').append('<li class="list-group-item clearfix open-projects">' +
                            '<div class="col-xs-3 sort-col-1">' +
                                '<p class="clickthrough" data-link="/projects#' + value[0].id + '-clicked">' + value[0].project_name + '</p>' +
                            '</div>' +
                            '<div class="col-xs-2 sort-col-2">' +
                                '<p>' + value[0].firstname + ' ' + value[0].surname +'</p>' +
                            '</div>' +
                            '<div class="col-xs-2 sort-col-3 append-costs">' +
                                '<p>' + value[1]+ '</p>' +
                                // @if($project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first() != null)
                                //    ' <p>£{{ $project->answer->where('project_id', $project->id)->where('question_id', 28)->pluck('answer_text')->first()}}</p>' +
                                // @endif
                           ' </div>' +
                           ' <div class="col-xs-2 sort-col-4">' +
                               '<p>' + value[0].start_date + '</p>' +
                            '</div>' +
                            '<div class="col-xs-2 sort-col-5">' +
                                '<p>' + value[0].end_date + '</p>' +
                           ' </div>' +
                        '</li>'
                        );

                        // $('.append-costs').append('<p>' + value.answer_text + '</p>');
                        }

                    });

                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }

            });
        }
    });

    $('.sort').click(function () {

        //Get the number of the element clicked, this will then be matched up to the sort-col-number
        var columnNumberGroup = $(this).index() + 1;

        var itemsToSort = [];

        //This determines which of the grouped columns will be added into the array to be sorted
        $('.sort-col-' + columnNumberGroup).each(function (index) {

            itemsToSort.push($(this).text());

        });

        var sortedItems = '';

        //Sort or reverse sort
        if ($(this).hasClass('asc')) {

            sortedItems = itemsToSort.reverse();
            $(this).removeClass('asc');

        } else {

            sortedItems = itemsToSort.sort();
            $(this).addClass('asc');

        }

        //The array item will correspond to the text value of one of the sortable elements, find this element via :contains and place it at the top of its parent ul
        for (var i = 1; i <= itemsToSort.length; i++) {

            var elementToOrder = $('.sort-col-' + columnNumberGroup + ':contains("' + sortedItems[i] + '")').parent();
            $('.sort-container').append(elementToOrder);
        }
    });

    //Clickthrough acordian
    $(document).on('click', '.clickthrough', function () {
        var addressValue = $(this).attr("data-link");
        var forStorage = addressValue.replace('/', '').replace('projects', '');
        localStorage.setItem('accordian', forStorage);
        window.location.href = baseUrl + addressValue;
    });

    if (localStorage.getItem('accordian')) {
        $(localStorage.getItem('accordian')).next('.steps-accordian').slideToggle().delay(4000);

        localStorage.removeItem('accordian');
    }

    if ($('textarea').length) {
        autosize($('textarea'));
    }

    $(document).on('click', '.pdf-gen-button', function (e) {
        e.stopPropagation();
        var projectName = $(this).closest('.steps-accordian').prev('li').find('h2').text();

        //Get the data-url attribute from the clicked item and combine with base URl at top of page
        var targetUrl = baseUrl + $(this).data('url');

        $.ajax({
            url: targetUrl,
            type: 'GET',
            success: function (data) {

                var columns = [{
                    title: "",
                    dataKey: "id"
                }, ];

                var rows = [];

                //New jsPDF instance
                var pdf = new jsPDF('p');

                //Find step header
                if ($(data).find('h3').length) {
                    pdf.text(10, 10, $(data).find('h3').text());
                } else {
                    pdf.text(10, 10, 'Project Details');
                }

                //Find which step is being requested
                var stepNumber = targetUrl.substr(targetUrl.length - 1);
                var x = 0;

                //Get the values of every user input from the page 
                $(data).find('.generate').each(function (index) {

                    var text = '';

                    var answerText = '';

                    //Look for the step number in the url and also wether or not 'projects is included', the project details page has a different url stucture to the steps
                    if (stepNumber == 1 && targetUrl.indexOf('projects') == -1) {

                        if ($(this).is(':checked')) {
                            text = $(this).closest('td').prev('td').find('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                            answerText = $(this).prop('checked').toString();
                        }

                    } else if (stepNumber == 6 && targetUrl.indexOf('projects') == -1) {
                        if($(this).closest('.block-parent').attr('id') != 'po' ){
                            text = $(this).closest('.block-parent').prev('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                            answerText = $(this).val().toString();
                        }else{
                            text = $(this).closest('.block-parent').prev('.row').find('label').first().text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "") + ', ' + 
                            $(this).closest('.block-parent').prev('.row').find('label').first().next('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "") + ', ' + 
                            $(this).closest('.block-parent').prev('.row').find('label').first().next('label').next('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                            answerText = $(this).val().toString() + ', ' + $(this).parent().next('div').find('input').first().val().toString() + ', ' + $(this).parent().next('div').next('div').find('input').first().val().toString();
                        
                        }
                        x = x + 1;  console.log(x); console.log(text);
                        console.log(answerText);

                    } else if (stepNumber == 7 && targetUrl.indexOf('projects') == -1) {

                        text = 'Review Date:';
                        answerText = $(this).val().toString();

                    } else if (stepNumber != 1 && targetUrl.indexOf('projects') == -1) {

                        if (!$(this).hasClass('checkbox')) {
                            text = $(this).closest('.form-group').prev('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                            answerText = $(this).val().toString();
                        } else {
                            text = $(this).closest('.form-group').prev('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                            answerText = $(this).find('input').val();
                        }

                    } else if (targetUrl.indexOf('projects') > -1) {
                        text = $(this).closest('.form-group').find('label').text().replace("What's this?", "");
                        answerText = $(this).val().toString();
                    }

                    //Add the questions and the answers as seperate rows inside the table
                    if( text != '' ){
                        rows.push({
                            'id': text
                        });
        
                        rows.push({
                            'id': answerText
                        });
                    }

                });

                pdf.autoTableSetDefaults({
                    addPageContent: function (data) {
                        pdf.setFontSize(6);
                        pdf.text("TEVA | Joint Working. All rights reserved", 10, 290);
                        pdf.setFontSize(10);
                        pdf.text(pdf.internal.getNumberOfPages().toString(), 200, 290);
                        var imgData = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAUAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzOCA3OS4xNTk4MjQsIDIwMTYvMDkvMTQtMDE6MDk6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE3IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoyRkMyMTFFQjU5MDgxMUU4OEFBMkU2REI1QUZCM0UwNiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoyRkMyMTFFQzU5MDgxMUU4OEFBMkU2REI1QUZCM0UwNiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjJGQzIxMUU5NTkwODExRTg4QUEyRTZEQjVBRkIzRTA2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjJGQzIxMUVBNTkwODExRTg4QUEyRTZEQjVBRkIzRTA2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAEg4ODhAOFRAQFR4UERQeIxoVFRojIhkZGhkZIiceIyEhIx4nJy4wMzAuJz4+QUE+PkFBQUFBQUFBQUFBQUFBQQEUFBQWGRYbFxcbGhYaFhohGh0dGiExISEkISExPi0nJycnLT44OzMzMzs4QUE+PkFBQUFBQUFBQUFBQUFBQUFB/8AAEQgAUwCWAwEiAAIRAQMRAf/EAIkAAQADAQEBAAAAAAAAAAAAAAADBAUCAQYBAQEBAQAAAAAAAAAAAAAAAAACAQMQAAIBAwIDBQUFBwQDAAAAAAECAwARBCESMSIFQVFiohNhgbEyFHGhUjMGkcHhQnIjFfGyczaCszQRAQACAQMDBQEAAAAAAAAAAAABEQIhMRJBUSKBobEyE3H/2gAMAwEAAhEDEQA/APuKUpQKUpQKUpQKUpQKUpQKr5WXHjJdtXPyoOJqwSACTwGtfPhJs3LIbQk83gUVeGMTcztCM8piojeXr5mbkttQn+iP+Gtc/QZ3zemb991v8a3YYY4UCRiw7e8/bUlX+taY4xEJ/K/tMzLAjzM3HcK+4+B7/dfWtyORZBcaHtFevGjizC/ce0Vm5Mj45sp5j2+GoyyjKLqphsROO83DQeeJDYtr3DWiTxObK2vcdKpwY/qrvJsp4d5pNjmKzA3Xv7RXG8t60bc71o0KVVE7HFZr866X/fSq5R7WrlHta0SACToBxNcRSxzRrLEweNxdWHAik35L/wBJ+FUOkSJF0XHlkO1Ei3Mx7AK1rSoSFBYmwGpJ7qy06lnyx/UQYJfHOqFpAkrp+IJY/HWrmNlwZeKMiLVGBup4gjirCglhnhniWWFw8bfKy6g20rusyDOjj6KM2KBY0WMusCmyixOlwP3VyvVM6WMT42C0mOQCGZwjt37UI/1oNWuUkjkXdGwdbkXU3F1NiNPbVaLO+pwhk4ieox4RMfTO4GzKx1taqP6ekyDjMrxBYg8hEm4Eli5uu23Z30GzSlKCOc2iPu+NR4oHMe3QVLMu6NgOPH9lQYzgMVP83D3VUfWUzvC1SlKlRWR1sMFiddNSCfhWvVDOkib+2+q21H20q9E5RcU56XlxtjLG7ASLca6XF6myp42T00YMb62N7VlnC2gem24dzaEVaw8Dm3u4tb5V4++ufntXqnzqpj1TopGI5PaRb3EUq7tXbtty2tb2UquPxSuPxTmb8l/6T8K+dnJH6SS3DZHut3eoL1pS4fVWR4Vy0MMlxvaP+8qt2AqwX7qtrhQDCGEReAJ6djxK2t+2rUnQKEUL8thttwtWR0m2/qQT8sZD27t1uau48HqsMX00OWnogbUd4y0yL2DRgpt9lXMPBixMX6eMk8SztqzM3FjQZEX/AFM/8DfE1t4oAxogNAEUAf8AiKpL0116N/jt43+mU9S2lz7K0I02Rql77QBf7BagzeiaJljs+rm+Ir3oX/wH/ll/9jVYwcRsVZgzBvVmeYWFrCQ8K96fiNiY/oswc73e4FvnYt++gtUpSsCqk0RRty/Lx+w1bpx41sTTJi1ePJFrPoe+pfVi/EK4bGQ6qdvwrj6VvxD9lb4/xmrjKzQi2j+Y8CeyqmLivO29tEvqT/MakmgUSkE7rWA7q0lUKoUaAaCtuIjTqqp6ofpE7zf3VA8bwsDf7GFXq5kUMjA91TbbRCcmEv8AzLoRSqqmyuOwgfEUrabSVcuT/KPhsBs9ESxkX3fNta9Q9W6k2AISihy73kBBNoU+dtO64rzL/t9YwZRwlWWFj7hIvwrlolzeo5at+XFAMf2bp+d/u21iWlLKsULyn5UUsfsUXqHp88uRhQzzACSVA5C3282ul71lyZTt+mnLfnCP6dx2+pu9E1Y6hJLjwYuBitslnIiVxxSNBzMPbag1aVk5HSFigaXDllTLjG5ZDI7+oy62dWJU3+yos/NbJ/TjZakozohO0kWbeqsB76wbdUOjzSzYjPKxdvVlFz3ByAK8wMF4m+qyJXlypV5xc+ml9dqL7KzOnZs/oPhYKb8r1ZS7t+VCrObM3eT2CtG/kGYQOYAGmCn0w3yluy9ewmUwoZgFlKj1Avyhra2rOmxmxOkZY9V5ZWjkd5WPMXKcR+EaaAVB1F5R+n4nRiJCsFmvruunbQbdKyn6KrRlzkTfWWv9QHYc/wDSDt2+zupB1R/8GOoSgGRUNx2M6koOHeawatKyMbpQmgWbMllfKkAZnWR09MtrZApAFq86P9QuXnx5EhldHQbj2jbobcBpa9BbywEYuxstrkn2VPi5UOTHviYNbRrdhplYyZMDwubBhoR2Hvr5eTH6h02Uuu5QOEqaow9v8areFRrD66o55AiEX5m0Ar5k9ezyu26A/i26/G1MTF6hm5KzsWABBMz6C3cvf7tKV3OPdtov9t27AAPvFKueknp+n2UpZbP60RFDj5R0GPPG7HwE7G/3V30dScQ5DCzZUjzm/c55fKBV6SOOVDHIodG4qwDKfca9VVVQqgBQLADQACpS+cnunUW6bblyMqLJUd6W3yeZKvdZDRSYmeAWTFkPqga2jkG1m91aZhhaVZiimVRZXIG4A9gPGu62xn5nVcOLDaVJUkZlIiRSGZ3OgAA141nZWM+L+ljBJo6opYdxaQMR7r1tR4WHHJ6kcEaSfjVFDftAqWSOOVDHKodG4qwDKfcaDocBXzeDhTHGOdgnbmJLKCp+SdA55G/cf9R9JXKRxxrtjUIpJNlFhdjcnTvrBlS58Wb0fLdQUkSKRZom+eN9p0NRZ/8A1+D7Mf8A3JWwceAmQmNSZRaU7RzgC1m769aGFoxEyKYxayEAqNvDT2Vo7NfPYeM+V+l/QTV2V9o72WVmA99q+hqFkMGOy4sS3RSY4haNS3G3suawVcLqmJLhrJJKkbooEyOQrI66EEH21W6NOuRmdQmUEK7xlbix2hLA2PeBeuGzcFz6mT06T6sfyGD1GLD8L2sat9LgyF9fKyV2TZT7zHx2Io2op9tq0aNKUrBz6UV92xb99heuqUoFKUoOG3XNt1vZtt99ec/j8lKUDn8fkpz+PyUpQOfx+SnP4/JSlA5/H5Kc/j8lKUDn8fkpz+PyUpQOfx+SnP4/JSlA5/H5Kc/j8lKUDn8fkpz+PyUpQOfx+SnP4/JSlA5/H5KUpQf/2Q==";
                        pdf.addImage(imgData, 'JPEG', 180, 5, 20, 11);
                        // pdf.addImage(image,'JPEG', '10', '10', '10', '10');
                    }
                });

                pdf.autoTable(columns, rows, {
                    theme: 'plain',
                    startY: 10,
                    margin: {
                        left: 9,
                        bottom: 30
                    },
                    createdCell: function (cell, data) {
                        if (data.row.index % 2 === 0) {
                            cell.styles.fontStyle = 'bold';
                        }
                    },
                    columnStyles: {
                        id: {
                            columnWidth: 190,
                            overflow: 'linebreak'
                        },
                        theme: 'plain'
                    }
                }, {
                    styles: {
                        overflow: 'linebreak',
                        columnWidth: 'wrap',
                        theme: 'plain'
                    }
                });

                //Generate PDF
                pdf.save('form.pdf');

            }
        });

    });

    //Whole project download
    $(document).on('click', '.full-pdf', function (e) {
        e.stopPropagation();
        var projectName = $(this).parent().find('strong').text();

        var pdf = new jsPDF('p');

        // pdf.text( 10, 10, projectName);

        //Get the data-url attribute from the clicked item and combine with base URl at top of page

        var projectId = $(this).data('project');

        var urls = [baseUrl + '/projects/' + projectId, baseUrl + '/' + projectId + '/steps/1', baseUrl + '/' + projectId + '/steps/2',
            baseUrl + '/' + projectId + '/steps/3', baseUrl + '/' + projectId + '/steps/4', baseUrl + '/' + projectId + '/steps/5',
            baseUrl + '/' + projectId + '/steps/6', baseUrl + '/' + projectId + '/steps/7', baseUrl + '/' + projectId + '/steps/8'
        ];

        var stepTitles = ['Project Details', 'Is this Joint Working?', 'Idea Generation', 'Project Proposal', 'Project Initiation Document', 'Governance and Executive Summary', 'Project Management', 'Project Review', 'Write Up'];


        //New jsPDF instance
        var pageNo = 0;

        $.each(urls, function (i, url) {

            var columns = [{
                title: "",
                dataKey: "id"
            }, ];

            var rows = [];

            $.ajax({
                async: false,
                url: url,
                type: 'GET',
                success: function (data) {

                    // Add the titles to the pages, create at the start of each loop, barring the first iteration (we dont want a random blank page at the top of our legally binding all inclusive PDF)
                    if (i != 0) {
                        pdf.addPage();
                        pdf.text(10, 10, stepTitles[i]);
                    } else if (i == 0) {
                        pdf.text(10, 10, stepTitles[i]);
                    }

                    //Find which step is being requested
                    var stepNumber = url.substr(url.length - 1);
                    var x = 0;

                    //Get the values of every user input from the page 
                    $(data).find('.generate').each(function (index) {

                        var text = '';

                        var answerText = '';

                        if (stepNumber == 1 && url.indexOf('projects') == -1) {
                            text = $(this).closest('td').prev('td').find('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                            answerText = $(this).prop('value').toString();
                            // x = x + 1;  console.log(x); console.log(text);
                            // console.log($(this).prop('value').toString());

                        } else if (stepNumber == 6 && url.indexOf('projects') == -1) {

                            if($(this).closest('.block-parent').attr('id') != 'po' ){
                                text = $(this).closest('.block-parent').prev('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                                answerText = $(this).val().toString();
                            }else{
                                text = $(this).closest('.block-parent').prev('.row').find('label').first().text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "") + ', ' + 
                                $(this).closest('.block-parent').prev('.row').find('label').first().next('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "") + ', ' + 
                                $(this).closest('.block-parent').prev('.row').find('label').first().next('label').next('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                                answerText = $(this).val().toString() + ', ' + $(this).parent().next('div').find('input').first().val().toString() + ', ' + $(this).parent().next('div').next('div').find('input').first().val().toString();
                                if (answerText == ', , '){
                                    answerText = ''
                                }                           
                            }

                            
                            x = x + 1;  console.log(x); console.log(text);
                            console.log(answerText);

                        } else if (stepNumber == 7 && url.indexOf('projects') == -1) {

                            text = 'Review Date:';
                            answerText = $(this).val().toString();

                        } else if (stepNumber != 1 && url.indexOf('projects') == -1) {

                            if (!$(this).hasClass('checkbox')) {
                                text = $(this).closest('.form-group').prev('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                                answerText = $(this).val().toString();
                            } else {
                                text = $(this).closest('.form-group').prev('label').text().replace("(1000 characters max)", "").replace("(500 characters max)", "").replace("(250 characters max)", "").replace("(1000 characters maximum)", "").replace("(250 characters maximum)", "").replace("(500 characters maximum)", "");
                                answerText = $(this).find('input').val();
                            }

                        } else if (url.indexOf('projects') > -1) {
                            text = $(this).closest('.form-group').find('label').text().replace("What's this?", "");
                            answerText = $(this).val().toString();

                        } 
                    

                        if( text != "" ){
                            rows.push({
                                'id': text
                            });
        
                            rows.push({
                                'id': answerText
                            });
                        }

                    });
                    pdf.autoTableSetDefaults({
                        addPageContent: function (data) {
                            pdf.setFontSize(6);
                            pdf.text("TEVA | Joint Working. All rights reserved", 10, 290);
                            pdf.setFontSize(10);
                            pdf.text(pdf.internal.getNumberOfPages().toString(), 200, 290);
                            var imgData = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAUAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzOCA3OS4xNTk4MjQsIDIwMTYvMDkvMTQtMDE6MDk6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE3IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoyRkMyMTFFQjU5MDgxMUU4OEFBMkU2REI1QUZCM0UwNiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoyRkMyMTFFQzU5MDgxMUU4OEFBMkU2REI1QUZCM0UwNiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjJGQzIxMUU5NTkwODExRTg4QUEyRTZEQjVBRkIzRTA2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjJGQzIxMUVBNTkwODExRTg4QUEyRTZEQjVBRkIzRTA2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAEg4ODhAOFRAQFR4UERQeIxoVFRojIhkZGhkZIiceIyEhIx4nJy4wMzAuJz4+QUE+PkFBQUFBQUFBQUFBQUFBQQEUFBQWGRYbFxcbGhYaFhohGh0dGiExISEkISExPi0nJycnLT44OzMzMzs4QUE+PkFBQUFBQUFBQUFBQUFBQUFB/8AAEQgAUwCWAwEiAAIRAQMRAf/EAIkAAQADAQEBAAAAAAAAAAAAAAADBAUCAQYBAQEBAQAAAAAAAAAAAAAAAAACAQMQAAIBAwIDBQUFBwQDAAAAAAECAwARBCESMSIFQVFiohNhgbEyFHGhUjMGkcHhQnIjFfGyczaCszQRAQACAQMDBQEAAAAAAAAAAAABEQIhMRJBUSKBobEyE3H/2gAMAwEAAhEDEQA/APuKUpQKUpQKUpQKUpQKUpQKr5WXHjJdtXPyoOJqwSACTwGtfPhJs3LIbQk83gUVeGMTcztCM8piojeXr5mbkttQn+iP+Gtc/QZ3zemb991v8a3YYY4UCRiw7e8/bUlX+taY4xEJ/K/tMzLAjzM3HcK+4+B7/dfWtyORZBcaHtFevGjizC/ce0Vm5Mj45sp5j2+GoyyjKLqphsROO83DQeeJDYtr3DWiTxObK2vcdKpwY/qrvJsp4d5pNjmKzA3Xv7RXG8t60bc71o0KVVE7HFZr866X/fSq5R7WrlHta0SACToBxNcRSxzRrLEweNxdWHAik35L/wBJ+FUOkSJF0XHlkO1Ei3Mx7AK1rSoSFBYmwGpJ7qy06lnyx/UQYJfHOqFpAkrp+IJY/HWrmNlwZeKMiLVGBup4gjirCglhnhniWWFw8bfKy6g20rusyDOjj6KM2KBY0WMusCmyixOlwP3VyvVM6WMT42C0mOQCGZwjt37UI/1oNWuUkjkXdGwdbkXU3F1NiNPbVaLO+pwhk4ieox4RMfTO4GzKx1taqP6ekyDjMrxBYg8hEm4Eli5uu23Z30GzSlKCOc2iPu+NR4oHMe3QVLMu6NgOPH9lQYzgMVP83D3VUfWUzvC1SlKlRWR1sMFiddNSCfhWvVDOkib+2+q21H20q9E5RcU56XlxtjLG7ASLca6XF6myp42T00YMb62N7VlnC2gem24dzaEVaw8Dm3u4tb5V4++ufntXqnzqpj1TopGI5PaRb3EUq7tXbtty2tb2UquPxSuPxTmb8l/6T8K+dnJH6SS3DZHut3eoL1pS4fVWR4Vy0MMlxvaP+8qt2AqwX7qtrhQDCGEReAJ6djxK2t+2rUnQKEUL8thttwtWR0m2/qQT8sZD27t1uau48HqsMX00OWnogbUd4y0yL2DRgpt9lXMPBixMX6eMk8SztqzM3FjQZEX/AFM/8DfE1t4oAxogNAEUAf8AiKpL0116N/jt43+mU9S2lz7K0I02Rql77QBf7BagzeiaJljs+rm+Ir3oX/wH/ll/9jVYwcRsVZgzBvVmeYWFrCQ8K96fiNiY/oswc73e4FvnYt++gtUpSsCqk0RRty/Lx+w1bpx41sTTJi1ePJFrPoe+pfVi/EK4bGQ6qdvwrj6VvxD9lb4/xmrjKzQi2j+Y8CeyqmLivO29tEvqT/MakmgUSkE7rWA7q0lUKoUaAaCtuIjTqqp6ofpE7zf3VA8bwsDf7GFXq5kUMjA91TbbRCcmEv8AzLoRSqqmyuOwgfEUrabSVcuT/KPhsBs9ESxkX3fNta9Q9W6k2AISihy73kBBNoU+dtO64rzL/t9YwZRwlWWFj7hIvwrlolzeo5at+XFAMf2bp+d/u21iWlLKsULyn5UUsfsUXqHp88uRhQzzACSVA5C3282ul71lyZTt+mnLfnCP6dx2+pu9E1Y6hJLjwYuBitslnIiVxxSNBzMPbag1aVk5HSFigaXDllTLjG5ZDI7+oy62dWJU3+yos/NbJ/TjZakozohO0kWbeqsB76wbdUOjzSzYjPKxdvVlFz3ByAK8wMF4m+qyJXlypV5xc+ml9dqL7KzOnZs/oPhYKb8r1ZS7t+VCrObM3eT2CtG/kGYQOYAGmCn0w3yluy9ewmUwoZgFlKj1Avyhra2rOmxmxOkZY9V5ZWjkd5WPMXKcR+EaaAVB1F5R+n4nRiJCsFmvruunbQbdKyn6KrRlzkTfWWv9QHYc/wDSDt2+zupB1R/8GOoSgGRUNx2M6koOHeawatKyMbpQmgWbMllfKkAZnWR09MtrZApAFq86P9QuXnx5EhldHQbj2jbobcBpa9BbywEYuxstrkn2VPi5UOTHviYNbRrdhplYyZMDwubBhoR2Hvr5eTH6h02Uuu5QOEqaow9v8areFRrD66o55AiEX5m0Ar5k9ezyu26A/i26/G1MTF6hm5KzsWABBMz6C3cvf7tKV3OPdtov9t27AAPvFKueknp+n2UpZbP60RFDj5R0GPPG7HwE7G/3V30dScQ5DCzZUjzm/c55fKBV6SOOVDHIodG4qwDKfca9VVVQqgBQLADQACpS+cnunUW6bblyMqLJUd6W3yeZKvdZDRSYmeAWTFkPqga2jkG1m91aZhhaVZiimVRZXIG4A9gPGu62xn5nVcOLDaVJUkZlIiRSGZ3OgAA141nZWM+L+ljBJo6opYdxaQMR7r1tR4WHHJ6kcEaSfjVFDftAqWSOOVDHKodG4qwDKfcaDocBXzeDhTHGOdgnbmJLKCp+SdA55G/cf9R9JXKRxxrtjUIpJNlFhdjcnTvrBlS58Wb0fLdQUkSKRZom+eN9p0NRZ/8A1+D7Mf8A3JWwceAmQmNSZRaU7RzgC1m769aGFoxEyKYxayEAqNvDT2Vo7NfPYeM+V+l/QTV2V9o72WVmA99q+hqFkMGOy4sS3RSY4haNS3G3suawVcLqmJLhrJJKkbooEyOQrI66EEH21W6NOuRmdQmUEK7xlbix2hLA2PeBeuGzcFz6mT06T6sfyGD1GLD8L2sat9LgyF9fKyV2TZT7zHx2Io2op9tq0aNKUrBz6UV92xb99heuqUoFKUoOG3XNt1vZtt99ec/j8lKUDn8fkpz+PyUpQOfx+SnP4/JSlA5/H5Kc/j8lKUDn8fkpz+PyUpQOfx+SnP4/JSlA5/H5Kc/j8lKUDn8fkpz+PyUpQOfx+SnP4/JSlA5/H5KUpQf/2Q==";
                            pdf.addImage(imgData, 'JPEG', 180, 5, 20, 11);
                        }
                    });

                    pdf.autoTable(columns, rows, {
                        theme: 'plain',
                        startY: 10,
                        margin: {
                            left: 9,
                            bottom: 30
                        },
                        createdCell: function (cell, data) {
                            if (data.row.index % 2 === 0) {
                                cell.styles.fontStyle = 'bold';
                            }
                        },
                        columnStyles: {
                            id: {
                                columnWidth: 190,
                                overflow: 'linebreak'
                            },
                            theme: 'plain'
                        }
                    }, {
                        styles: {
                            overflow: 'linebreak',
                            columnWidth: 'wrap',
                            theme: 'plain'
                        }
                    });

                },
                error: function (err) {
                    console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
                }
            });

        });

        //Generate PDF
        pdf.save('form.pdf');

    });


});