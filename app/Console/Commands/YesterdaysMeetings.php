<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\SendPastMeetings;
use App\Project;
use App\User;

class YesterdaysMeetings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yesterdays:meetings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email of meetings yesterday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $totalPastReviews = DB::table('reviews')
        ->whereRaw('Date(review_date) = CURDATE() - 1')
        ->join('projects', 'reviews.project_id', '=', 'projects.id')
        ->select('reviews.review_date','projects.project_name','projects.id')
        ->get();

        if ($totalPastReviews->count()) {
            $allEditors = User::where('role','editor')->pluck('email');
            Mail::to( $allEditors )->send(new SendPastMeetings($totalPastReviews));
        }
    }
}
