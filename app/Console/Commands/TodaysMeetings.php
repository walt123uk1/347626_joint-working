<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\SendMeetings;
use App\Project;
use App\User;

class TodaysMeetings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'todays:meetings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email of meetings today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $totalReviews = DB::table('reviews')
        ->whereRaw('Date(review_date) = CURDATE()')
        ->join('projects', 'reviews.project_id', '=', 'projects.id')
        ->select('reviews.review_date','projects.project_name')
        ->get();
        if ($totalReviews->count()) {
            $allEditors = User::where('role','editor')->pluck('email');
            Mail::to( $allEditors )->send(new SendMeetings($totalReviews));  
        }
    }
}
