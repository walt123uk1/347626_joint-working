<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{    
    protected $guarded = ['id'];
	public function project()
    {
        return $this->belongsTo('App\Project');
    }	
    protected $fillable = [
            'project_id',
            'review_date',
            'review_attended'
    ];
    protected $dates = ['review_date'];
}
