<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compliance extends Model
{

    protected $guarded = ['id'];

    public function project(){
        return $this->belongsTo('App\Project');
    }
}
