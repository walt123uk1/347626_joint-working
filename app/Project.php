<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    // protected $dates = ['approach_date','step_1_date','step_2_date','step_3_date','step_4_date','step_5_date','step_6_date','step_7_date','step_8_date'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function review()
    {
        return $this->hasMany('App\Review');
    }
    public function diligence()
    {
        return $this->hasMany('App\Diligence');
    }
    public function compliance()
    {
        return $this->hasMany('App\Compliance');
    }
    public function zinc()
    {
        return $this->hasMany('App\Zinc');
    }
    public function po()
    {
        return $this->hasMany('App\Po');
    }
    public function question()
    {
        return $this->hasMany('App\Question');
    }
    public function answer()
    {
        return $this->hasMany('App\Answer');
    }
    protected $fillable = [
        'users_id','project_name','approach_date','start_date','end_date','step_1_status','step_1_date','step_1_locked','step_2_status','step_2_date','step_2_locked','step_3_status',
        'step_3_date','step_3_locked','step_4_status','step_4_date','step_4_locked','step_5_status','step_5_date','step_5_locked','step_6_status','step_6_date','step_6_locked',
        'step_7_status','step_7_date','step_7_locked','step_8_status','step_8_date','step_8_locked'
    ];

    protected $dates = ['approach_date','step_1_date','step_2_date','step_3_date','step_4_date','step_5_date','step_6_date','step_7_date','step_8_date'];
}
