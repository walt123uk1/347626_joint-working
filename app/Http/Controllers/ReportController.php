<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;

use App\Answer;

class ReportController extends Controller
{
    public function finance (){

        $projects = Project::all();

        return view('reports.financeReport', compact('projects'));

    }

    public function view (){

        $projects = Project::all();

        $currentSteps = array();

        //Loop over the steps of each project, if the step status is anything but 4 or 0 this step is active

        foreach ($projects as $project) {

            for($i = 0; $i <= 8; $i++){
                if($project->{'step_'.$i.'_status'} != 0 && $project->{'step_'.$i.'_status'} != 4){
                    array_push($currentSteps, array($project, $i ));
                }
            }
        }

        // dd($currentSteps);
        
        return view('reports.viewReport', compact('currentSteps'));

    }

    public function searchView(Request $request){
        
        $projects = Project::where('project_name','LIKE','%'.$request->search."%")
        ->join('users', 'projects.user_id', '=', 'users.id')
        ->select('projects.*','users.firstname','users.surname')
        ->get();

        $currentSteps = array();

        //Loop over the steps of each project, if the step status is anything but 4 or 0 this step is active

        foreach ($projects as $project) {

            for($i = 0; $i <= 8; $i++){
                if($project->{'step_'.$i.'_status'} != 0 && $project->{'step_'.$i.'_status'} != 4){
                    array_push($currentSteps, array($project, $i ));
                }
            }
        }

        return Response($currentSteps);
    }

    
    public function searchFinance(Request $request){
        
        //join projects on users firstname and surname

        $projects = Project::where('project_name','LIKE','%'.$request->search."%")
        ->join('users', 'projects.user_id', '=', 'users.id')
        ->select('projects.*','users.firstname','users.surname')
        ->get();

        $projectsArray = array();

        //Loop over results of first query and get the answer to question 28 for the specific project

        foreach($projects as $project){
            $res = Answer::where('project_id', '=', $project->id)->where('question_id', '=', '28')->pluck('answer_text')->first();
            //If result exists....
            if(count($res)){
                array_push($projectsArray, array($project, '£' . $res));
            }else{
                array_push($projectsArray, array($project, 'N/A'));
            }
            
        }

        return Response($projectsArray);
    }

}
