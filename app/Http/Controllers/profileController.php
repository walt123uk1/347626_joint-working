<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\User;
use Auth;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(User $user){

        if(Auth::user()->id == $user->id  ){

            $roles = array("author", "viewer", "editor", 'master');

            return view('profile.profile', compact('user', 'roles'));

        }else{
            
            return redirect('/projects');
            
        }

    }

    public function update(User $user, Request $request){

        if($request->password !== NULL){
            $this->validate($request, [
                'firstname' => 'required',
                'surname' => 'required',
                'email' => 'required|email|unique:users,email,'.$user->id,
                'job_title' => 'required',
                'phone' => 'required',
                'password' => 'required|confirmed',
            ]);


            $user->update(['firstname' => $request->firstname, 'surname' => $request->surname, 'email' => $request->email, 'job_title' => $request->job_title, 'phone' => $request->phone, 'password' => bcrypt($request->password)]);

        }else{
            $this->validate($request, [
                'firstname' => 'required',
                'surname' => 'required',
                'email' => 'required|email|unique:users,email,'.$user->id,
                'job_title' => 'required',
                'phone' => 'required',
            ]);


            $user->update(
                ['firstname' => $request->firstname, 'surname' => $request->surname, 'email' => $request->email, 'job_title' => $request->job_title, 'phone' => $request->phone ]
            );
        }

        Session::flash('message', 'Your profile has been updated'); 

        return redirect()->action(
            'profileController@index', ['id' => $user->id]
        );
    }
    
}
