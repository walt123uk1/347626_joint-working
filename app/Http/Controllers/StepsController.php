<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Carbon\Carbon; 

use Illuminate\Support\Facades\Mail;
use App\Mail\SendForApproval;
use App\Mail\CalendarInvite;
use App\Mail\SubmissionSuccess;
use App\Mail\ProjectSubmission;

use App\Question;
use App\Project;
use App\Answer;
use App\Annotation;
use App\Review;
use App\Diligence;
use App\Compliance;
use App\Po;
use App\Zinc;
use App\User;

use Storage;

class StepsController extends Controller
{
    //This function will redirect the user after the user becomes approved, works with the CheckApproved middleware
    public function waitingApproval(){

        if(Auth::check() && Auth::user()->approved == 0){

            return view('auth.approval');

        }else{

            return redirect('/projects');

        }
        
    }

    public function newProjectDetails(){

        if(Auth::user()->role == 'author'){

            return view('steps.addProject');

        }else{

            return redirect()->route('projects');
            
        }

    }

    public function projectDetails(Project $project) {

        return view('steps.projectDetails', compact('project'));
    }

    public function addProject(Request $request){

        $allMasters = User::where('role','master')->pluck('email');
        $allEditors = User::where('role','editor')->pluck('email');

        $this->validate($request, [
            'project_name' => 'required|min:3|max:255',
            'approach_date' => 'required|date_format:"d/m/Y"',
            'step_1_date' => 'required|date_format:"d/m/Y"',
            'step_2_date' => 'required|date_format:"d/m/Y"',
            'step_3_date' => 'required|date_format:"d/m/Y"',
            'step_4_date' => 'required|date_format:"d/m/Y"',
            'step_5_date' => 'required|date_format:"d/m/Y"',
        ]);

        $data = $request->all();
        $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
        $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
        $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
        $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
        $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
        $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');
        if ($request->step_6_date ==''){
            $data['step_6_date'] =$request->step_6_date;
        }else{
            $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
        }
        if ($request->step_7_date ==''){
            $data['step_7_date'] =$request->step_7_date;
        }else{
            $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
        }
        if ($request->step_8_date ==''){
            $data['step_8_date'] =$request->step_8_date;
        }else{
            $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
        }
        
        //$project->update($request->all());
        $project = new Project($data);
        //$project = new Project($request->all());

        $project->step_0_locked = 1;
    
        $project->step_0_status = 2;

        $project->user_id = Auth::user()->id;

        $project->start_date =  date('Y-m-d H:i:s');

        $project->save();

        Mail::to( $allEditors )->send(new SendForApproval($project, null));

        Mail::to( $project->user->email )->send(new ProjectSubmission($project));

        return redirect()->route('projects');
        
    }

    //project update
    public function updateProject(Project $project, Request $request){

        $allMasters = User::where('role','master')->pluck('email');
        $allEditors = User::where('role','editor')->pluck('email');

        if( $request->submitButton == 'submit' && Auth::user()->id == $project->user_id && Auth::user()->role == 'author' && $project->step_5_status == 4){

            //Author submit update

            $this->validate($request, [
                'project_name' => 'required|min:3|max:255',
                'approach_date' => 'required|date_format:"d/m/Y"',
                'step_1_date' => 'required|date_format:"d/m/Y"',
                'step_2_date' => 'required|date_format:"d/m/Y"',
                'step_3_date' => 'required|date_format:"d/m/Y"',
                'step_4_date' => 'required|date_format:"d/m/Y"',
                'step_5_date' => 'required|date_format:"d/m/Y"',
                'step_6_date' => 'required|date_format:"d/m/Y"',
                'step_7_date' => 'required|date_format:"d/m/Y"',
                'step_8_date' => 'required|date_format:"d/m/Y"'
            ]);
    
            $project->step_0_locked = 1;
    
            $project->step_0_status = 2;
    
            $data = $request->all();
            $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
            $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
            $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
            $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
            $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
            $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');
            if ($request->step_6_date ==''){
                $data['step_6_date'] =$request->step_6_date;
            }else{
                $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
            }
            if ($request->step_7_date ==''){
                $data['step_7_date'] =$request->step_7_date;
            }else{
                $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
            }
            if ($request->step_8_date ==''){
                $data['step_8_date'] =$request->step_8_date;
            }else{
                $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
            }

            //$project->update($request->all());

            $project->update($data);
    
            Mail::to($allEditors)->send(new SendForApproval($project, null));

            Mail::to( $project->user->email )->send(new ProjectSubmission($project));
    
            return redirect('/projects');

        }elseif( $request->submitButton == 'submit' && Auth::user()->id == $project->user_id && Auth::user()->role == 'author' ){

            //Author submit update

            $this->validate($request, [
                'project_name' => 'required|min:3|max:255',
                'approach_date' => 'required|date_format:"d/m/Y"',
                'step_1_date' => 'required|date_format:"d/m/Y"',
                'step_2_date' => 'required|date_format:"d/m/Y"',
                'step_3_date' => 'required|date_format:"d/m/Y"',
                'step_4_date' => 'required|date_format:"d/m/Y"',
                'step_5_date' => 'required|date_format:"d/m/Y"'
            ]);
    
            $project->step_0_locked = 1;
    
            $project->step_0_status = 2;
    

            $data = $request->all();
            $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
            $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
            $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
            $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
            $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
            $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');

            if ($request->step_6_date ==''){
                $data['step_6_date'] =$request->step_6_date;
            }else{
                $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
            }
            if ($request->step_7_date ==''){
                $data['step_7_date'] =$request->step_7_date;
            }else{
                $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
            }
            if ($request->step_8_date ==''){
                $data['step_8_date'] =$request->step_8_date;
            }else{
                $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
            }
            
            //$project->update($request->all());
            $project->update($data);
    
            Mail::to($allEditors)->send(new SendForApproval($project, null));

            Mail::to( $project->user->email )->send(new ProjectSubmission($project));
    
            return redirect('/projects');

        }elseif( $request->submitButton == 'signatory' && Auth::user()->role == 'editor' ){

            //Editor send to master

            $project->step_0_locked = 1;
    
            $project->step_0_status = 3;
    
            $data = $request->all();
            $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
            $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
            $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
            $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
            $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
            $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');
            if ($request->step_6_date ==''){
                $data['step_6_date'] =$request->step_6_date;
            }else{
                $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
            }
            if ($request->step_7_date ==''){
                $data['step_7_date'] =$request->step_7_date;
            }else{
                $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
            }
            if ($request->step_8_date ==''){
                $data['step_8_date'] =$request->step_8_date;
            }else{
                $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
            }
            
            //$project->update($request->all());

            $project->update($data);

            $project->editorEmail = Auth::user()->email;
    
            Mail::to($allMasters)->send(new SendForApproval($project, null));
    
            return redirect('/projects');

        }elseif( $request->submitButton == 'amend-editor' && Auth::user()->role == 'editor' ){

            //Editor send back to author for editing

            $project->step_0_locked = 0;
    
            $project->step_0_status = 1;
    
            $data = $request->all();
            $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
            $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
            $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
            $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
            $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
            $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');
            if ($request->step_6_date ==''){
                $data['step_6_date'] =$request->step_6_date;
            }else{
                $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
            }
            if ($request->step_7_date ==''){
                $data['step_7_date'] =$request->step_7_date;
            }else{
                $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
            }
            if ($request->step_8_date ==''){
                $data['step_8_date'] =$request->step_8_date;
            }else{
                $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
            }
            
            //$project->update($request->all());

            $project->update($data);

            $project->sendback = true;
    
            Mail::to($project->user->email)->send(new SendForApproval($project, null));
    
            return redirect('/projects');
        }
        elseif( $request->submitButton == 'amend-master' && Auth::user()->role == 'master' ){

            //Master send back to author

            $project->step_0_locked = 0;
    
            $project->step_0_status = 1;
    
            $data = $request->all();
            $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
            $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
            $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
            $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
            $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
            $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');
            if ($request->step_6_date ==''){
                $data['step_6_date'] = $request->step_6_date;
            }else{
                $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
            }
            if ($request->step_7_date ==''){
                $data['step_7_date'] = $request->step_7_date;
            }else{
                $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
            }
            if ($request->step_8_date ==''){
                $data['step_8_date'] = $request->step_8_date;
            }else{
                $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
            }
            
            //$project->update($request->all());

            $project->update($data);

            $project->sendback = true;
    
            Mail::to($project->user->email)->send(new SendForApproval($project, null));
    
            return redirect('/projects');
        }
        elseif( $request->submitButton == 'approve' && Auth::user()->role == 'master' ){

            //Master approve project

            //Second lot of dates have been entered
            if($project->step_5_status == 4){

                $project->step_0_locked = 1;
        
                $project->step_0_status = 4;

                $project->step_6_status = 1;
        
                $data = $request->all();
                $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
                $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
                $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
                $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
                $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
                $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');
                if ($request->step_6_date ==''){
                    $data['step_6_date'] =$request->step_6_date;
                }else{
                    $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
                }
                if ($request->step_7_date ==''){
                    $data['step_7_date'] =$request->step_7_date;
                }else{
                    $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
                }
                if ($request->step_8_date ==''){
                    $data['step_8_date'] =$request->step_8_date;
                }else{
                    $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
                }
                
                //$project->update($request->all());

                $project->update($data);

                $project->move_to_step_6 = true;
        
                Mail::to($project->user->email)->send(new SendForApproval($project, null));
        
                return redirect('/projects');

            }else{

                //First lot of dates have been entered
                $project->step_0_locked = 1;
        
                $project->step_0_status = 4;

                $project->step_1_status = 1;
        
                $data = $request->all();
                $data['approach_date'] = Carbon::createFromFormat('d/m/Y', $request->approach_date)->format('Y-m-d');
                $data['step_1_date'] = Carbon::createFromFormat('d/m/Y', $request->step_1_date)->format('Y-m-d');
                $data['step_2_date'] = Carbon::createFromFormat('d/m/Y', $request->step_2_date)->format('Y-m-d');
                $data['step_3_date'] = Carbon::createFromFormat('d/m/Y', $request->step_3_date)->format('Y-m-d');
                $data['step_4_date'] = Carbon::createFromFormat('d/m/Y', $request->step_4_date)->format('Y-m-d');
                $data['step_5_date'] = Carbon::createFromFormat('d/m/Y', $request->step_5_date)->format('Y-m-d');
                if ($request->step_6_date ==''){
                    $data['step_6_date'] =$request->step_6_date;
                }else{
                    $data['step_6_date'] = Carbon::createFromFormat('d/m/Y', $request->step_6_date)->format('Y-m-d');
                }
                if ($request->step_7_date ==''){
                    $data['step_7_date'] =$request->step_7_date;
                }else{
                    $data['step_7_date'] = Carbon::createFromFormat('d/m/Y', $request->step_7_date)->format('Y-m-d');
                }
                if ($request->step_8_date ==''){
                    $data['step_8_date'] =$request->step_8_date;
                }else{
                    $data['step_8_date'] = Carbon::createFromFormat('d/m/Y', $request->step_8_date)->format('Y-m-d');
                }
                
                //$project->update($request->all());

                $project->update($data);

                $project->isApproved = true;
        
                Mail::to($project->user->email)->send(new SendForApproval($project, null));
        
                return redirect('/projects');

            }


        }


    }

    public function index(){

        $user_id  = Auth::user()->id;

        if(Auth::user()->role == 'master' || Auth::user()->role == 'viewer'){

            $stepStatus = 3;

            $projectsForApproval = Project::where('step_0_status', $stepStatus)
            ->orWhere('step_1_status', $stepStatus)
            ->orWhere('step_2_status', $stepStatus)
            ->orWhere('step_3_status', $stepStatus)
            ->orWhere('step_4_status', $stepStatus)
            ->orWhere('step_5_status', $stepStatus)
            ->orWhere('step_6_status', $stepStatus)
            ->orWhere('step_7_status', $stepStatus)
            ->orWhere('step_8_status', $stepStatus)
            ->get();

            $projects = Project::all();

            return view('steps.index', compact('projects', 'projectsForApproval', 'stepStatus'));

        }elseif(Auth::user()->role == 'editor'){

            $stepStatus = 2;

            $projectsForApproval = Project::where('step_0_status', $stepStatus)
            ->orWhere('step_1_status', $stepStatus)
            ->orWhere('step_2_status', $stepStatus)
            ->orWhere('step_3_status', $stepStatus)
            ->orWhere('step_4_status', $stepStatus)
            ->orWhere('step_5_status', $stepStatus)
            ->orWhere('step_6_status', $stepStatus)
            ->orWhere('step_7_status', $stepStatus)
            ->orWhere('step_8_status', $stepStatus)
            ->get();

            $projects = Project::all();

            return view('steps.index', compact('projects', 'projectsForApproval', 'stepStatus'));

        }elseif(Auth::user()->role == 'author'){

            $projects = Project::where('user_id', $user_id)->get();

            return view('steps.index', compact('projects', 'projectsForApproval'));
        }
    }

    public function detail(Project $project, $step){
        //Switch statement for each step will make it easier to make views that are less complicated due to the possible different question types
        //Each case gets the questions associated to the step and returns them as a variable to the page

        if(Auth::user()->id == $project->user->id || Auth::user()->role != 'author'){ //Author should only be able to view their own projects

            if($project->{'step_'. $step .'_status'} != 0 || Auth::user()->role != 'author'){ //Protect against viewing futher steps when earlier steps havent been completed

                switch ($step) {
                    case 1:

                        $redQuestions = Question::where('step', $step)->where('question_no', '<=', '11')->get();

                        $amberQuestions = Question::where('step', $step)->where('question_no', '>', '11')->get();

                        $answers = Answer::where('project_id', $project)->get();

                        return view('steps.step1', compact('project','redQuestions', 'amberQuestions', 'step', 'answers'));

                    break;

                    case 2 :

                        $questions = Question::where('step', $step)->get();
                        return view('steps.step2', compact('project','questions', 'step'));

                    break;

                    case 3:

                        $questions = Question::where('step', $step)->get();
                        $answer_step2_18 = Answer::where('project_id', $project->id)->where('question_id','18')->pluck('answer_text');
                        $answer_step2_19 = Answer::where('project_id', $project->id)->where('question_id','19')->pluck('answer_text');
                        $answer_step2_20 = Answer::where('project_id', $project->id)->where('question_id','20')->pluck('answer_text');
                        $answer_step2_21 = Answer::where('project_id', $project->id)->where('question_id','21')->pluck('answer_text');
                        return view('steps.step3', compact('project','questions', 'step', 'answer_step2_18', 'answer_step2_19', 'answer_step2_20' ,'answer_step2_21'));
                    break;
                    
                    case 4:
                        $questions = Question::where('step', $step)->get();
                        return view('steps.step4', compact('project','questions', 'step'));
                    break;

                    case 5:

                        $questions = Question::where('step', $step)->get();
                        $diligences = Diligence::where('project_id', $project->id)->get();
                        $compliances = Compliance::where('project_id', $project->id)->get();
                        return view('steps.step5', compact('project','questions', 'step', 'diligences', 'compliances'));

                    break;

                    case 6:

                        $questions = Question::where('step', $step)->get();
                        $pos = Po::where('project_id', $project->id)->get();
                        $zincs = Zinc::where('project_id', $project->id)->get();
                        $diligences = Diligence::where('project_id', $project->id)->get();
                        $compliances = Compliance::where('project_id', $project->id)->get();
                        return view('steps.step6', compact('project','questions', 'step','pos','zincs', 'diligences', 'compliances'));

                    break;

                    case 7:

                        $questions = Question::where('step', $step)->get();
                        $reviews = Review::where('project_id', $project->id)->orderBy('review_date', 'asc')->get();
                        return view('steps.step7', compact('project','questions','reviews','step'));

                    break;

                    case 8:

                        $questions = Question::where('step', $step)->get();

                        return view('steps.step8', compact('project','questions', 'step'));

                    break;
                }
            }else{
                abort(403, 'Permission Denied');
            }
        }else{
            abort(403, 'You do not yet have access to this step, please progress the steps in the correct order.');
        }
    }

    public function submit(Request $request, Project $project, $step){

        $allMasters = User::where('role','master')->pluck('email');
        $allEditors = User::where('role','editor')->pluck('email');

        $project->step = $step;
        //Get all form POST data through the request
        $allData = $request->except('_token');
        if ($step=='1' && $request->submitButton != 'save') {
            $this->validate($request, [
                'Q1' => 'required',
                'Q2' => 'required',
                'Q3' => 'required',
                'Q4' => 'required',
                'Q5' => 'required',
                'Q6' => 'required',
                'Q7' => 'required',
                'Q8' => 'required',
                'Q9' => 'required',
                'Q10' => 'required',
                'Q11' => 'required',
                'Q12' => 'required',
                'Q13' => 'required',
                'Q14' => 'required',
                'Q15' => 'required',
                'Q16' => 'required'
            ]);
        } elseif ($step=='2' && $request->submitButton != 'save') {
            if ($project->step_2_locked == 0) { 
                $this->validate($request, [
                    'Q17' => 'required|max:250',
                    'Q18' => 'required|max:250',
                    'Q19' => 'required|max:250', 
                    'Q20' => 'required|max:250',
                    'Q21' => 'required|max:250'
                ]);
            }
        } elseif ($step=='3' && $request->submitButton != 'save') {
            if ($project->step_3_locked == 0) { 
                $this->validate($request, [
                    'Q22' => 'required',
                    'Q23' => 'required',
                    'Q24' => 'required', 
                    'Q25' => 'required',
                    'Q26' => 'required',
                    'Q27' => 'required',
                    'Q28' => 'required', 
                    'Q29' => 'required'
                ]);
            }
        } elseif ($step=='4' && $request->submitButton != 'save') {           
            if ($project->step_4_locked == 0) {

                $this->validate($request, [
                    'Q30' => 'required|max:1000',
                    'Q31' => 'required|max:1000',
                    'Q32' => 'required|max:1000',
                    'Q33' => 'required|max:1000',
                    'Q34' => 'required|max:1000',
                    'Q35' => 'required|max:1000',
                    'Q36' => 'required|max:1000',
                    'Q37' => 'required|max:1000',
                    'Q38' => 'required|max:1000',
                    'Q39' => 'required|max:1000',
                    'Q40' => 'required|max:2000',
                    'Q41' => 'required|max:500',
                    'Q42' => 'required|max:500',
                    'Q43' => 'required|max:500',
                    'Q44' => 'required|max:500',
                    'Q45' => 'required|max:500',
                    'Q46' => 'required|max:500',
                    'Q47' => 'required|max:500',
                    'Q48' => 'required|max:500',
                    'Q49' => 'required|max:500',
                    'Q50' => 'required|max:500'
                ]);

            }
        } elseif ($step=='5' && $request->submitButton != 'save') {           
                if ($project->step_5_locked == 0) {
                        $this->validate($request, [
                            'Q51' => 'required',
                            'Q52' => 'required',
                            'Q53' => 'required',
                            'Q54' => 'required',
                            'Q55' => 'required|date_format:"d/m/Y"',
                            'Q56' => 'required|date_format:"d/m/Y"',
                            'diligence.*' => 'required|min:1',
                            'compliance.*' => 'required|min:1',
                            'diligenceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',
                            'complianceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',
                            'Q59' => 'required',
                            'Q60' => 'required',
                        ],
                        [
                            'Q55.date_format'=> 'Date must be d/m/Y format',
                            'Q55.date_format'=> 'Date must be d/m/Y format'
                        ]);
                }
            }elseif ($step=='6' && $request->submitButton != 'save') {
                if ($project->step_6_locked == 0) { 
                    $this->validate($request, [
                        "po.*" => 'distinct',
                        "zinc.*" => 'distinct',
                        'diligence.*' => 'required|min:1',
                        'compliance.*' => 'required|min:1',
                        'diligenceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',
                        'complianceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',

                    ],
                    [
                        'po.*.distinct'=> 'No duplicate Purchase Order numbers',
                        'zinc.*.distinct'=> 'No duplicate Zinc numbers'
                    ]);
                }
            }elseif ($step=='7' && $request->submitButton != 'save') {
                if ($project->step_7_locked == 0) { 
                    $this->validate($request, [
                        'review' => 'required|array|min:3',
                        "review.*" => 'required|distinct|date_format:"d/m/Y"'
                    ],
                    [
                        'review.min'=> 'A minimum of 3 meetings must be attended',
                        'review.*.distinct'=> 'Meetings cannot be on the same day',
                        'review.*.date_format'=> 'Date must be d/m/Y format'
                    ]);
                    //send calendar invite to editors and master

                }
            } elseif ($step=='8' && $request->submitButton != 'save') {
                if ($project->step_8_locked == 0) { 
                    $this->validate($request, [
                        'Q70' => 'required',
                        'Q71' => 'required',
                        'Q72' => 'required', 
                        'Q73' => 'required',
                        'Q74' => 'required',
                    ],
                    [
                        'Q70.required' => 'You must explain the data to be included in the report',
                        'Q71.required' => 'You must detail the person who collected the data',
                        'Q72.required' => 'You must explain where the data will be published',
                        'Q73.required' => 'You must have stakeholders permission',
                        'Q74.required' => 'You must explain when and where',
                    ]);
                }
            }
           
        $x = 0;
        //For Each request make a new record in the db
        foreach($allData as $questionId => $value) {
            //echo $questionId .  $value .'<br>';
            $questionId = ltrim($questionId, 'Q');
            $typeCheckQuery = Question::where('id', (int)$questionId)->value('answer_type');
            $ifExists = Answer::where('project_id', $project->id)->where('question_id', (int)$questionId)->first();
            if (!is_null($ifExists)) {        
                if($typeCheckQuery == 'radio' || $typeCheckQuery == 'checkbox'){
                    if($value == 'yes'){
                        Answer::where('project_id', $project->id)->where('question_id', (int)$questionId)->update(['answer_boolean' => 1]);  
                    }else{
                        Answer::where('project_id', $project->id)->where('question_id', (int)$questionId)->update(['answer_boolean' => 0]);
                        if ((int)$questionId >= 12) {$x=1;}
                    }
                }elseif($typeCheckQuery == 'text'){    
                    Answer::where('project_id', $project->id)->where('question_id', (int)$questionId)->update(['answer_text' => $value]);
                }elseif($typeCheckQuery == 'date'){  
                    if ($value != '') {
                        $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                    }
                    Answer::where('project_id', $project->id)->where('question_id', (int)$questionId)->update(['answer_date' => $value]);
                }
            }else{
                if($typeCheckQuery == 'radio' || $typeCheckQuery == 'checkbox'){
                    $answer = new Answer;
                    $answer->question_id = $questionId;
                    $answer->project_id = $project->id;
                    if($value == 'yes'){
                        $answer->answer_boolean = 1;
                    }else{
                        $answer->answer_boolean = 0;
                        if ((int)$questionId >= 12) {$x=1;}
                    }
                    $answer->save();
                }elseif($typeCheckQuery == 'text'){                    
                    $answer = new Answer;
                    $answer->question_id = $questionId;
                    $answer->project_id = $project->id;                    
                    $answer->answer_text = $value;
                    $answer->save();
                }elseif($typeCheckQuery == 'date'){      
                    if ($value<>'') {
                        $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                    }
                    $answer = new Answer;
                    $answer->question_id = $questionId;
                    $answer->project_id = $project->id;                    
                    $answer->answer_date = $value;
                    $answer->save();
                }
            }
        }
        if($request->submitButton == 'save'){
            if($step == 1){
                // alert();
            }
            if($step == 4){
                $this->validate($request, [
                    'Q30' => 'max:1000',
                    'Q31' => 'max:1000',
                    'Q32' => 'max:1000',
                    'Q33' => 'max:1000',
                    'Q34' => 'max:1000',
                    'Q35' => 'max:1000',
                    'Q36' => 'max:1000',
                    'Q37' => 'max:1000',
                    'Q38' => 'max:1000',
                    'Q39' => 'max:1000',
                    'Q40' => 'max:2000',
                    'Q41' => 'max:500',
                    'Q42' => 'max:500',
                    'Q43' => 'max:500',
                    'Q44' => 'max:500',
                    'Q45' => 'max:500',
                    'Q46' => 'max:500',
                    'Q47' => 'max:500',
                    'Q48' => 'max:500',
                    'Q49' => 'max:500',
                    'Q50' => 'max:500'
                ]);
            }   
            if($step == 5){

                $this->validate($request, [
                    'diligenceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',
                    'complianceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',
                ]);

                $diligenceFile = $request->diligenceFile;
                //dd($request);
                if ($request->diligence !='') {
                    foreach($request->diligence as $key => $diligence){
                        if($diligence != NULL || $diligence != ''){
                            //No file
                            if(!isset($diligenceFile[$key])){
                                $newDiligence = Diligence::updateOrCreate(
                                    [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                    [ 'description' => $diligence]
                                );
                                //file
                            }else{
                                $newDiligence = Diligence::updateOrCreate(
                                    [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                    [ 'file_name' => $diligenceFile[$key]->hashName(), 'file_name_unhashed' => $diligenceFile[$key]->getClientOriginalName(), 'description' => $diligence]
                                );
                                //Store file in public folder, its automatically hashed by laravel
                                $diligenceFile[$key]->store('public');
                            }
                        }
                    }
                }
                $complianceFile = $request->complianceFile;
                if ($request->compliance !='') {
                    foreach($request->compliance as $key => $compliance){
                        if($compliance != NULL || $compliance != ''){
                            if(!isset($complianceFile[$key])){
                                $newCompliance = Compliance::updateOrCreate(
                                    [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                    [ 'description' => $compliance]
                                );
                            }else{

                                $newCompliance = Compliance::updateOrCreate(
                                    [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                    [ 'file_name' => $complianceFile[$key]->hashName(),'file_name_unhashed' => $complianceFile[$key]->getClientOriginalName(), 'description' => $compliance]
                                );
                                //Store file in public folder, its automatically hashed by laravel
                                $complianceFile[$key]->store('public');
                            }
                        }
                    }
                }
            }elseif($step == 6) {

                $this->validate($request, [
                    'diligenceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',
                    'complianceFile.*' => 'required|mimes:doc,pdf,docx,zip,xla,xls,xlsx,ppt,pptx',
                ]);
                
                $po_id = $request->po_id;
                $po_who = $request->po_who;
                $po_purpose = $request->po_purpose;
                if ($request->po !='') {
                    foreach($request->po as $key => $po){
                        if ($po != null) {                     
                            //echo $project->id." ".$po." ".$po_who[$key]." ".$po_purpose[$key]."<br>";
                            $newPo = Po::updateOrCreate(
                                ['id' => $po_id[$key],'project_id' => $project->id ],
                                ['description' => $po, 'who' => $po_who[$key], 'purpose' => $po_purpose[$key]]
                            );
                        }
                    }
                }
                $zinc_id = $request->zinc_id;
                if ($request->zinc !='') {
                    foreach($request->zinc as $key => $zinc){
                        if ($zinc != null) {                     
                            //echo $project->id." ".$zinc." ".$zinc_id[$key]."<br>";
                            $newZinc = Zinc::updateOrCreate(
                                ['id' => $zinc_id[$key], 'project_id' => $project->id],
                                ['description' => $zinc]
                            );
                        }
                    }
                }

                $diligenceFile = $request->diligenceFile;
                if ($request->diligence !='') {
                    foreach($request->diligence as $key => $diligence){
                        if($diligence != NULL || $diligence != ''){
                            if(!isset($diligenceFile[$key])){
                                $newDiligence = Diligence::updateOrCreate(
                                    [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                    [ 'description' => $diligence]
                                );
                            }else{
                                $newDiligence = Diligence::updateOrCreate(
                                    [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                    [ 'file_name' => $diligenceFile[$key]->hashName(), 'file_name_unhashed' => $diligenceFile[$key]->getClientOriginalName(),'description' => $diligence]
                                );                         
                                //Store file in public folder, its automatically hashed by laravel
                                $diligenceFile[$key]->store('public');
                            }
                        }
                    }
                }

                $complianceFile = $request->complianceFile;
                if ($request->compliance !='') {
                    foreach($request->compliance as $key => $compliance){
                        if($compliance != NULL || $compliance != ''){
                            if(!isset($complianceFile[$key])){
                                $newCompliance = Compliance::updateOrCreate(
                                    [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                    [ 'description' => $compliance]
                                );
                            }else{
                                $newCompliance = Compliance::updateOrCreate(
                                    [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                    [ 'file_name' => $complianceFile[$key]->hashName(),'file_name_unhashed' => $complianceFile[$key]->getClientOriginalName(), 'description' => $compliance]
                                );
                                //Store file in public folder, its automatically hashed by laravel           
                                $complianceFile[$key]->store('public');
                            }
                        }
                    }
                }
            }elseif($step == 7) {
                $this->validate($request, [
                    'review' => 'required',
                    "review.*" => 'required'
                ]);

                $review_attended = $request->review_attended;

                if ($request->review !='') {
                    foreach($request->review as $key => $review){
                        // if selected is yes
                        if ($review_attended[$key]=='yes'){
                            $attended = 1;
                        }elseif ($review_attended[$key]=='no'){
                            $attended = 2;
                        }else{
                            $attended = 0;
                        }
                        if ($review != null) { 
                            $date = $review; 
                            $date = str_replace('/','-',$date);
                            $date = date('Y-m-d' , strtotime($date));
                           
                            $newReview = Review::updateOrCreate(
                                ['project_id' => $project->id, 'review_date' => $date],
                                ['review_attended' => $attended]
                            );
                        }
                    }
                }
            }

           Session::flash('save-success', 'Step saved.');

           return redirect()->route('step detail',['project' => $project,'step'=>$step]);

        }elseif( $request->submitButton == 'submit'){
            if ($step==1) {
                if ($x == 1) {
                    Project::where('id', $project->id)->update(['step_1_locked' => 1, 'step_1_status' => 4, 'step_2_status' => 1]);
                    Mail::to($project->user->email)->send(new SubmissionSuccess($step, $project));
                    Session::flash('message-step1', 'A No answer in the Amber Questions may predict a low success rate. You may proceed, but you should consult your line manager for advice first.');
                    return redirect()->route('step detail',['project' => $project,'step'=>$step]);
                } else {  
                    Project::where('id', $project->id)->update(['step_1_locked' => 1, 'step_1_status' => 4, 'step_2_status' => 1]);
                    Mail::to($project->user->email)->send(new SubmissionSuccess($step, $project));
                    return redirect()->route('step detail',['project' => $project,'step'=> 2]);
                }  
            }elseif($step == 5){

                $diligenceFile = $request->diligenceFile;

                foreach($request->diligence as $key => $diligence){

                    if($diligence != NULL || $diligence != ''){
                        if(!isset($diligenceFile[$key])){

                            $newDiligence = Diligence::updateOrCreate(
                                [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                [ 'description' => $diligence]
                            );

                        }else{

                            $newDiligence = Diligence::updateOrCreate(
                                [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                [ 'file_name' => $diligenceFile[$key]->hashName(), 'file_name_unhashed' => $diligenceFile[$key]->getClientOriginalName(), 'description' => $diligence]
                            );
                            
                            //Store file in public folder, its automatically hashed by laravel
                            
                            $diligenceFile[$key]->store('public');
                        }
                    }
                }

                $complianceFile = $request->complianceFile;

                foreach($request->compliance as $key => $compliance){

                    if($compliance != NULL || $compliance != ''){
                        if(!isset($complianceFile[$key])){

                            $newCompliance = Compliance::updateOrCreate(
                                [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                [ 'description' => $compliance]
                            );

                        }else{

                            $newCompliance = Compliance::updateOrCreate(
                                [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                [ 'file_name' => $complianceFile[$key]->hashName(), 'file_name_unhashed' => $complianceFile[$key]->getClientOriginalName(), 'description' => $compliance]
                            );
                            
                            //Store file in public folder, its automatically hashed by laravel
                            
                            $complianceFile[$key]->store('public');
                        }
                    }
                }

               Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 2]);

               Mail::to($project->user->email)->send(new SubmissionSuccess($step, $project));

               Mail::to($allEditors)->send(new SendForApproval($project, $step));

               return redirect()->route('projects');

            } elseif($step == 6) {
                $po_id = $request->po_id;
                $po_who = $request->po_who;
                $po_purpose = $request->po_purpose;
                foreach($request->po as $key => $po){
                    if ($po != null) {                     
                        //echo $project->id." ".$po." ".$po_who[$key]." ".$po_purpose[$key]."<br>";
                        $newPo = Po::updateOrCreate(
                            ['id' => $po_id[$key],'project_id' => $project->id ],
                            ['description' => $po, 'who' => $po_who[$key], 'purpose' => $po_purpose[$key]]
                        );
                    }
                }
                $zinc_id = $request->zinc_id;
                foreach($request->zinc as $key => $zinc){
                    if ($zinc != null) {                     
                        // echo $project->id." ".$zinc." ".$zinc_id[$key]."<br>";
                        $newZinc = Zinc::updateOrCreate(
                            ['id' => $zinc_id[$key], 'project_id' => $project->id],
                            ['description' => $zinc]
                        );
                    }
                }

                $diligenceFile = $request->diligenceFile;
                foreach($request->diligence as $key => $diligence){
                    if($diligence != NULL || $diligence != ''){
                        if(!isset($diligenceFile[$key])){
                            $newDiligence = Diligence::updateOrCreate(
                                [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                [ 'description' => $diligence]
                            );
                        }else{
                            $newDiligence = Diligence::updateOrCreate(
                                [ 'id' => $request->hiddenDiligence[$key],'project_id' => $project->id  ],
                                [ 'file_name' => $diligenceFile[$key]->hashName(), 'file_name_unhashed' => $diligenceFile[$key]->getClientOriginalName(), 'description' => $diligence]
                            );                         
                            //Store file in public folder, its automatically hashed by laravel
                            $diligenceFile[$key]->store('public');
                        }
                    }
                }

                $complianceFile = $request->complianceFile;
                foreach($request->compliance as $key => $compliance){
                    if($compliance != NULL || $compliance != ''){
                        if(!isset($complianceFile[$key])){
                            $newCompliance = Compliance::updateOrCreate(
                                [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                [ 'description' => $compliance]
                            );
                        }else{
                            $newCompliance = Compliance::updateOrCreate(
                                [ 'id' => $request->hiddenCompliance[$key],'project_id' => $project->id  ],
                                [ 'file_name' => $complianceFile[$key]->hashName(), 'file_name_unhashed' => $complianceFile[$key]->getClientOriginalName(), 'description' => $compliance]
                            );
                            //Store file in public folder, its automatically hashed by laravel           
                            $complianceFile[$key]->store('public');
                        }
                    }
                }
                Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 2]);
                Mail::to($project->user->email)->send(new SubmissionSuccess($step, $project));
                Mail::to($allEditors)->send(new SendForApproval($project, $step));
                return redirect()->route('projects');
            } elseif($step == 7) {
                $x=0;
                $review_id = $request->review_id;
                $review_attended = $request->review_attended;
                foreach($request->review as $key => $review){
                    if ($review_attended[$key]!=='no' ){
                        $x = $x + 1;
                    }
                }
                if ($x <= 2) {
                    Session::flash('message-step7', 'At least 3 meetings must be attendedable');
                    return redirect()->route('step detail',['project' => $project,'step'=>$step]);
                } else {
                    foreach($request->review as $key => $review){
                        // if selected is yes
                        if ($review_attended[$key]=='yes'){
                            $attended = 1;
                        }elseif ($review_attended[$key]=='no'){
                            $attended = 2;
                        }else{
                            $attended = 0;
                        }
                        if ($review != null) {
                            $date = $review; 
                            $date = str_replace('/','-',$date);
                            $date = date('Y-m-d' , strtotime($date));
                            // echo $project->id." ".$review." ".$attended."<br>";
                            $newReview = Review::updateOrCreate(
                                ['id' => $review_id[$key],'project_id' => $project->id ],
                                ['review_date' => $date, 'review_attended' => $attended]
                            );
                        }
                    }

                   Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 2]);

                   Mail::to($project->user->email)->send(new SubmissionSuccess($step, $project));

                   //Not necessary if calendar notifications sent - see below
                   // Mail::to($allEditors)->send(new SendForApproval($project, $step));

                   Mail::to($allEditors)->send(new CalendarInvite($project, $step, $request));

                   Mail::to($project->user->email)->send(new CalendarInvite($project, $step, $request));

                   return redirect()->route('projects');
                }

            } else {
                Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 2]);

                Mail::to($project->user->email)->send(new SubmissionSuccess($step, $project));

                Mail::to($allEditors)->send(new SendForApproval($project, $step));

                return redirect()->route('projects'); 
                        
            }
        }elseif( $request->submitButton == 'signatory' && Auth::user()->role == 'editor' ){
            if($step == 7) {
                $x=0;
                $review_id = $request->review_id;
                $review_attended = $request->review_attended;
                foreach($request->review as $key => $review){
                    // if selected is yes
                    if ($review_attended[$key]=='yes'){
                        $x = $x + 1;
                    }
                }
                if ($x <= 2) {
                    Session::flash('message-step7', 'At least 3 meetings must be attended before submission');
                    return redirect()->route('step detail',['project' => $project,'step'=>$step]);
                } else {
                    foreach($request->review as $key => $review){
                      // if selected is yes
                        if ($review_attended[$key]=='yes'){
                            $attended = 1;
                        }elseif ($review_attended[$key]=='no'){
                            $attended = 2;
                        }else{
                            $attended = 0;
                        }
                        if ($review != null) {
                            $date = $review; 
                            $date = str_replace('/','-',$date);
                            $date = date('Y-m-d' , strtotime($date));
                            // echo $project->id." ".$review." ".$attended."<br>";
                            $newReview = Review::updateOrCreate(
                                ['id' => $review_id[$key],'project_id' => $project->id ],
                                ['review_date' => $date, 'review_attended' => $attended]
                            );
                        }
                    }
                }
                //Editor send to master
                
                Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 3]);

                $project->editorEmail = Auth::user()->email;

                Mail::to($allMasters)->send(new SendForApproval($project, $step));
    
                return redirect()->route('projects');
            }else{
            //Editor send to master
                
                Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 3]);

                $project->editorEmail = Auth::user()->email;

                Mail::to($allMasters)->send(new SendForApproval($project, $step));
    
                return redirect()->route('projects');
            }

        }elseif( $request->submitButton == 'amend-editor' && Auth::user()->role == 'editor' ){

            if($step == 7) {
                //Editor send back to author for editing
                $review_id = $request->review_id;
                $review_attended = $request->review_attended;
                foreach($request->review as $key => $review){
                  // if selected is yes
                    if ($review_attended[$key]=='yes'){
                        $attended = 1;
                    }elseif ($review_attended[$key]=='no'){
                        $attended = 2;
                    }else{
                        $attended = 0;
                    }
                    if ($review != null) {
                        $date = $review; 
                        $date = str_replace('/','-',$date);
                        $date = date('Y-m-d' , strtotime($date));
                        // echo $project->id." ".$review." ".$attended."<br>";
                        $newReview = Review::updateOrCreate(
                            ['id' => $review_id[$key],'project_id' => $project->id ],
                            ['review_date' => $date, 'review_attended' => $attended]
                        );
                    }
                }
            }
            Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 0, 'step_'.$step.'_status' => 1]);

            $project->sendback = true;

            Mail::to($project->user->email)->send(new SendForApproval($project, $step));

            return redirect()->route('projects'); 
        }
        elseif( $request->submitButton == 'amend-master' && Auth::user()->role == 'master' ){

            //Master send back to author for editing

            Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 0, 'step_'.$step.'_status' => 1]);

            $project->sendback = true;

            Mail::to($project->user->email)->send(new SendForApproval($project, $step));

            return redirect()->route('projects'); 
        }
        elseif( $request->submitButton == 'approve' && Auth::user()->role == 'master' ){

            //Master approve project
            if($step == 8){

                //Step 8 is that last step, so we dont increment

                Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 4, 'end_date' => date("Y-m-d")]);

                $project->isFinished = true;

                Mail::to($project->user->email)->send(new SendForApproval($project, $step));
    
                return redirect()->route('projects');

            }elseif($step == 5){

                Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 4, 'step_0_status' => 1, 'step_0_locked' => 0]);

                $project->back_to_dates = true;

                Mail::to($project->user->email)->send(new SendForApproval($project, $step));
    
                return redirect()->route('projects');
                
            }else{

                Project::where('id', $project->id)->update(['step_'.$step.'_locked' => 1, 'step_'.$step.'_status' => 4, 'step_'.($step+1).'_status' => 1]);

                $project->isApproved = true;

                Mail::to($project->user->email)->send(new SendForApproval($project, $step));
    
                return redirect()->route('projects');

            }


        }
    }
}
