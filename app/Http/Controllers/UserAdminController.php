<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;

use App\User;

use App\Mail\ConfirmApproval;

use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;

class UserAdminController extends Controller
{
    public function userList() {

        $users = User::all();

        return view('userManagement.userList', compact('users'));
    }

    public function userDetails(User $user){

        $roles = array("author", "viewer", "editor", "master");

        return view('userManagement.userDetails', compact('user', 'roles'));
    }

    public function editUser(Request $request, User $user){

        $this->validate($request, [
            'firstname' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'job_title' => 'required',
            'phone' => 'required',
        ]);

        $currentlyApproved = User::where('id', '=', $user->id)->first();

        //Match the request with the stored record to check if the approved status has changed 

        if( $request->input('approved') == 1 && $currentlyApproved->approved == 0){

            $user->update($request->all());

            Session::flash('message', 'User Updated and approval confirmation sent'); 

            //Send confirmation email

            Mail::to($user->email)->send(new ConfirmApproval($user));

            return redirect()->action(
                'UserAdminController@userDetails', ['id' => $user->id]
            );

        }else{
            $user->update($request->all());

            Session::flash('message', 'User Updated'); 

            return redirect()->action(
                'UserAdminController@userDetails', ['id' => $user->id]
            );
        }

    }

    public function newUserDetails(){

        return view('userManagement.addUser');
    }

    public function addUser(Request $request){

        $this->validate($request, [
            'role' => 'required',
            'firstname' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'job_title' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = new User($request->all());

        //Encrypt password

        $user->password = bcrypt($request->input('password'));

        $user->save();

        Session::flash('message', 'New User Added'); 

        return redirect()->action(
            'UserAdminController@newUserDetails'
        );

    }
}
