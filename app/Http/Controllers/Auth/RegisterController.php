<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;



use App\Mail\ConfirmUser;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Traits\reCAPATCHATrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use reCAPATCHATrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/projects';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // call the verifyCaptcha method to see if Google approves
        $data['captcha-verified'] = $this->verifyCaptcha($data['g-recaptcha-response']);

        return Validator::make($data, [
            //'role' => 'required|string|max:255',
            'firstname' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users|confirmed',
            'job_title' => 'required|string|min:6',
            'role' => 'required',
            'phone' => 'required|string|min:8',
            'password' => 'required|string|min:6|confirmed',            
            'captcha-verified'  => 'required|min:1',
            'g-recaptcha-response' => 'required'
        ],
        [
            'g-recaptcha-response.required' => 'Please confirm that you are not a robot',
            'captcha-verified.min'           => 'reCaptcha verification failed'
        ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create( array $data)
    {
        $user = User::create([
            'firstname' => $data['firstname'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'job_title' => $data['job_title'],
            'role' => $data['role'],
            'password' => bcrypt($data['password']),
        ]);
        $allMasters = User::where('role','master')->pluck('email');

        Mail::to($allMasters)->send(new ConfirmUser($user));
        
        return $user;
    }

}
