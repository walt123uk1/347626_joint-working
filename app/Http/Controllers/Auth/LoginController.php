<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\reCAPATCHATrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use reCAPATCHATrait;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/projects';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect('/login');
    }

    public function redirect(){
        return redirect('/login');
    }
    /**
 * Handle a login request to the application.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
    protected function validateLogin(\Illuminate\Http\Request $request)
    {

        // call the verifyCaptcha method to see if Google approves
        $request['captcha-verified'] = $this->verifyCaptcha($request['g-recaptcha-response']);

            $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',          
            'captcha-verified'  => 'required|min:1',
            'g-recaptcha-response' => 'required'
        ],
        [
            'g-recaptcha-response.required' => 'Please confirm that you are not a robot',
            'captcha-verified.min'           => 'reCaptcha verification failed'
        ]
        );
    }
}
