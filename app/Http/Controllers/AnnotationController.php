<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;

use App\Annotation;
use App\Project;

use Auth;

use Illuminate\Http\Request;

class AnnotationController extends Controller
{
    public function noteset(Request $request) {
        $annotation = new Annotation();
        $annotation->note = $request->note;
        $annotation->posn_x = $request->posn_x;
        $annotation->posn_y = $request->posn_y;
        $annotation->answer_id = $request->answer_id;
        $annotation->project_id = $request->project;
        $annotation->step_id = $request->step;
        $annotation->user_id = Auth::user()->id;
        $annotation->save();
        return Response($annotation);
    }

    public function noteget(Project $project, $step) { 
        $annotation = Annotation::where('project_id','=',$project->id)->where('step_id','=',$step)
         ->join('users', 'annotations.user_id', '=', 'users.id')
         ->select('annotations.*','users.firstname','users.surname')
        ->get();
        return Response($annotation);
    }

    public function update($id, Request $request)
    {
        // dd($request);
        $annotation = Annotation::find($id);
        if($annotation) {
            $annotation->note = $request->note;
            if($annotation->save()) {
                return response()->json(['status' => 'success']);
            }
        }

        return response()->json(['status' => 'error']);
    }    

    public function archive($id, Request $request)
    {
        //dd($request);
        $annotation = Annotation::find($id);
        if($annotation) {
            $annotation->archive = 1;
            if($annotation->save()) {
                return response()->json(['status' => 'success']);
            }
        }

        return response()->json(['status' => 'error']);
    }

    public function delete($id)
    {
        if(Annotation::destroy($id)) {
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);
    }

}
