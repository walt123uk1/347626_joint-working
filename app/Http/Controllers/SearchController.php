<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use App\User;
use Auth;

class SearchController extends Controller
{
    public function test(Request $request){

        if(Auth::user()->role == 'author'){
            
            $projects = Project::where('user_id',Auth::user()->id)->where('project_name','LIKE','%'.$request->search."%")
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->select('projects.*','users.firstname','users.surname')
            ->get();

            return Response($projects);

        }else{
            $projects = Project::where('project_name','LIKE','%'.$request->search."%")
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->select('projects.*','users.firstname','users.surname')
            ->get();

            return Response($projects);
        }

    }

    public function usersSearch(Request $request){

            $users = User::where('surname','LIKE','%'.$request->search."%")->orWhere('firstname','LIKE','%'.$request->search."%")->get();

            return Response($users);

    }

    public function financeSearch(Request $request){

        $users = User::where('surname','LIKE','%'.$request->search."%")->orWhere('firstname','LIKE','%'.$request->search."%")->get();

        return Response($users);

    }

    
}
