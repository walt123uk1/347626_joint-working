<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

use Illuminate\Support\Facades\Session;

class CheckApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user();

        //If logged in user is approved, allow access

        if(Auth::check()){

            if ($user->approved == 1){

                return $next($request);

            }elseif($user->approved == 0 && $user->beenToApproval == 1){

                Auth::logout();
                Session::flash('notAuth', 'Please wait patiently until somebody can approve your request to joint the Joint Working Portal.');
                return redirect('/login');

            }elseif($user->approved == 0){

                $user->beenToApproval = 1;

                $user->save();

                return redirect('/approval');

            }
        }   
        else{

            return redirect()->guest('login');
            
        }
    }
}
