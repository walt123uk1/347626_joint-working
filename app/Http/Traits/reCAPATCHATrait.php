<?php
 
namespace App\Http\Traits;
 
use ReCaptcha\ReCaptcha;
 
trait reCAPATCHATrait {
 
    public function verifyCaptcha($response)
    {
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $secret   = env('GOOGLE_RECAPTCHA_SECRET');
        $recaptcha = new ReCaptcha($secret);
        $status = $recaptcha->verify($response, $remoteip);
        if ($status->isSuccess()) {
            return true;
        } else {
            return false;
        }
 
    }
 
}