<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SubmissionSuccess extends Mailable
{
    use Queueable, SerializesModels;

    public $myStep;
    public $myProject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($step, $project)
    {
        $this->myStep = $step;
        $this->myProject = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Step submitted for approval')->view('mail.stepFlow.SubmissionSuccess');
    }
}
