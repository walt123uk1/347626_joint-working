<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CalendarInvite extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $step;
    public $request;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $step, $request)
    {
        $this->project = $project;
        $this->step = $step;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
        return $this->subject($this->project->project_name. ' - Step '.$this->step . ' Calendar Invitation')->view('mail.calendarInvite');
        
    }
}
