<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPastMeetings extends Mailable
{
    use Queueable, SerializesModels;
    public $totalPastReviews;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($totalPastReviews)
    {
        $this->totalPastReviews = $totalPastReviews;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.yesterdaysmeetings');
    }
}
