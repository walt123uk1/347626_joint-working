<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendForApproval extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $step;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $step)
    {
        $this->project = $project;
        $this->step = $step;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if( $this->project->isApproved == true ){
            return $this->subject($this->project->project_name. ' - Step '.$this->step . ' Approved')->view('mail.stepFlow.toEditor');
        }elseif($this->project->move_to_step_6 == true){
            //Project re-approved, move to step 6
            return $this->subject($this->project->project_name. ' - Step '.$this->step . ' Approved')->view('mail.stepFlow.toEditor');
        }elseif($this->project->back_to_dates == true){
            return $this->subject($this->project->project_name. ' - Step '.$this->step . ' Approved')->view('mail.stepFlow.toEditor');
        }elseif( isset($this->project->isFinished) ){
            return $this->subject($this->project->project_name. ' Completed')->view('mail.stepFlow.toEditor');
        }elseif( isset($this->project->step) && !isset($this->project->sendback) ){
            //Submitted to editor or master for approval
            return $this->subject($this->project->project_name. ' - Step '.$this->step . ' Submitted for approval')->view('mail.stepFlow.toEditor');
        }elseif( isset($this->project->step) && isset($this->project->sendback) && $this->project->step == 7 ){
            return $this->subject($this->project->project_name. ' - Step '.$this->step . ' Date Review Required')->view('mail.stepFlow.toEditor');
        }elseif( isset($this->project->step) && isset($this->project->sendback) ){
            return $this->subject($this->project->project_name. ' - Step '.$this->step . ' Amendments Required')->view('mail.stepFlow.toEditor');
        }elseif( !isset($this->project->step) && !isset($this->project->sendback) ){
            return $this->subject($this->project->project_name. ' submitted for approval')->view('mail.stepFlow.toEditor');
        }elseif( !isset($this->project->step) && isset($this->project->sendback) ){
            return $this->subject($this->project->project_name. ' Amendments Required')->view('mail.stepFlow.toEditor');
        }
    }
}