<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMeetings extends Mailable
{
    use Queueable, SerializesModels;
    public $totalReviews;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($totalReviews)
    {
        $this->totalReviews = $totalReviews;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.todaysmeetings');
    }
}
