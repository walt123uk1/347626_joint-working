<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Po extends Model
{
    protected $guarded = ['id'];
    
    public function project(){
        return $this->belongsTo('App\Project');
    }
    protected $fillable = [
            'project_id',
            'description',
            'who',
            'purpose'
    ];
}
