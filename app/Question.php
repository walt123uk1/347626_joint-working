<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function answer()
    {
        return $this->hasMany('App\Answer');
    }

    public function question()
    {
        return $this->belongsTo('App\Project');
    }
    protected $fillable = [
        'step','question_no','question','answer_type','answer_length'
    ];
}
