<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{    
	public function question()
    {
        return $this->belongsTo('App\Question');
    }
	public function annotation()
    {
        return $this->hasMany('App\Annotation');
    }
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    protected $fillable = [
        'answer_boolean','answer_text','answer_date'
    ];

    protected $dates = ['answer_date'];
}
