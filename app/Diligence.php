<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diligence extends Model
{
    protected $guarded = ['id'];
    
    public function project(){
        return $this->belongsTo('App\Project');
    }
}
