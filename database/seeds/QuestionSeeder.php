<?php

use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    	
    	DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 1,
            'question' => 'The main benefit of the project is focused on the patient',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);    	
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 2,
            'question' => 'All parties acknowledge the arrangements may also benefit the NHS and pharmaceutical stakeholders involved',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 3,
            'question' => 'Any subsequent benefits are at an organisational level and not specific to any individual',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 4,
            'question' => 'There is a significant contribution of pooled resources (taking into account people, finance, equipment and time) from each of the parties involved',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 5,
            'question' => 'There is a shared commitment to joint development, implementation and delivery of a patient-centred project by all involved',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 6,
            'question' => 'Patient outcomes of the project will be measured and documented',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 7,
            'question' => 'All partners are committed to publishing an executive summary of the Joint Working Agreement',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 8,
            'question' => 'All proposed treatments involved are in line with national guidance where such exists',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 9,
            'question' => 'All activities are to be conducted in an open and transparent manner',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 10,
            'question' => 'Exit strategy and any contingency arrangements have been agreed',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 11,
            'question' => "Confirm you have informed the NHS partner that Teva is required to publish an executive summary and case study on the company's Joint Working website, in accordance with the ABPI's Code of Practice. Teva's spend and resource on the project will be disclosed in line with ABPI disclosure reporting requirements at a named NHS organisational level.",
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 12,
            'question' => 'Will the project be managed by a joint project team with pharmaceutical industry, NHS and any appropriate third party representation?',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 13,
            'question' => 'Do all parties and their respective organisations have appropriate skills and capabilities in place to manage the project thus enabling delivery of patient outcomes?',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 14,
            'question' => 'Have all stakeholder organisations got clear procedures in place for reviewing and approving Joint Working projects?',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 15,
            'question' => 'Are all stakeholders aware of and committed to using the Joint Working Agreement Template (or equivalent) developed by the DH and ABPI?',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 1,
            'question_no' => 16,
            'question' => 'Are all partners clear on who within their organisations is the signatory to ensure Joint Working Agreements can be certified?',
            'answer_type' => 'radio',
            'answer_length' => '1'
        ]);

        DB::table('questions')->insert([
            'step' => 2,
            'question_no' => 1,
            'question' => 'What is the proposed name of your project? (250 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '250'
        ]);
        DB::table('questions')->insert([
            'step' => 2,
            'question_no' => 2,
            'question' => 'What NHS challenge does your project aim to address? (250 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '250'
        ]);
        DB::table('questions')->insert([
            'step' => 2,
            'question_no' => 3,
            'question' => 'Describe how your project contributes to the solution of this NHS challenge? (250 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '250'
        ]);
        DB::table('questions')->insert([
            'step' => 2,
            'question_no' => 4,
            'question' => 'Where in the UK is the project based? (250 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '250'
        ]);
        DB::table('questions')->insert([
            'step' => 2,
            'question_no' => 5,
            'question' => 'What NHS organisation are you working with? (250 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '250'
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 1,
            'question' => 'How will the project benefit patients?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 2,
            'question' => 'How will the project benefit the NHS?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 3,
            'question' => 'How will the project benefit Teva?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 4,
            'question' => 'What is the estimated duration of the project?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 5,
            'question' => 'What is the estimated cost to Teva?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 6,
            'question' => 'What are the resources required from Teva?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 7,
            'question' => 'What is the estimated cost and/or resource required from the NHS stakeholder?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 3,
            'question_no' => 8,
            'question' => 'When and by whom will the write up be completed?',
            'answer_type' => 'text',
            'answer_length' => ''
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 1,
            'question' => 'Project Title:<br>Please enter the full title of your Joint Working project: (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 2,
            'question' => 'Background:<br>Write a brief rationale explaining the context of the project and why it was thought to be necessary. (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 3,
            'question' => 'Overarching Aim:<br>Explain what the project intends to achieve. (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 4,
            'question' => 'Objectives:<br>What actions will be taken to deliver on the project rationale? (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 5,
            'question' => 'Project Deliverables:<br>Describe the deliverables and/or desired outcomes of the project. (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 6,
            'question' => 'Project Scope:<br>Describe what is included in the project (e.g. inclusion criteria: adults; exclusion criteria: children). Also include how outcomes will be measured. (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);
        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 7,
            'question' => 'Explain the benefit to the Patient:<br>E.g. demonstrate a patient centred approach, integrating care, improving outcomes, addressing inequalities, providing a more positive experience of care. (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);
        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 8,
            'question' => 'Explain the benefit to the NHS:<br>E.g. address needs of local population, develop integrated care, apply innovative approach to address local healthcare challenges.
(1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 9,
            'question' => "Explain the benefit to Teva:<br>E.g. raise Teva's profile within a locality, deliver solutions to address local challenges, apply evidence based approach to healthcare. (1000 characters maximum)",
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 10,
            'question' => 'Constraints:<br>Describe any constraints within which the project will have to operate. (1000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 11,
            'question' => 'Project Approach:<br>Define the type of solution to be developed or procured by the project. It should identify the environment into which the project must be delivered and define the financial and/or resource investment. Please indicate resources required (e.g. people, equipment, expertise, finance, communication channels, IT) and where they will come from. (2000 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '2000'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 12,
            'question' => 'Interfaces:<br>Describe the networks between the project and other service areas (e.g. primary and secondary care, community services). (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 13,
            'question' => 'Assumptions:<br>Describe the assumptions which are being made within the project. (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);
        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 14,
            'question' => 'Acceptance Criteria:<br>Describe any acceptance criteria agreed with the stakeholder which will determine the success of the project (e.g. number of participating practices). (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);
        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 15,
            'question' => 'Risk Assessment:<br>Please list any risks associated with the project (e.g. not enough patients or practices recruited or key project stakeholders leave). (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 16,
            'question' => 'Stakeholders:<br>Explain who all the relevant stakeholders (name, role and title) are. Please provide name of NHS stakeholders and internal Teva stakeholders. (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 17,
            'question' => 'Project Lead:<br>Identify the individual or group that is taking the role of Project Executive. (There is a minimum requirement of at least one stakeholder from Teva and one stakeholder from the NHS organisation). (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 18,
            'question' => 'Project Team:<br>State the names and job titles of the Project Management team and identify the members. (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 19,
            'question' => 'NHS stakeholder responsible for data collection:<br>State the name of the person, their role and, if applicable, organisation responsible for collecting data used in this project. (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);

        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 20,
            'question' => 'Project Plan:<br>Provide the details of your project milestones and deadlines. (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);
        DB::table('questions')->insert([
            'step' => 4,
            'question_no' => 21,
            'question' => 'Project Controls:<br>Describe the reporting and monitoring mechanisms within the project and advise on the exit strategy. (500 characters maximum)',
            'answer_type' => 'text',
            'answer_length' => '500'
        ]);

        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 1,
            'question' => 'Executive summary of Joint Working between (1000 characters max)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 2,
            'question' => 'Project Name: (1000 characters max)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 3,
            'question' => 'Overarching Aim: (1000 characters max)',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 4,
            'question' => 'Project Scope: (1000 characters max) Describe what is included in the project (e.g. inclusion criteria: adults; exclusion criteria: children). Also include how outcomes will be measured.',
            'answer_type' => 'text',
            'answer_length' => '1000'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 5.1,
            'question' => 'Project Start Date:',
            'answer_type' => 'date'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 5.2,
            'question' => 'Project End Date:',
            'answer_type' => 'date'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 6,
            'question' => 'Third party due diligence / if applicable',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 7,
            'question' => 'Compliance Portal Activity ID',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 8,
            'question' => 'List contracts required',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 9,
            'question' => 'Name of individual and/or organisation',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 10,
            'question' => 'Third party due diligence / if applicable',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 5,
            'question_no' => 11,
            'question' => 'Compliance Portal Activity ID',
            'answer_type' => 'text'
        ]);

        DB::table('questions')->insert([
            'step' => 6,
            'question_no' => 1,
            'question' => 'Zinc / if applicable',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 6,
            'question_no' => 2.1,
            'question' => 'PO Numbers / if applicable',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 6,
            'question_no' => 2.2,
            'question' => 'Who is it for: (Name of CCG, Hospital)',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 6,
            'question_no' => 2.3,
            'question' => 'Purpose: (Training, education)',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 6,
            'question_no' => 3,
            'question' => 'Third party due diligence if applicable',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 6,
            'question_no' => 4,
            'question' => 'Compliance Portal Activity ID',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 7,
            'question_no' => 1,
            'question' => 'Review',
            'answer_type' => 'date'
        ]);
        DB::table('questions')->insert([
            'step' => 8,
            'question_no' => 1,
            'question' => 'Please explain what data is to be included in the report?',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 8,
            'question_no' => 2,
            'question' => 'Explain who has collected the data throughout this project?',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 8,
            'question_no' => 3,
            'question' => 'Where has the data been published? (Please include the Zinc job code, approval date and date of publication)',
            'answer_type' => 'text'
        ]);
        DB::table('questions')->insert([
            'step' => 8,
            'question_no' => 4,
            'question' => 'Do you have the stakeholders written permission in the Joint Working agreement to publish the data?',
            'answer_type' => 'checkbox',
            'answer_length' => '1'
        ]);
        DB::table('questions')->insert([
            'step' => 8,
            'question_no' => 5,
            'question' => 'When and where?',
            'answer_type' => 'text'
        ]);
    }
}
