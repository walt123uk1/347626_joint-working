<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
    	// DB::table('users')->insert([
     //        'role' => 'author',
     //        'approved' => 1,
     //        'firstname' => 'rick',
     //        'surname' => 'hayhurst',
     //        'phone' => '01204888888',
     //        'job_title' => 'footballer',
     //        'email' =>	'a@a.com',
     //        'password' => bcrypt('author')
     //    ]);
     //    DB::table('users')->insert([
     //        'role' => 'editor',
     //        'approved' => 1,
     //        'firstname' => 'rick',
     //        'surname' => 'hayhurst',
     //        'phone' => '01204888888',
     //        'job_title' => 'footballer',
     //        'email' =>	'e@e.com',
     //        'password' => bcrypt('editor')
     //    ]);
        // DB::table('users')->insert([
        //     'role' => 'master',
        //     'approved' => 1,
        //     'firstname' => 'Master Michael',
        //     'surname' => 'McMasterson',
        //     'phone' => '01204888888',
        //     'job_title' => 'Master',
        //     'email' =>	'master@pulsarhealthcare.com',
        //     'password' => bcrypt('$Pulsar2018$')
        // ]); 
    	DB::table('users')->insert([
            'role' => 'author',
            'approved' => 1,
            'firstname' => 'tim',
            'surname' => 'walton',
            'phone' => '01204888888',
            'job_title' => 'footballer',
            'email' =>	'tw_a@a.com',
            'password' => bcrypt('author')
        ]);
        DB::table('users')->insert([
            'role' => 'editor',
            'approved' => 1,
            'firstname' => 'tim',
            'surname' => 'walton',
            'phone' => '01204888888',
            'job_title' => 'footballer',
            'email' =>	'tw_e@e.com',
            'password' => bcrypt('editor')
        ]);
        DB::table('users')->insert([
            'role' => 'master',
            'approved' => 1,
            'firstname' => 'tim',
            'surname' => 'walton',
            'phone' => '01204888888',
            'job_title' => 'footballer',
            'email' =>	'tw_m@m.com',
            'password' => bcrypt('master')
        ]);
    }
}
