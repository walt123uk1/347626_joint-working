<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   	
        DB::table('projects')->insert([
            'user_id' => 1,
            'project_name' => 'Project Keep Bury Up',
            'approach_date' => '2018-01-01',
            'start_date' => '2018-01-18',
            'step_0_status' => '4',
            'step_0_locked' => '1',
            'step_1_status' => '1',
            'step_1_date' => '2018-01-18',
            'step_2_date' => '2018-02-01',
            'step_3_date' => '2018-03-01',
            'step_4_date' => '2018-04-01',
            'step_5_date' => '2018-05-01',
            'step_6_date' => '2018-06-01',
            'step_7_date' => '2018-07-01',
            'step_8_date' => '2018-08-01'
        ]);     

        // DB::table('projects')->insert([
        //     'user_id' => 1,
        //     'project_name' => 'Project Send Bury down',
        //     'approach_date' => '2018-01-01',
        //     'start_date' => '2018-01-18',
        //     'step_0_status' => '4',
        //     'step_0_locked' => '1',
        //     'step_1_status' => '1',
        //     'step_1_date' => '2018-01-18',
        //     'step_2_date' => '2018-02-01',
        //     'step_3_date' => '2018-03-01',
        //     'step_4_date' => '2018-04-01',
        //     'step_5_date' => '2018-05-01',
        //     'step_6_date' => '2018-06-01',
        //     'step_7_date' => '2018-07-01',
        //     'step_8_date' => '2018-08-01'
        // ]);         

        // DB::table('projects')->insert([
        //     'user_id' => 4,
        //     'project_name' => 'Project Keep City Up',
        //     'approach_date' => '2018-01-01',
        //     'start_date' => '2018-01-18',
        //     'step_0_status' => '4',
        //     'step_0_locked' => '1',
        //     'step_1_status' => '1',
        //     'step_2_status' => '1',
        //     'step_3_status' => '1',
        //     'step_4_status' => '1',
        //     'step_5_status' => '1',
        //     'step_6_status' => '1',
        //     'step_7_status' => '1',
        //     'step_8_status' => '1',
        //     'step_1_date' => '2018-01-18',
        //     'step_2_date' => '2018-02-01',
        //     'step_3_date' => '2018-03-01',
        //     'step_4_date' => '2018-04-01',
        //     'step_5_date' => '2018-05-01',
        //     'step_6_date' => '2018-06-01',
        //     'step_7_date' => '2018-07-01',
        //     'step_8_date' => '2018-08-01'
        // ]);     

        // DB::table('projects')->insert([
        //     'user_id' => 4,
        //     'project_name' => 'Project Send City down',
        //     'approach_date' => '2018-01-01',
        //     'start_date' => '2018-01-18',
        //     'step_0_status' => '4',
        //     'step_0_locked' => '1',
        //     'step_1_status' => '1',
        //     'step_1_date' => '2018-01-18',
        //     'step_2_date' => '2018-02-01',
        //     'step_3_date' => '2018-03-01',
        //     'step_4_date' => '2018-04-01',
        //     'step_5_date' => '2018-05-01',
        //     'step_6_date' => '2018-06-01',
        //     'step_7_date' => '2018-07-01',
        //     'step_8_date' => '2018-08-01'
        // ]);   
    }
}
