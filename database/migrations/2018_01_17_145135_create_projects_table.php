<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('project_name');
            $table->date('approach_date');
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->integer('step_0_status')->default(0);
            $table->integer('step_0_locked')->default(0);
            $table->integer('step_1_status')->default(0);
            $table->date('step_1_date');
            $table->integer('step_1_locked')->default(0);
            $table->integer('step_2_status')->default(0);
            $table->date('step_2_date');
            $table->integer('step_2_locked')->default(0);
            $table->integer('step_3_status')->default(0);
            $table->date('step_3_date');
            $table->integer('step_3_locked')->default(0);
            $table->integer('step_4_status')->default(0);
            $table->date('step_4_date');
            $table->integer('step_4_locked')->default(0);
            $table->integer('step_5_status')->default(0);
            $table->date('step_5_date');
            $table->integer('step_5_locked')->default(0);
            $table->integer('step_6_status')->default(0);
            $table->date('step_6_date')->nullable();
            $table->integer('step_6_locked')->default(0);
            $table->integer('step_7_status')->default(0);
            $table->date('step_7_date')->nullable();
            $table->integer('step_7_locked')->default(0);
            $table->integer('step_8_status')->default(0);
            $table->date('step_8_date')->nullable();
            $table->integer('step_8_locked')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
