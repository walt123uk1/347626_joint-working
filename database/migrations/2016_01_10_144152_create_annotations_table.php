<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAnnotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annotations', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->text('note')->nullable();
            $table->integer('archive')->default(0);
            $table->integer('posn_x');
            $table->integer('posn_y');
            $table->integer('user_id');
            $table->string('answer_id');
            $table->integer('project_id');
            $table->integer('step_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('annotations');
    }
}
