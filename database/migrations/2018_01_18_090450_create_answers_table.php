<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->boolean('answer_boolean')->nullable();
            $table->text('answer_text')->nullable();
            $table->date('answer_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->unique(['project_id', 'question_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('answers');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
