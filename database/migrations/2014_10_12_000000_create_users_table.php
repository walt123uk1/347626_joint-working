<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(255);

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('role')->default('viewer');
            $table->boolean('approved')->default(0);
            $table->string('firstname');
            $table->string('surname');
            $table->string('phone');
            $table->string('job_title');
            $table->string('department')->default('');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('beenToApproval')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::dropIfExists('users');
    }
}
