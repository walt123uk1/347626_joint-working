<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['Https Production']], function () {

    Auth::routes();

    Route::get('/', 'Auth\LoginController@redirect');

    Route::get('/approval', 'StepsController@waitingApproval');

    //All routes in this group check if the user is approved, if not they are redirected accordingly to the holding page

        Route::group(['middleware' => ['checkApproved']], function () {

        Route::get('/profile/{user}', 'profileController@index')->name('profile');

        Route::patch('/profile/update/{user}', 'profileController@update')->name('profile update');

        /////////////////Projects and Steps /////////////////////

        Route::get('/projects', 'StepsController@index')->name('projects');

        //search project
        Route::get('/projects/search', 'SearchController@test');

        Route::get('/projects/{project}', 'StepsController@projectDetails')->name('view project');

        Route::patch('/projects/{project}/update', 'StepsController@updateProject');

        //Add Project

        Route::get('/projects/new/project', 'StepsController@newProjectDetails')->name('new project details');

        Route::post('/projects/new/submit', 'StepsController@addProject')->name('add new project');

        //Steps

        Route::get('/{project}/steps/{step}', 'StepsController@detail')->name('step detail');

        Route::post('/{project}/steps/{step}/submit', 'StepsController@submit');

        ////////////////////////Master Routes/////////////////////

        Route::group(['middleware' => ['user is master']], function () {

            Route::get('/master/user-admin', 'UserAdminController@userList')->name('user admin');

            //Edit User

            Route::get('/master/user-admin/edit-user/{user}', 'UserAdminController@userDetails');

            Route::patch('/master/user-admin/edit-user/{user}/edit', 'UserAdminController@editUser');

            //Add User

            Route::get('/master/user-admin/new-user', 'UserAdminController@newUserDetails');

            Route::post('/master/user-admin/new-user/submit', 'UserAdminController@addUser');

            //Search user
            Route::get('/users/search', 'SearchController@usersSearch');

            //Reports
            Route::get('/reports/finance', 'ReportController@finance');

            Route::get('/reports/view', 'ReportController@view');

            Route::get('/reports/view/search', 'ReportController@searchView');

            Route::get('/reports/view/search-finance', 'ReportController@searchFinance');

        });

        //Add Annotation
        Route::post('/annotation/noteset', 'AnnotationController@noteset');
        Route::get('/annotation/noteget/{project}/{step}', 'AnnotationController@noteget');
        Route::put('/annotation/update/{id}', 'AnnotationController@update');
        Route::put('/annotation/archive/{id}', 'AnnotationController@archive');
        Route::delete('/annotation/delete/{id}', 'AnnotationController@delete');

        //Get all questions and answers for pdf
        Route::get('/projects/{id}/full-pdf', 'SearchController@pdf');

    });
});


